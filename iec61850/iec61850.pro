#-------------------------------------------------
#
# Project created by QtCreator 2017-01-27T11:55:56
#
#-------------------------------------------------

#QT       += core network

QT       -= gui

TARGET = iec61850
TEMPLATE = lib

#DEFINES += PLUGIN

#contains(DEFINES, PLUGIN) {
#    message(Compiling as plugin)
#    target.path = $${PREFIX}/lib
#    INSTALLS+=target
#} else {
#    message(Compiling as local lib)
#}

unix {
    CONFIG += unversioned_libname unversioned_soname

    target.path = $${PREFIX}/lib
#    target.path = /usr/lib
    INSTALLS += target
}

win32:LIBS += -lWS2_32

DEFINES += EXCLUDE_ETHERNET_WINDOWS=1

INCLUDEPATH += config
INCLUDEPATH += src/common/inc
INCLUDEPATH += src/mms/iso_mms/asn1c
INCLUDEPATH += src/mms/inc
INCLUDEPATH += src/mms/inc_private
INCLUDEPATH += hal/inc
#INCLUDEPATH += src/goose
#INCLUDEPATH += src/sampled_values
INCLUDEPATH += src/iec61850/inc
INCLUDEPATH += src/iec61850/inc_private
INCLUDEPATH += src/logging

win32:SOURCES += \
     hal/socket/win32/socket_win32.c \
     hal/thread/win32/thread_win32.c \
     hal/filesystem/win32/file_provider_win32.c \
     hal/time/win32/time.c \
     hal/serial/win32/serial_port_win32.c \
     hal/memory/lib_memory.c

linux:SOURCES += \
     hal/socket/linux/socket_linux.c \
     hal/ethernet/linux/ethernet_linux.c \
     hal/thread/linux/thread_linux.c \
     hal/filesystem/linux/file_provider_linux.c \
     hal/time/unix/time.c \
     hal/serial/linux/serial_port_linux.c \
     hal/memory/lib_memory.c

SOURCES += \
     src/common/string_map.c \
     src/common/map.c \
     src/common/linked_list.c \
     src/common/byte_buffer.c \
     src/common/string_utilities.c \
     src/common/buffer_chain.c \
     src/common/conversions.c \
     src/common/mem_alloc_linked_list.c \
     src/common/simple_allocator.c \
     src/mms/iso_server/iso_connection.c \
     src/mms/iso_server/iso_server.c \
     src/mms/iso_acse/acse.c \
     src/mms/iso_mms/common/mms_type_spec.c \
     src/mms/iso_mms/common/mms_value.c \
     src/mms/iso_mms/common/mms_common_msg.c \
     src/mms/iso_mms/client/mms_client_initiate.c \
     src/mms/iso_mms/client/mms_client_write.c \
     src/mms/iso_mms/client/mms_client_identify.c \
     src/mms/iso_mms/client/mms_client_status.c \
     src/mms/iso_mms/client/mms_client_named_variable_list.c \
     src/mms/iso_mms/client/mms_client_connection.c \
     src/mms/iso_mms/client/mms_client_files.c \
     src/mms/iso_mms/client/mms_client_get_namelist.c \
     src/mms/iso_mms/client/mms_client_get_var_access.c \
     src/mms/iso_mms/client/mms_client_common.c \
     src/mms/iso_mms/client/mms_client_read.c \
     src/mms/iso_mms/client/mms_client_journals.c \
     src/mms/iso_mms/server/mms_read_service.c \
     src/mms/iso_mms/server/mms_file_service.c \
     src/mms/iso_mms/server/mms_association_service.c \
     src/mms/iso_mms/server/mms_identify_service.c \
     src/mms/iso_mms/server/mms_status_service.c \
     src/mms/iso_mms/server/mms_named_variable_list_service.c \
     src/mms/iso_mms/server/mms_value_cache.c \
     src/mms/iso_mms/server/mms_get_namelist_service.c \
     src/mms/iso_mms/server/mms_access_result.c \
     src/mms/iso_mms/server/mms_server.c \
     src/mms/iso_mms/server/mms_server_common.c \
     src/mms/iso_mms/server/mms_named_variable_list.c \
     src/mms/iso_mms/server/mms_domain.c \
     src/mms/iso_mms/server/mms_device.c \
     src/mms/iso_mms/server/mms_information_report.c \
     src/mms/iso_mms/server/mms_journal.c \
     src/mms/iso_mms/server/mms_journal_service.c \
     src/mms/iso_mms/server/mms_server_connection.c \
     src/mms/iso_mms/server/mms_write_service.c \
     src/mms/iso_mms/server/mms_get_var_access_service.c \
     src/mms/iso_cotp/cotp.c \
     src/mms/iso_presentation/iso_presentation.c \
     src/mms/asn1/ber_decode.c \
     src/mms/asn1/asn1_ber_primitive_value.c \
     src/mms/asn1/ber_encoder.c \
     src/mms/asn1/ber_integer.c \
     src/mms/iso_client/iso_client_connection.c \
     src/mms/iso_common/iso_connection_parameters.c \
     src/mms/iso_session/iso_session.c \
     src/iec61850/client/client_control.c \
     src/iec61850/client/client_report_control.c \
     src/iec61850/client/client_goose_control.c \
     src/iec61850/client/client_sv_control.c \
     src/iec61850/client/client_report.c \
     src/iec61850/client/ied_connection.c \
     src/iec61850/common/iec61850_common.c \
     src/iec61850/server/impl/ied_server.c \
     src/iec61850/server/impl/ied_server_config.c \
     src/iec61850/server/impl/client_connection.c \
     src/iec61850/server/model/model.c \
     src/iec61850/server/model/dynamic_model.c \
     src/iec61850/server/model/cdc.c \
     src/iec61850/server/model/config_file_parser.c \
     src/iec61850/server/mms_mapping/control.c \
     src/iec61850/server/mms_mapping/mms_mapping.c \
     src/iec61850/server/mms_mapping/reporting.c \
     src/iec61850/server/mms_mapping/mms_goose.c \
     src/iec61850/server/mms_mapping/mms_sv.c \
     src/iec61850/server/mms_mapping/logging.c \
     src/logging/log_storage.c \
     src/mms/iso_mms/asn1c/DataAccessError.c \
     src/mms/iso_mms/asn1c/DeleteNamedVariableListRequest.c \
     src/mms/iso_mms/asn1c/constr_SET_OF.c \
     src/mms/iso_mms/asn1c/MmsPdu.c \
     src/mms/iso_mms/asn1c/GetNamedVariableListAttributesResponse.c \
     src/mms/iso_mms/asn1c/BIT_STRING.c \
     src/mms/iso_mms/asn1c/ber_tlv_tag.c \
     src/mms/iso_mms/asn1c/constr_SEQUENCE_OF.c \
     src/mms/iso_mms/asn1c/asn_SET_OF.c \
     src/mms/iso_mms/asn1c/ReadResponse.c \
     src/mms/iso_mms/asn1c/InformationReport.c \
     src/mms/iso_mms/asn1c/ConfirmedServiceRequest.c \
     src/mms/iso_mms/asn1c/DeleteNamedVariableListResponse.c \
     src/mms/iso_mms/asn1c/asn_SEQUENCE_OF.c \
     src/mms/iso_mms/asn1c/VariableAccessSpecification.c \
     src/mms/iso_mms/asn1c/GetVariableAccessAttributesRequest.c \
     src/mms/iso_mms/asn1c/xer_support.c \
     src/mms/iso_mms/asn1c/ObjectName.c \
     src/mms/iso_mms/asn1c/NativeEnumerated.c \
     src/mms/iso_mms/asn1c/per_encoder.c \
     src/mms/iso_mms/asn1c/constr_SEQUENCE.c \
     src/mms/iso_mms/asn1c/GetNameListResponse.c \
     src/mms/iso_mms/asn1c/MMSString.c \
     src/mms/iso_mms/asn1c/InitiateErrorPdu.c \
     src/mms/iso_mms/asn1c/IndexRangeSeq.c \
     src/mms/iso_mms/asn1c/ConfirmedErrorPDU.c \
     src/mms/iso_mms/asn1c/UnconfirmedService.c \
     src/mms/iso_mms/asn1c/UTF8String.c \
     src/mms/iso_mms/asn1c/ServiceError.c \
     src/mms/iso_mms/asn1c/TimeOfDay.c \
     src/mms/iso_mms/asn1c/GetNameListRequest.c \
     src/mms/iso_mms/asn1c/asn_codecs_prim.c \
     src/mms/iso_mms/asn1c/Data.c \
     src/mms/iso_mms/asn1c/ScatteredAccessDescription.c \
     src/mms/iso_mms/asn1c/ReadRequest.c \
     src/mms/iso_mms/asn1c/per_decoder.c \
     src/mms/iso_mms/asn1c/Identifier.c \
     src/mms/iso_mms/asn1c/ServiceSupportOptions.c \
     src/mms/iso_mms/asn1c/Integer8.c \
     src/mms/iso_mms/asn1c/ConfirmedServiceResponse.c \
     src/mms/iso_mms/asn1c/ParameterSupportOptions.c \
     src/mms/iso_mms/asn1c/Integer16.c \
     src/mms/iso_mms/asn1c/ber_tlv_length.c \
     src/mms/iso_mms/asn1c/OCTET_STRING.c \
     src/mms/iso_mms/asn1c/DefineNamedVariableListRequest.c \
     src/mms/iso_mms/asn1c/FloatingPoint.c \
     src/mms/iso_mms/asn1c/xer_encoder.c \
     src/mms/iso_mms/asn1c/Unsigned8.c \
     src/mms/iso_mms/asn1c/BOOLEAN.c \
     src/mms/iso_mms/asn1c/INTEGER.c \
     src/mms/iso_mms/asn1c/UnconfirmedPDU.c \
     src/mms/iso_mms/asn1c/DataSequence.c \
     src/mms/iso_mms/asn1c/constraints.c \
     src/mms/iso_mms/asn1c/der_encoder.c \
     src/mms/iso_mms/asn1c/VisibleString.c \
     src/mms/iso_mms/asn1c/InitiateResponsePdu.c \
     src/mms/iso_mms/asn1c/StructComponent.c \
     src/mms/iso_mms/asn1c/Address.c \
     src/mms/iso_mms/asn1c/Unsigned16.c \
     src/mms/iso_mms/asn1c/ber_decoder.c \
     src/mms/iso_mms/asn1c/per_support.c \
     src/mms/iso_mms/asn1c/WriteResponse.c \
     src/mms/iso_mms/asn1c/InitRequestDetail.c \
     src/mms/iso_mms/asn1c/InitiateRequestPdu.c \
     src/mms/iso_mms/asn1c/DefineNamedVariableListResponse.c \
     src/mms/iso_mms/asn1c/NULL.c \
     src/mms/iso_mms/asn1c/ListOfVariableSeq.c \
     src/mms/iso_mms/asn1c/UtcTime.c \
     src/mms/iso_mms/asn1c/ConcludeResponsePDU.c \
     src/mms/iso_mms/asn1c/AccessResult.c \
     src/mms/iso_mms/asn1c/Integer32.c \
     src/mms/iso_mms/asn1c/GetNamedVariableListAttributesRequest.c \
     src/mms/iso_mms/asn1c/VariableSpecification.c \
     src/mms/iso_mms/asn1c/Unsigned32.c \
     src/mms/iso_mms/asn1c/constr_CHOICE.c \
     src/mms/iso_mms/asn1c/AlternateAccess.c \
     src/mms/iso_mms/asn1c/ObjectClass.c \
     src/mms/iso_mms/asn1c/InitResponseDetail.c \
     src/mms/iso_mms/asn1c/ConfirmedResponsePdu.c \
     src/mms/iso_mms/asn1c/GetVariableAccessAttributesResponse.c \
     src/mms/iso_mms/asn1c/NativeInteger.c \
     src/mms/iso_mms/asn1c/xer_decoder.c \
     src/mms/iso_mms/asn1c/AlternateAccessSelection.c \
     src/mms/iso_mms/asn1c/ConfirmedRequestPdu.c \
     src/mms/iso_mms/asn1c/ConcludeRequestPDU.c \
     src/mms/iso_mms/asn1c/WriteRequest.c \
     src/mms/iso_mms/asn1c/RejectPDU.c \
     src/mms/iso_mms/asn1c/TypeSpecification.c \
     src/mms/iso_mms/asn1c/constr_TYPE.c \
     src/mms/iso_mms/asn1c/GeneralizedTime.c

HEADERS += \
     config/stack_config.h \
     src/common/inc/buffer_chain.h \
     src/common/inc/byte_buffer.h \
     src/common/inc/conversions.h \
     src/common/inc/libiec61850_common_api.h \
     src/common/inc/libiec61850_platform_includes.h \
     src/common/inc/linked_list.h \
     src/common/inc/map.h \
     src/common/inc/mem_alloc_linked_list.h \
     src/common/inc/simple_allocator.h \
     src/common/inc/string_map.h \
     src/common/inc/string_utilities.h \
     hal/inc/hal_ethernet.h \
     hal/inc/hal_filesystem.h \
     hal/inc/hal_serial.h \
     hal/inc/hal_socket.h \
     hal/inc/hal_thread.h \
     hal/inc/hal_time.h \
     hal/inc/lib_memory.h \
     hal/inc/platform_endian.h \
     hal/inc/tls_config.h \
     hal/inc/tls_socket.h \
     src/iec61850/inc/iec61850_cdc.h \
     src/iec61850/inc/iec61850_client.h \
     src/iec61850/inc/iec61850_common.h \
     src/iec61850/inc/iec61850_config_file_parser.h \
     src/iec61850/inc/iec61850_dynamic_model.h \
     src/iec61850/inc/iec61850_model.h \
     src/iec61850/inc/iec61850_server.h \
     src/iec61850/inc_private/control.h \
     src/iec61850/inc_private/ied_connection_private.h \
     src/iec61850/inc_private/ied_server_private.h \
     src/iec61850/inc_private/logging.h \
     src/iec61850/inc_private/mms_goose.h \
     src/iec61850/inc_private/mms_mapping.h \
     src/iec61850/inc_private/mms_mapping_internal.h \
     src/iec61850/inc_private/mms_sv.h \
     src/iec61850/inc_private/reporting.h \
     src/logging/logging_api.h \
     src/mms/inc/asn1_ber_primitive_value.h \
     src/mms/inc/ber_integer.h \
     src/mms/inc/iso_connection_parameters.h \
     src/mms/inc/iso_server.h \
     src/mms/inc/mms_client_connection.h \
     src/mms/inc/mms_common.h \
     src/mms/inc/mms_device_model.h \
     src/mms/inc/mms_named_variable_list.h \
     src/mms/inc/mms_server.h \
     src/mms/inc/mms_types.h \
     src/mms/inc/mms_type_spec.h \
     src/mms/inc/mms_value.h \
     src/mms/inc_private/acse.h \
     src/mms/inc_private/ber_decode.h \
     src/mms/inc_private/ber_encoder.h \
     src/mms/inc_private/cotp.h \
     src/mms/inc_private/iso_client_connection.h \
     src/mms/inc_private/iso_presentation.h \
     src/mms/inc_private/iso_server_private.h \
     src/mms/inc_private/iso_session.h \
     src/mms/inc_private/mms_client_internal.h \
     src/mms/inc_private/mms_common_internal.h \
     src/mms/inc_private/mms_server_connection.h \
     src/mms/inc_private/mms_server_internal.h \
     src/mms/inc_private/mms_value_cache.h \
     src/mms/inc_private/mms_value_internal.h \
     src/mms/iso_mms/asn1c/AccessResult.h \
     src/mms/iso_mms/asn1c/Address.h \
     src/mms/iso_mms/asn1c/AlternateAccess.h \
     src/mms/iso_mms/asn1c/AlternateAccessSelection.h \
     src/mms/iso_mms/asn1c/asn_application.h \
     src/mms/iso_mms/asn1c/asn_codecs.h \
     src/mms/iso_mms/asn1c/asn_codecs_prim.h \
     src/mms/iso_mms/asn1c/asn_internal.h \
     src/mms/iso_mms/asn1c/asn_SEQUENCE_OF.h \
     src/mms/iso_mms/asn1c/asn_SET_OF.h \
     src/mms/iso_mms/asn1c/asn_system.h \
     src/mms/iso_mms/asn1c/ber_decoder.h \
     src/mms/iso_mms/asn1c/ber_tlv_length.h \
     src/mms/iso_mms/asn1c/ber_tlv_tag.h \
     src/mms/iso_mms/asn1c/BIT_STRING.h \
     src/mms/iso_mms/asn1c/BOOLEAN.h \
     src/mms/iso_mms/asn1c/ConcludeRequestPDU.h \
     src/mms/iso_mms/asn1c/ConcludeResponsePDU.h \
     src/mms/iso_mms/asn1c/ConfirmedErrorPDU.h \
     src/mms/iso_mms/asn1c/ConfirmedRequestPdu.h \
     src/mms/iso_mms/asn1c/ConfirmedResponsePdu.h \
     src/mms/iso_mms/asn1c/ConfirmedServiceRequest.h \
     src/mms/iso_mms/asn1c/ConfirmedServiceResponse.h \
     src/mms/iso_mms/asn1c/constraints.h \
     src/mms/iso_mms/asn1c/constr_CHOICE.h \
     src/mms/iso_mms/asn1c/constr_SEQUENCE.h \
     src/mms/iso_mms/asn1c/constr_SEQUENCE_OF.h \
     src/mms/iso_mms/asn1c/constr_SET_OF.h \
     src/mms/iso_mms/asn1c/constr_TYPE.h \
     src/mms/iso_mms/asn1c/Data.h \
     src/mms/iso_mms/asn1c/DataAccessError.h \
     src/mms/iso_mms/asn1c/DataSequence.h \
     src/mms/iso_mms/asn1c/DefineNamedVariableListRequest.h \
     src/mms/iso_mms/asn1c/DefineNamedVariableListResponse.h \
     src/mms/iso_mms/asn1c/DeleteNamedVariableListRequest.h \
     src/mms/iso_mms/asn1c/DeleteNamedVariableListResponse.h \
     src/mms/iso_mms/asn1c/der_encoder.h \
     src/mms/iso_mms/asn1c/FloatingPoint.h \
     src/mms/iso_mms/asn1c/GeneralizedTime.h \
     src/mms/iso_mms/asn1c/GetNamedVariableListAttributesRequest.h \
     src/mms/iso_mms/asn1c/GetNamedVariableListAttributesResponse.h \
     src/mms/iso_mms/asn1c/GetNameListRequest.h \
     src/mms/iso_mms/asn1c/GetNameListResponse.h \
     src/mms/iso_mms/asn1c/GetVariableAccessAttributesRequest.h \
     src/mms/iso_mms/asn1c/GetVariableAccessAttributesResponse.h \
     src/mms/iso_mms/asn1c/Identifier.h \
     src/mms/iso_mms/asn1c/IndexRangeSeq.h \
     src/mms/iso_mms/asn1c/InformationReport.h \
     src/mms/iso_mms/asn1c/InitiateErrorPdu.h \
     src/mms/iso_mms/asn1c/InitiateRequestPdu.h \
     src/mms/iso_mms/asn1c/InitiateResponsePdu.h \
     src/mms/iso_mms/asn1c/InitRequestDetail.h \
     src/mms/iso_mms/asn1c/InitResponseDetail.h \
     src/mms/iso_mms/asn1c/INTEGER.h \
     src/mms/iso_mms/asn1c/Integer16.h \
     src/mms/iso_mms/asn1c/Integer32.h \
     src/mms/iso_mms/asn1c/Integer8.h \
     src/mms/iso_mms/asn1c/ListOfVariableSeq.h \
     src/mms/iso_mms/asn1c/MmsPdu.h \
     src/mms/iso_mms/asn1c/MMSString.h \
     src/mms/iso_mms/asn1c/NativeEnumerated.h \
     src/mms/iso_mms/asn1c/NativeInteger.h \
     src/mms/iso_mms/asn1c/NULL.h \
     src/mms/iso_mms/asn1c/ObjectClass.h \
     src/mms/iso_mms/asn1c/ObjectName.h \
     src/mms/iso_mms/asn1c/OCTET_STRING.h \
     src/mms/iso_mms/asn1c/ParameterSupportOptions.h \
     src/mms/iso_mms/asn1c/per_decoder.h \
     src/mms/iso_mms/asn1c/per_encoder.h \
     src/mms/iso_mms/asn1c/per_support.h \
     src/mms/iso_mms/asn1c/ReadRequest.h \
     src/mms/iso_mms/asn1c/ReadResponse.h \
     src/mms/iso_mms/asn1c/RejectPDU.h \
     src/mms/iso_mms/asn1c/ScatteredAccessDescription.h \
     src/mms/iso_mms/asn1c/ServiceError.h \
     src/mms/iso_mms/asn1c/ServiceSupportOptions.h \
     src/mms/iso_mms/asn1c/StructComponent.h \
     src/mms/iso_mms/asn1c/TimeOfDay.h \
     src/mms/iso_mms/asn1c/TypeSpecification.h \
     src/mms/iso_mms/asn1c/UnconfirmedPDU.h \
     src/mms/iso_mms/asn1c/UnconfirmedService.h \
     src/mms/iso_mms/asn1c/Unsigned16.h \
     src/mms/iso_mms/asn1c/Unsigned32.h \
     src/mms/iso_mms/asn1c/Unsigned8.h \
     src/mms/iso_mms/asn1c/UtcTime.h \
     src/mms/iso_mms/asn1c/UTF8String.h \
     src/mms/iso_mms/asn1c/VariableAccessSpecification.h \
     src/mms/iso_mms/asn1c/VariableSpecification.h \
     src/mms/iso_mms/asn1c/VisibleString.h \
     src/mms/iso_mms/asn1c/WriteRequest.h \
     src/mms/iso_mms/asn1c/WriteResponse.h \
     src/mms/iso_mms/asn1c/xer_decoder.h \
     src/mms/iso_mms/asn1c/xer_encoder.h \
     src/mms/iso_mms/asn1c/xer_support.h \
     src/sampled_values/sv_publisher.h \
     src/sampled_values/sv_subscriber.h \
     src/vs/stdbool.h \

