# thucls

THU CLS Application builds on german smart grid regulations to link distributed energy systems with Grid Operator backend system utilising IEC 61850. While the software is intended to be used as part of a smart grid - it may be also used for other purposes like home energy management systems.

At the moment these devices are supported:
- Sunspec compatible PV inverter
- SolarLog controlled PV system
- SonnenBatterie Eco 8 series battery
- Phoenix Contact based EV charger station
- E.G.O Smart Heater
- Janitza UMG 96RM series metering device

The connection to the backend builds on a static IEC 61850 model. In addition the software allows to integrate into ZENNER Hessware control center. Further on there is an experimental interface to interact with a local blockchain node provided by Oli Systems.

The software was developed for research purposes beeing part of the following research projects:
- [SerendiPV](http://serendipv.eu)
- [C/sells](http://www.csells.net)
- CLS-App BW
- Smart Solar Grid

# Dependencies
The software is based on C++ and uses Qt. It was developed using the QtCreator IDE (open souce edition). The following libraries are needed: 
- [Qt runtime libraries](https://www.qt.io/)
- [libmodbus](https://github.com/stephane/libmodbus)
- [libiec61850](http://libiec61850.com/libiec61850)

Unfortunatelly the current project folder structure is inadequate to integrate the later 2 libraries by reference to the respective repository. The software is included as copy therefore.

# Folder Structure
- iec61850: compile this first and put the binary into folder lib
- thucls:
    - io: contains I/O handlers for both device interaction and backend connection
	    - modsun: modbus based interfaces including sunspec layer
	    - rest: REST based interfaces
	    - soefler: i/o handler to fill iec61850 data model
	    - uplink: connection to backend (mostly Zenner Hessware specific)
	- plugins: interconnect local device(s) with backend system and/or perform local regulation
	- uTest: unit tests - mostly experimental
	
# Setup and Compile
- [ ] unzip ./iec61850/libiec61850.zip into folders hal and src
- [ ] open iec61850.pro with [QtCreator](https://www.qt.io/) and build the library
- [ ] move compiled iec61850.dll or iec61850.so into ./lib
- [ ] unzip ./thucls/io/soefler/libiec61850 into folder libiec61850
- [ ] open ./thucls/thucls.pro with [QtCreator](https://www.qt.io/)
- [ ] adapt ./thucls/config.json according to your needs (see config_readme.txt)
- [ ] now compile (and run) thucls.pro
- [ ] make sure you have the QtCreator build chain up and running before compiling with docker (see ./scripts/Dockerfile)

# License
These parts come with their own copyright and licence:
- libmodbus has Copyright (c) Stéphane Raimbault
- libiec61850 has Copyright (c) Michael Zillgith
- control-interface.h has Copyright (c) Sebastian Heß, ZENNER Hessware GmbH

Copyright(c) 2021 Heiko Lorenz, University of Applied Siences Ulm [THU](http://www.thu.de)

The software is provided under the license of GNU GPL v3.

It comes WITHOUT ANY WARRANTY. See the GNU General Public License for more details.
