/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef BALING_H
#define BALING_H

#include "appling.h"
#include "../io/rest/restsonneneco8.h"

/**
 * Battery Application PlugIn.
 */
class BaLing: public AppLing
{
	Q_OBJECT
public:
	explicit BaLing(QObject *parent = 0);

	bool setupObject(QString swid, QVariantMap &data);

	bool init();
	bool connectDevice();

	QString getName() { return "Battery Plug-In"; }
	GenIo* getIoHandler() { return &battery; }
	QString getShortInfo() { return "Communicate with \"Sonnenbatterie\" via proprietary REST interface and foward values using IEC 61850."; }
signals:

public slots:
	bool slotSetBatPower(qint32 pow);
	void slotSetBatOpMod(QVariant value);
	void slotSetBatMaxSOC(QVariant value);
	void slotSetBatMinSOC(QVariant value);
	void slotUpdate();

protected:
	virtual int setPercent(int percent);
	RestSonnenEco8 battery; //todo: make generic
	float maxPower;
};

#endif // BALING_H
