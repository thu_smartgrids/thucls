/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef APPLING_H
#define APPLING_H

#include <QTimer>
#include <QDebug>

#include "geling.h"
#include "../io/soefler/clsProfile.h"
#include "../io/modsun/mbolibox.h"

#define SETEM_NOT_SUPPORTED -1001
#define SETEM_WRITE_FAILED -1002
#define SETEM_READ_FAILED -1003

/**
 * Generic application whith 61850 connection.
 * Limits the percentage from the EMT to the value given by the DIstribution Net Operator.
 */
class AppLing : public GeLing
{
	Q_OBJECT
public:
	explicit AppLing(QObject *parent = 0);

	bool setupObject(QString swid, QVariantMap &data);

	bool init();
	void start();
	virtual bool connectDevice() = 0;

	/* Appling has to ensure emt is below dino */

signals:

public slots:
	void slotConnect();
	virtual void updateHealth();

protected slots:
	virtual bool slotUpdatePwr();
	void slotEndPwr();

protected:
	friend class MbOliBox;
	virtual void setEmPercent(QString id, uint percent);
	virtual void slotSetDinoPercent(QVariant value) { Q_UNUSED(value) /* do nothing by default */ };
	void updateDinoPercent(int percent);

	virtual int setPercent(int percent) { Q_UNUSED(percent)
					      qDebug() << "function not supported for this device";
					      return SETEM_NOT_SUPPORTED; }

	ClsProfile* dino;
	Soefler* soefler;

	MbOliBox* oli;
	QString oliip;
	quint32 oliconf;

	QTimer updatimer, montimer;
	QTimer setimer, endtimer; /* refresh set value until endtimer is reached */

	uint emPercent;
	uint dinoPercent;
	int emGranularity;
};

#endif // APPLING_H
