/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "geling.h"

GeLing::GeLing(QObject* parent) : QObject(parent)
{
	swid = ip = "";
	deviceId = -1;
}

bool GeLing::setupObject(QString swid, QVariantMap &data)
{
	this->swid = swid;

	if( data.contains("deviceID") ){
		bool okay = false;
		qint32 id = data["deviceID"].toInt(&okay);
		if( okay ){
			deviceId = id;
		}
	}

	if( !data.contains("IP") ) {
		qDebug() << "data doesn't hold an element named IP";
		return false;
	}

	QVariant ipv = data["IP"];
	if( ipv.type() != QVariant::String ) {
		qDebug() << "item IP is not of type string";
		return false;
	}

	ip = ipv.toString();
	return true;
}

void GeLing::setEmPercent(QString id, QString swid, uint percent)
{
	if( this->swid != swid ){
		/* shall do nothing in this case */
		return;
	}
	setEmPercent(id, percent);
}

void GeLing::sendAdjustedSlotPercent(uint percent)
{
	ControlServer CC(this);
	QString rawValue = "{\"f\":\"setPercent\",\"id\":\"1\",\"s\":\"1\",\"d\":{";
	rawValue += "\"eip\":\"" + swid + "\",\"p\":" + QString::number(percent);
	rawValue += "}}";

	CC.sendRaw(rawValue);
}
