/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU
 ** Lesser General Public License version 3. A copy of the license
 ** should be part of this distribution package.
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details.
 **
 *******************************************************************/

#ifndef PBLING_H
#define PBLING_H

#include <QTimer>
#include "appling.h"
#include "aggregator.h"
#include "baling.h"
#include "healing.h"
#include "sling.h"
#include "uTest/loadscenario.h"

#include <QDebug>

/**
 * PV-Power to Heat Application PlugIn.
 * Integrates SunSpec PlugIn and Heater PlugIn as components.
 */
class PbLing : public AppLing
{
public:
	explicit PbLing(QObject *parent = 0);

	bool setupObject(QString swid, QVariantMap &data);

	bool init();
	void start();
	bool connectDevice();

	QString getName() { return "Home Agent Plug-In"; }
	GenIo* getIoHandler() { return inverter.getIoHandler(); }
	QString getShortInfo() { return "Combine SunSpec compatible PV inverter with Battery and Smart Heater"; }

signals:

public slots:
	void slotSetVals();
	void slotUpdate();
	void checkHealth();
	void slotSetAgent(qint32 agent);

protected:
	virtual int setPercent(int percent) { Q_UNUSED(percent) return 0; }

	LoadScenario load;

	NoDevice aggregator;
	Sling inverter;
	BaLing battery;
	HeaLing heater;

	qint32 agent;

protected slots:

};

#endif // PBLING_H
