/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "chaling.h"
#include <QDebug>


Chaling::Chaling(QObject* parent) :
	AppLing(parent), modler(this)
{
}

bool Chaling::init()
{
	bool okay;

	okay = AppLing::init();
	if( ! okay ){
		return false;
	}

	okay =  connect(dino, &ClsProfile::signalSetChaMaxI, this, &Chaling::slotSetMaxI)
	     && connect(&updatimer, &QTimer::timeout, this, &Chaling::slotUpdate);

	if( ! okay ){
		qDebug() << "BaLing: init of signals & slots failed";
		return false;
	}

	return true;
}

bool Chaling::connectDevice()
{
	bool okay = modler.init(ip, deviceId);
	if( ! okay ){
		qDebug() << "initialisation of Modbus layer failed";
		return false;
	}

	modler.addBlock("status", 25000, "EvStatus", "ErrorCodes");
	modler.addBlock("measures1", 25000, "DispV1", "DispPowerFactor");
	modler.addBlock("measures2", 25000, "DispEnergyOfCurrentCharging", "DispFrequency");
	dino->initCharger(&modler);
	modler.enterCyclingMode();

	qDebug() << "Chaling::setupObject successful for " << ip;
	return true;
}

int Chaling::setPercent(int percent)
{
	if( percent < 0 || 100 < percent ){
		return SETEM_WRITE_FAILED;
	}

	QVariant current = QVariant(percent * 100 / 80);
	bool okay = modler.write("PwmSignal", current);
	if( !okay ){
		return SETEM_WRITE_FAILED;
	}

	/* reading not supported (only available when car is connected) */
	return percent;
}

void Chaling::slotSetMaxI(QVariant value)
{
	qint32 mCurrent = value.toInt();
	if( mCurrent < 0 || 80 < mCurrent ){
		qDebug() << "invalid value for PwmSignal";
		return;
	}

	modler.write("PwmSignal", QVariant(mCurrent));
	updateDinoPercent(mCurrent * 80 / 100); /* 80A = 100 % */
}

void Chaling::slotUpdate()
{
	dino->updateCharger(&modler);
	if( oli ){
		oli->update(modler.read("DispRealPower"));
	}
}

