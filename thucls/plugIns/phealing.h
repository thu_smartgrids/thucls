/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef PHEALING_H
#define PHEALING_H

#include <QTimer>
#include "appling.h"
#include "aggregator.h"
#include "healing.h"
#include "sling.h"

#include <QDebug>

/**
 * PV-Power to Heat Application PlugIn.
 * Integrates SunSpec PlugIn and Heater PlugIn as components.
 */
class PheaLing : public AppLing
{
public:
	explicit PheaLing(QObject *parent = 0);

	bool setupObject(QString swid, QVariantMap &data);

	bool init();
	void start();
	bool connectDevice();

	QString getName() { return "P2H Agent Plug-In"; }
	GenIo* getIoHandler() { return inverter.getIoHandler(); }
	QString getShortInfo() { return "Combine SunSpec compatible PV inverter with a Smart Heater"; }

signals:

public slots:
	void slotUpdate();
	void checkHealth();
	void slotSetAgent(qint32 agent);

protected:
	virtual int setPercent(int percent) { Q_UNUSED(percent) return 0; }
	//void setHeatPower(qint32 power, bool hard = false);
	void updateHeatPower(qint32 curWHeat);

	NoDevice aggregator;
	Sling inverter;
	HeaLing heater;

	SetVal dhPower;
	qint32 agent;
	qint32 wrtg;

protected slots:

};

#endif // PHEALING_H
