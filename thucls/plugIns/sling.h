/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef SLING_H
#define SLING_H

#include <QTimer>

#include "appling.h"
#include "../io/modsun/sunslayer.h"
//#include "../io/modsun/mbkacopowador.h"

/**
 * SunSpec Inverter Application Plug-In
 */
class Sling : public AppLing
{
	Q_OBJECT
public:
	explicit Sling(QObject *parent = 0);

	bool init();
	bool connectDevice();

	QString getName() { return "SunSpec Inverter Plug-In"; }
	GenIo* getIoHandler() { return &slayer; }
	QString getShortInfo() { return "Communicate with SunSpec compatible photovoltaic inverter and foward values using IEC 61850."; }

public slots:
	void slotSetDinoPercent(QVariant value);
	void slotUpdate();

protected slots:
	bool slotUpdatePwr();

protected:
	virtual int setPercent(int percent);

//	MbKacoPowador slayer;
	SunSlayer slayer;
	uint currentPercent;
};

#endif // SLING_H
