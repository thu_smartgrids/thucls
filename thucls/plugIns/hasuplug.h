/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef HASUPLUG_H
#define HASUPLUG_H

#include <QObject>
#include "io/uplink/control-interface.h"
#include "geling.h"

/**
 * HasuPlug serves as generic CLS controller (plug in) for the HSU functionalities.
 * The concrete implementation is selected depending on the values of \c data map within \c setupObject.
 */
class HasuPlug : public QObject, public ControlInterface
{
	Q_OBJECT
	Q_INTERFACES(ControlInterface)
public:
	explicit HasuPlug (QObject *parent = 0);
	~HasuPlug();

	bool setupObject(QString &swid, QVariantMap &data);
	bool allowMultiple() { return false; }
	bool asyncInitialization() { return false; }

	//QString getName() { return (impl != NULL)? impl->getName(): ""; }
	//QString getShortInfo() { return (impl != NULL)? impl->getShortInfo(): ""; }

signals:

public slots:
	void slotStart();
	void slotIoTest();
	void slotSetPercent(QString id, QString swid, uint percent);

protected:
	GeLing* impl;
	GeLing* meter;
	GeLing* create(QString uri);
	GeLing *createMeter(QString uri);
};

#endif // HASUPLUG_H
