/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "sampling.h"

#include <QDateTime>
#include <cmath>

Sampling::Sampling(QObject *parent) :
	AppLing(parent), sampler(93000, this), sampler2(124000, this)
{

}

bool Sampling::init()
{
	bool okay;

	okay = AppLing::init();
	if( !okay ){
		return false;
	}

	okay =  connect(&updatimer, &QTimer::timeout, this, &Sampling::slotUpdate)
	     && connect(&montimer, &QTimer::timeout, &sampler, &Sampler::checkHealth);

	if( ! okay ){
		qDebug() << "Sampling: init of signals & slots failed";
		return false;
	}

	return true;
}

bool Sampling::connectDevice()
{
	dino->initMeter( "THU",
			 "0.2",
			 "example data only");
	dino->initMeterAt2( "THU",
			    "0.2",
			    "example data only");

	return true;
}

void Sampling::slotUpdate()
{
	dino->updateJanitzaMeter(&sampler);
	dino->updateJanitzaMeterAt2(&sampler2);

	qint64 ctime = 0;
	io_ConnState conn = sampler.getHealth(&ctime);
	soefler->lock("LLN0");
	soefler->update("Mod", 1, ctime);
	soefler->update("Beh", 1, ctime);
	soefler->update("Health", conn, ctime);
	soefler->unlock();

	soefler->update("Beh", 1, ctime, QUALITY_VALIDITY_GOOD, "FSCH1");

	if( oli ){
		oli->update(sampler.read("TotW"));
	}
}

/********/

Sampler::Sampler(quint32 period, QObject* parent) : GenIo(parent)
{
	static WaveType chosenWave = sawWave;

	this->period = std::max(period, 1u);
	this->wave = chosenWave;

	chosenWave = WaveType(chosenWave+1 % lastWave);
}

bool Sampler::readBlock(QString block)
{
	Q_UNUSED(block);

	Hz = sample(Hz_avg, Hz_rng);
	PhV_phsA = sample(V_avg, V_rng);
	PhV_phsB = PhV_phsA + 3;
	PhV_phsC = PhV_phsA + 5;
	TotW = sample(W_avg, W_rng);
	TotVAr = TotW / 10;
	return true;
}

QVariant Sampler::read(QString element, QString block)
{
	Q_UNUSED(block);

	if( element == "Hz" ){
		return QVariant(Hz);
	} else if( element == "PhV_phsA" ){
		return QVariant(PhV_phsA);
	} else if( element == "PhV_phsB" ){
		return QVariant(PhV_phsB);
	} else if( element == "PhV_phsC" ){
		return QVariant(PhV_phsC);
	} else if( element == "TotW" ){
		return QVariant(TotW);
	} else if( element == "TotVAr" ){
		return QVariant(TotVAr);
	} else
		return QVariant(QVariant::Invalid);
}

bool Sampler::write(QString element, QVariant value)
{
	Q_UNUSED(element);
	Q_UNUSED(value);

	return false;
}

float Sampler::sample(float avg, float range)
{
	qint64 ticks = QDateTime::currentMSecsSinceEpoch() % period;
	float factor;

	switch( wave ){
	case sawWave:
		factor = abs(double(ticks) / period * 2.0 - 1) - 0.5;
		break;
	case sinWave:
		factor = cos(double(ticks) / period * 3.1416 * 2) * 0.5;
		break;
	default:
		factor = 1;
	}

	return avg + (range * factor);
}
