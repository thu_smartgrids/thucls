/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "sling.h"

#include <QDebug>
#include <QTimer>


Sling::Sling(QObject *parent) :
	AppLing(parent), slayer(this)
{
	currentPercent = 0xFF; /* invalid value */
}

bool Sling::init()
{
	bool okay;

	okay = AppLing::init();
	if( !okay  ){
		return false;
	}

	okay =  connect(dino, &ClsProfile::signalSetPercent, this, &Sling::slotSetDinoPercent)
	     && connect(&updatimer, &QTimer::timeout, this, &Sling::slotUpdate)
	     && connect(&montimer, &QTimer::timeout, &slayer, &SunSlayer::checkHealth);

	if( !okay ){
		qDebug() << "Sling: init of signals & slots failed";
		return false;
	}

	return true;
}

bool Sling::connectDevice()
{
	bool okay;

	okay = slayer.init(ip, deviceId);
	if( ! okay ) {
		qDebug() << "initialisation of SunSpec layer failed";
		return false;
	}

	if( ! (slayer.isModelAvail(103)  || slayer.isModelAvail(101)) ) {
		qDebug() << "SunSpec device does not support model 103";
		//return false;
	}

	if( ! slayer.isModelAvail(123) ) {
		qDebug() << "SunSpec device does not support model 123";
		return false;
	}

	slayer.addBlock("measures1", 25000, "PhVphA", "PF"); // use "PF" instead "VAr" OR "VAr_SF" (depending on model)
	slayer.addBlock("measures2", 25000, "St", "Evt2");

	dino->initPV(&slayer);
	if( oli ){
		oli->initValues(slayer.read("WRtg"));
	}

	/* */
	slayer.enterCyclingMode();

	qDebug() << "MyPlugIn::setupObject successful for " << ip;
	return true;
}

int Sling::setPercent(int percent)
{
	if( percent < 0 || 100 < percent ){
		return SETEM_WRITE_FAILED;
	}

	slayer.reconnect();

	currentPercent = percent;
	bool okay = slotUpdatePwr();
	setimer.start(180000);  // refresh each 3 min
	endtimer.start();

	if( !okay ) {
		return SETEM_WRITE_FAILED;
	}

	//TODO: wait 0,5 second

	QVariant reply = slayer.read("WMaxLimPct");
	if( !reply.isValid() ) {
		return SETEM_READ_FAILED;
	}

	return reply.toInt();
}

void Sling::slotSetDinoPercent(QVariant value)
{
	uint percent = value.toUInt();
	bool okay = slayer.write("WMaxLimPct", QVariant(percent));
	okay &= slayer.write("WMaxLim_Ena", QVariant(1));

	updateDinoPercent(percent);

	qDebug() << "slotSetDinoPercent"  << percent << (okay? " okay": " failed");
}

void Sling::slotUpdate()
{
	dino->updatePV(&slayer);
	if( oli ){
		oli->update(-slayer.read("W").toFloat(), slayer.read("WMaxLimPct"));
	}
}

bool Sling::slotUpdatePwr()
{
	if( 100 < currentPercent ){
		return false;
	}

	bool okay = slayer.write("WMaxLimPct", QVariant(currentPercent));
	okay &= slayer.write("WMaxLim_Ena", QVariant(1));
	return okay;
}
