/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef GELING_H
#define GELING_H

#include <QObject>
#include "io/uplink/control-interface.h"
#include "../io/genio.h"

/**
 * Generig Application PlugIn.
 * Basis for all App implementations. Together with HasuPlug mostly encapsules the
 * handling of hessware control-interface.
 */
class GeLing : public QObject
{
public:
	GeLing(QObject* parent);
	~GeLing() { }

	void setEmPercent(QString id, QString swid, uint percent);
	virtual bool setupObject(QString swid, QVariantMap &data);

	virtual bool init() = 0;
//	virtual void start() = 0;

	virtual QString getName() = 0;
	virtual GenIo* getIoHandler() = 0;
	virtual QString getShortInfo() = 0;

public slots:
	virtual void start() = 0;

protected:
	virtual void setEmPercent(QString id, uint percent) = 0;
	//virtual bool setDinoPercent(qint32 percent);
	void sendAdjustedSlotPercent(uint percent);

	QString swid, ip;
	qint32 deviceId;
};

#endif // GELING_H
