#ifndef SLOING_H
#define SLOING_H

#include "appling.h"
#include "../io/modsun/mbslogger.h"

/**
 * SolarLog PV logger Application PlugIn
 */

class Sloing : public AppLing
{
	Q_OBJECT
public:
	explicit Sloing(QObject *parent = 0);

	bool init();
	bool connectDevice();

	QString getName() { return "SolarLog Plug-In"; }
	GenIo* getIoHandler() { return &modler; }
	QString getShortInfo() { return "Communicate with SolarLog PV data logger via Modbus interface and foward values using IEC 61850."; }

public slots:
	void slotSetDinoPercent(QVariant value);
	void slotUpdate();

protected slots:
	bool slotUpdatePwr();

protected:
	int setPercent(int percent);
	uint currentPercent;
	MbSLogger modler;

private:
};

#endif // SLOING_H
