/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "appling.h"

#include <QDebug>
#include <algorithm>

AppLing::AppLing(QObject *parent) : GeLing(parent),
	updatimer(this), montimer(this), setimer(this), endtimer(this)
{
	dino = ClsProfile::getInstance(this);
	soefler = dino->getSoefler();
	oli = NULL;
	emPercent = 0;
	dinoPercent = 100;
	emGranularity = 5;
}

bool AppLing::setupObject(QString swid, QVariantMap &data)
{
	if( data.contains("oli") ){
		QVariant ipv = data["oli"];
		if( ipv.type() != QVariant::String ) {
			qDebug() << "item oli is not of type string";
			return false;
		}

		oliip = ipv.toString();
		oliconf = data["oliConf"].toInt();
		if( !oli ){
			oli = MbOliBox::getOneInstance(this);
			if( oli ){
				connect(&montimer, &QTimer::timeout, oli, &Modler::checkHealth);
			}
		}
	}
	endtimer.setInterval(3600000); // 1 hour by default
	if( data.contains("endPwrRefresh") ){
		quint64 minutes = data["endPwrRefresh"].toInt();
		if( minutes < 1 || 1500 < minutes ){
			qDebug() << "invalid value for endPwrRefresh";
		}
		endtimer.setInterval(minutes * 60000);
	}

	return GeLing::setupObject(swid, data);
}

bool AppLing::init()
{
	if( dino == NULL ){
		return false;
	}
	dino->init(getName(), getShortInfo());

	bool result = 	connect(&montimer, &QTimer::timeout, this, &AppLing::updateHealth)
			&& connect(&setimer, &QTimer::timeout, this, &AppLing::slotUpdatePwr)
			&& connect(&endtimer, &QTimer::timeout, this, &AppLing::slotEndPwr);

	return result;
}

void AppLing::start()
{
	/* first stop to avoid ugly effects during re-init */
	updatimer.stop();
	montimer.stop();
	/* do start even when init failed */
	updatimer.start(5000); /* each  5 s */
	montimer.start(30000); /* each 30 s */

	dino->start();

	if( oli != NULL ){
		bool olikay = oli->init(oliip, OliAccessConfig(oliconf));
		if( !olikay ){
			qDebug() << "intialisation of OLI Modbus connection failed";
		}
	}

	updateDinoPercent(100);  //TODO: the last setting from net operator should be saved somewhere

	QTimer::singleShot(0, this, SLOT(slotConnect()) );
}

void AppLing::slotConnect()
{
	bool okay = connectDevice();

	if( !okay ){
		QTimer::singleShot(15000, this, SLOT(slotConnect()) ); /* try again after 15 s */
	}
}

void AppLing::updateHealth()
{
	if( dino == NULL ){
		return;
	}
	dino->updateHealth();
}

bool AppLing::slotUpdatePwr()
{
	qDebug() << "when using setimer then override slotUpdatePwr";
	return false;
}

void AppLing::slotEndPwr()
{
	qDebug() << "stop refresh of power value";
	setimer.stop();
	endtimer.stop();
}

/* Write your setPercent code here
 *
 * For example: you want to write the percent value via SunSpec to an inverter
 *	take the value
 *	send it to the device
 *	if (successful)
 *		sendPluginOK;
 *		return;
 *	else if (failure)
 *		sendPluginFAIL(9990, "Your reason for the failure");
 */
void AppLing::setEmPercent(QString id, uint percent)
{
	int replyInt;

	emPercent = std::max(0u, percent);
	percent = std::min(emPercent, dinoPercent);

	qInfo() << "setEmPercent" << ip << "to" << percent << "% (" << emPercent << ")";

	replyInt = setPercent(percent);

	qInfo() << "reply is" << replyInt;

	if( replyInt == SETEM_NOT_SUPPORTED ){
		sendPluginFAIL(0, "function not supported for this switching point");
	}else if( replyInt == SETEM_WRITE_FAILED ){
		sendPluginFAIL(0, "writing failed");
	} else if( replyInt == SETEM_READ_FAILED ){
		sendPluginFAIL(0, "reading failed");
	} else if( abs((qint32)(replyInt - percent)) >= emGranularity ) {
		sendPluginFAIL(0, "new value was not taken");
	} else if( (uint)replyInt == emPercent ) {
		sendPluginOK;
	} else {
		sendPluginOKAdjusted(QString(replyInt));
	}
	return;
}

void AppLing::updateDinoPercent(int percent)
{
	static int lastReportedPercent = 0xFF; //invalid value
	dinoPercent = percent;

	if( percent < 15 ){
		percent = 0;
	} else if( percent < 45 ){
		percent = 30;
	} else if( percent < 75 ){
		percent = 60;
	} else {
		percent = 100;
	}
	if( lastReportedPercent != percent ){
		sendAdjustedSlotPercent(percent);
		lastReportedPercent = percent;
	}
}
