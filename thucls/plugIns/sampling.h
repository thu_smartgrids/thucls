/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef SAMPLING_H
#define SAMPLING_H

#include "appling.h"
#include "../io/genio.h"

#define Hz_avg  50
#define Hz_rng  10
#define V_avg  230
#define V_rng   20
#define W_avg  100
#define W_rng  200

typedef enum { sawWave, sinWave, lastWave } WaveType;

class Sampler : public GenIo
{
	Q_OBJECT
public:
	explicit Sampler(quint32 period, QObject *parent = 0);

	QString getName() { return "Sample Generator"; }

	bool readBlock(QString block);
	QVariant read(QString element, QString block = NULL);
	bool write(QString element, QVariant value);
	bool recover(quint16 failrate) { Q_UNUSED(failrate); return true; }

protected:
	float Hz, PhV_phsA, PhV_phsB, PhV_phsC, TotVAr, TotW;

	float sample(float min, float range);
private:
	bool ascending;
	float factor;
	quint32 period;
	WaveType wave;
};


class Sampling : public AppLing
{
	Q_OBJECT
public:
	explicit Sampling(QObject *parent = 0);

	bool init();
	bool connectDevice();

	QString getName() { return "Sample Data Plug-In"; }
	GenIo* getIoHandler() { return NULL; }
	QString getShortInfo() { return "foward sample values using IEC 61850."; }
public slots:
	void slotUpdate();
protected:
	Sampler sampler, sampler2;
};

#endif // SAMPLING_H
