/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "phealing.h"
#include "targetlayer.h"

PheaLing::PheaLing(QObject *parent) : AppLing(parent) , aggregator(this) , inverter(this) , heater(this)
{
	agent = 0;
	wrtg = 0;
}

bool PheaLing::setupObject(QString swid, QVariantMap &data)
{
	bool result = AppLing::setupObject(swid, data);
	result &= inverter.setupObject(swid, data);

	QVariant ipHeat = data["IPheat"];
	data.remove("IP");
	data.remove("IPheat");
	data["IP"] = ipHeat;

	result &= heater.setupObject(swid, data);
	return result;
}

bool PheaLing::init()
{
	bool okay = heater.init()
		  & inverter.init();

	bool okay2 = AppLing::init();
	dino->initMeter( "HS-Ulm Smart Grid Research Group",
			 TargetLayer::getSwRevision(),
			 "aggregation from PV and heater" );

	okay2 &= connect(&updatimer, &QTimer::timeout, this, &PheaLing::slotUpdate) != NULL;
	okay2 &= connect(dino, &ClsProfile::signalSetAgent, this, &PheaLing::slotSetAgent) != NULL;

	return okay && okay2;
}

void PheaLing::start()
{
	heater.start();
	inverter.start();

	AppLing::start();
}

bool PheaLing::connectDevice()
{
	//TODO heater.connectDevice(); inverter.connectDevice();
	/*bool okay = heater.connectDevice()
		  & inverter.connectDevice();*/

	return true;
}

/* agent values
 * 1: powerCap2kW
 *	when pv power exceeds cap of 2 kW, then heater is turned on stepwise
 * 2: abovePowerLimit
 *	pv is limited by em (and or dino?). heater utilises the "free" energy
 * 3: voltageRegulation
 *	when voltage goes below some limit then the heating power is reduced
 *	when voltage goes beyond some limit then the heating power is increased
 * 4: 1+3 (power cap has precedence)
 * 5: 2+3 (power limit has precedence)
 * 6: voltageLimit
 *	when voltage goes below hard limit then the heater is shut off
 *	when voltage goes beyond hard limit then the heater is turned on
 * 7: 4+3 (voltage limit has precedence)
 * 8: 5+3 (voltage limit has precedence)
 * 9: powerCap2kW
 *	like 1 but using home power(InWSet) to control the heater
 */

void PheaLing::slotUpdate()
{
	/* already done by own timers
	heater.slotUpdate();
	inverter.slotUpdate();
	*/

	if( !wrtg ){
		wrtg = inverter.getIoHandler()->read("WRtg").toInt();
		heater.slotSetPower(0);
		return;
	}

	QVariant pvTotalW = inverter.getIoHandler()->read("W");
	qint32 pvW = pvTotalW.isValid()? pvTotalW.toInt(): 0;
	qint32 curWHeat = heater.getIoHandler()->read("RelaisStatus").toInt() * 500;
	qint32 curV = inverter.getIoHandler()->read("PhVphA").toFloat() + 0.5;
	qint32 homeW = 0;
	qint32 setW;

	/* start with lower priority (latest set wins) */
	switch( agent ){
	case 3:
	case 4:
	case 5:
		if( curV <= 219 ){
			dhPower.ramp(-500);
		}
		if( curV >= 241 ){
			dhPower.ramp(500);
		}
		break;
	default:
		break;
	}

	/* now medium priority (latest set wins) */
	switch( agent ){
	case 1:
	case 4:
	case 7:
		homeW = 2000;
		setW = pvTotalW.toInt() - homeW;

		if( ! pvTotalW.isValid() ){
			setW = -homeW; // assume 0 W from pv
		}

		if( setW - curWHeat <= -500 ){
			dhPower.ramp(-500);
		}
		if( setW - curWHeat >= 500 ){
			dhPower.ramp(500);
		}
		break;
	case 2:
	case 5:
	case 8:
		//qint32 allowedW = std::min(dinoPercent, emPercent) * wrtg / 100;

		break;
	case 9:
		homeW = 2000;
		heater.slotSetHomePower(homeW + curWHeat - pvW);
		break;
	}

	if( agent >= 6 && agent < 9 ){
		if( curV <= 210 ){
			dhPower.ramp(-1000);
		}
		if( 250 <= curV ){
			dhPower.ramp(1000);
		}
	}

	if( agent ){
		updateHeatPower(curWHeat);
	}
	float nvpPower = homeW + curWHeat - pvTotalW.toInt();
	dino->updateMeter(nvpPower, pvTotalW.isValid()? QUALITY_VALIDITY_QUESTIONABLE: QUALITY_VALIDITY_INVALID);
	if( oli ){
		oli->update(nvpPower);
	}
}

void PheaLing::checkHealth()
{
	/* already done by own timers
	heater.getIoHandler()->checkHealth();
	inverter.getIoHandler()->checkHealth();
	*/
}

void PheaLing::slotSetAgent(qint32 agent)
{
	if( agent < 0 || 9 < agent ){
		qDebug() << "PheaLing: unknown agent chosen";
		return;
	}
	if( agent == 9 ){
		heater.slotSetPower(-1);
	}
	dhPower.reset();
	this->agent = agent;
	dino->setAgent(agent);
}

void PheaLing::updateHeatPower(qint32 curWHeat)
{
//	qint32 softHeatPower = (heatPower + heatPowerTail) / 2;

	qint32 delta = dhPower.eval();

	if( delta != 0 ){
		qint32 adjustedPower = std::max(0, std::min( curWHeat + delta, 4000));
		qDebug() << "PheaLing: set heater power to" << adjustedPower;
		heater.slotSetPower(adjustedPower);
	}

	/* the tail slightly tends downwards because of integer truncation */
	//heatPowerTail = (heatPower * 20 + heatPowerTail * 80) / 100;
}
