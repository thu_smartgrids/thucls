/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "baling.h"

/** Battery Plug-In
 */

#include "baling.h"

#include <QDebug>
#include <QThread>


BaLing::BaLing(QObject *parent) :
	AppLing(parent), battery(this)
{
	maxPower = 0;
}

bool BaLing::setupObject(QString swid, QVariantMap &data)
{
	bool result = AppLing::setupObject(swid, data);
	QVariant value;
	if( data["maxPower"].isValid() ){
		maxPower = data["maxPower"].toFloat();
	}
	return result;
}

bool BaLing::init()
{
	bool okay;

	okay = AppLing::init();
	if( !okay ){
		return false;
	}

	okay =  connect(dino, &ClsProfile::signalSetBatPower, this, &BaLing::slotSetBatPower)
	     && connect(dino, &ClsProfile::signalSetBatMaxSOC, this, &BaLing::slotSetBatMaxSOC)
	     && connect(dino, &ClsProfile::signalSetBatMinSOC, this, &BaLing::slotSetBatMinSOC)
	     && connect(dino, &ClsProfile::signalSetBatOpMod, this, &BaLing::slotSetBatOpMod)

	     && connect(&updatimer, &QTimer::timeout, this, &BaLing::slotUpdate)
	     && connect(&montimer, &QTimer::timeout, &battery, &Restler::checkHealth);

	if( ! okay ){
		qDebug() << "BaLing: init of signals & slots failed";
		return false;
	}

	return true;
}

bool BaLing::connectDevice()
{
	bool okay = battery.init(ip);
	if( ! okay ){
		qDebug() << "initialisation of REST layer failed";
		return false;
	}

	dino->initBat(&battery);
	/* QVariant value = battery.read("REMOVED", "REMOVED").toFloat();
	if( value.isValid() ){
		maxPower = value.toFloat();
	} */

	qDebug() << "BaLing::setupObject successful for " << ip;
	return true;
}

bool BaLing::slotSetBatPower(qint32 pow)
{
	bool okay;

	if( 0 < pow ){
		okay = battery.write("discharge", QVariant(pow));
	} else {
		okay = battery.write("charge", QVariant(abs(pow)));
	}
	if( !okay ) {
		qDebug() << "slotSetBatPower failed";
	}
	return okay;
}

void BaLing::slotSetBatMaxSOC(QVariant value)
{
	qint32 maxSOC = value.toInt();
	bool okay;

	okay = battery.write("maxSOC", QVariant(maxSOC));
	if( !okay ) {
		qDebug() << "slotSetBatMaxSOC failed";
		return;
	}
}

void BaLing::slotSetBatMinSOC(QVariant value)
{
	qint32 minSOC = value.toInt();
	bool okay;

	okay = battery.write("minSOC", QVariant(minSOC));
	if( !okay ) {
		qDebug() << "slotSetBatMinSOC failed";
		return;
	}
}

void BaLing::slotSetBatOpMod(QVariant value)
{
	qint32 opModBat = value.toInt();
	bool okay;

	okay = battery.write("OpModBat", QVariant(opModBat));
	if( !okay ) {
		qDebug() << "slotSetBatOpMod failed";
		return;
	}
}

void BaLing::slotUpdate()
{
	dino->updateBat(&battery);
	if( oli != NULL ){
		oli->update(-battery.read("Pac_total_W", "status").toFloat());
	}
}

int BaLing::setPercent(int percent)
{
	if( maxPower < 1 ){
		return SETEM_WRITE_FAILED;
	}

	bool okay = slotSetBatPower(-(percent * maxPower)/100);

	if( !okay ){
		return SETEM_WRITE_FAILED;
	}

	return percent;
}

