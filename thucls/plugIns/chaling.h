/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef CHALING_H
#define CHALING_H

#include "appling.h"
#include "../io/modsun/mbphoenixccontrol.h"

/**
 * E-mobile Charger Application PlugIn
 */
class Chaling : public AppLing
{
	Q_OBJECT
public:
	explicit Chaling(QObject *parent = 0);

	bool init();
	bool connectDevice();

	QString getName() { return "EV Charger Plug-In"; }
	GenIo* getIoHandler() { return &modler; }
	QString getShortInfo() { return "Communicate with Electric Vehicle Charger via proprietary Modbus interface and foward values using IEC 61850."; }

signals:

public slots:
//	void slotSetPercent(uint percent);
	void slotSetMaxI(QVariant value);
	void slotUpdate();

protected:
	virtual int setPercent(int percent);

	MbPhoenixCControl modler;

private:
};

#endif // CHALING_H
