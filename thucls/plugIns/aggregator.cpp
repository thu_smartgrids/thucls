/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU
 ** Lesser General Public License version 3. A copy of the license
 ** should be part of this distribution package.
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details.
 **
 *******************************************************************/

#include "aggregator.h"
//#include <QTimer>
#include <QDebug>

int SetVal::eval()
{
	tail = (delta * 22 + tail * 80) / 100;
	qDebug() << "tail is" << tail;
	if( tail < -500 ){
		tail += 500;
		return -500;
	}
	if( tail > 500 ){
		tail -= 500;
		return 500;
	}
	delta = 0;
	return 0;
}

NoDevice::NoDevice(QObject* parent) : GenIo (parent)
{
}
