/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "hasuplug.h"

#include "sling.h"
#include "chaling.h"
#include "baling.h"
#include "healing.h"
#include "jaling.h"
#include "pbling.h"
#include "phealing.h"
#include "helpling.h"
#include "sampling.h"
#include "sloing.h"
#include "targetlayer.h"
#include "../io/uplink/tunnler.h"
#include "../uTest/basevaltest.h"
#include "../uTest/iotest.h"
#include "../uTest/timtest.h"
#include <QTimer>
#include <unistd.h> // todo: remove only for getpid()

HasuPlug::HasuPlug(QObject *parent) :
	QObject(parent)
{
	static quint32 nInstances = 0;
	nInstances++;
	impl = NULL;
	meter = NULL;
	TargetLayer::initLogger(this);
	//qDebug() << "created HasuPlug" << nInstances;
}

HasuPlug::~HasuPlug()
{
	qDebug() << "delete HasuPlug";
	if( impl != NULL ){
		delete impl;
	}
	if( meter != NULL ){
		delete meter;
	}
}

GeLing* HasuPlug::create(QString uri)
{
	uri = uri.toLower();
	if( uri.startsWith("hsu/") ) {
		uri = uri.remove(0, uri.indexOf('/')+1);

		if( uri.startsWith("pv") ) {
			return new Sling(this);
		}
		if( uri.startsWith("slog") ){
			return new Sloing(this);
		}
		if( uri.startsWith("emob") ) {
			return new Chaling(this);
		}
		if( uri.startsWith("bat") ) {
			return new BaLing(this);
		}
		if( uri.startsWith("heat") ) {
			return new HeaLing(this);
		}
		if( uri.startsWith("p2bat") ) {
			return new PbLing(this);
		}
		if( uri.startsWith("p2heat") ) {
			return new PheaLing(this);
		}
		if( uri.startsWith("jan") ) {
			return new JaLing(this);
		}
		if( uri.startsWith("basevaltest") ) {
			return new BasevalTest(this);
		}
		if( uri.startsWith("hell") ) {
			return new HelPling(this);
		}
		if( uri.startsWith("modlingtest") ) {
			// return new Modling(parent, params);
		}
		if( uri.startsWith("sample") ) {
			return new Sampling(this);
		}
	}

	qDebug() << "no HSU function specified";
	return NULL;
}

/**
 * like create but limited to "read-only" plug ins
 */
GeLing* HasuPlug::createMeter(QString uri)
{
	uri = uri.toLower();
	if( uri.startsWith("hsu/") ) {
		uri = uri.remove(0, uri.indexOf('/')+1);

		if( uri.startsWith("jan") ) {
			return new JaLing(this);
		}
		if( uri.startsWith("hell") ) {
			return new HelPling(this);
		}
	}

	qDebug() << "no HSU meter specified";
	return NULL;
}

/**
 * (re)-initialises an application plug-in depending on given data.
 * Preserves initialisation order of implementation namely init(), setup(), start()
 */
bool HasuPlug::setupObject(QString &swid, QVariantMap &data)
{
	QVariant kind = data["kind"];
	QString uri;
	bool okay;

	//data = TargetLayer::readConfigFile(); //use fixed config for testing only

	qInfo() << "HasuPlug::setupObject called for " << swid << " pid is " << getpid();
	qInfo() << data;

	if( data["timTest"] == true ) {
		qDebug() << "HasuPlug::timing benchmark";
		TimTest* test = new TimTest();
		test->interactiveTest();
		delete test;
	}

	if( data.contains("meter") && data["meter"].type() == QVariant::Map ){
		bool meterOkay = false;
		QVariantMap meterData = data["meter"].toMap();
		QString meterUri = meterData["kind"].toString();
		if( meter == NULL ){
			meter = createMeter(meterUri);
			if( meter != NULL ){
				meter->init();
			}
		}
		if( meter != NULL ){
			meterOkay = meter->setupObject("meter", meterData);
		}

		if( meterOkay ){
			qDebug() << "init of meter successfull";
		} else {
			qDebug() << "init of meter not successfull";
		}
	}

	if( data["chan"] == true ){
		qDebug() << "Tunnler::start";
		Tunnler::start();
	} else {
		//qDebug() << "Tunnler::stop";
		Tunnler::stop();
	}

	if( kind.type() != QVariant::String ) {
		kind = QVariant("unknown");
	}

	uri = kind.toString();
	//qDebug() << "kind is" << uri;

	if( impl == NULL ){
		impl = create(uri);

		if( impl != NULL ){
			qInfo() << "PlugIn created:" << impl->getName();
			qInfo() << impl->getShortInfo();
			okay = impl->init();
			if( !okay ){
				qDebug() << "PlugIn init failed";
				return false;
			}
		}
	}

	if( kind == "unknown" ){
		return false;
	}

	if( impl != NULL ){
		okay = impl->setupObject(swid, data);

		if( !okay ){
			return false;
		}

		QTimer::singleShot(0, this, SLOT(slotStart()) );

#ifndef PLUGIN
		if( data["ioTest"].toBool() ){
			QTimer::singleShot(100, this, SLOT(slotIoTest()));
		}
#endif
		return true;
	}
	return false;
}

void HasuPlug::slotStart()
{
	impl->start();
	if( meter != NULL ){
		meter->start();
	}
}

void HasuPlug::slotIoTest()
{
	#ifndef PLUGIN
		IoTest::interactiveTest(impl->getName(), impl->getIoHandler());
	#endif
}

void HasuPlug::slotSetPercent(QString id, QString swid, uint percent)
{
	if( impl == NULL) {
		sendPluginFAIL(0, "unknown CLS component");
		return;
	}
	emit impl->setEmPercent(id, swid, percent);
}
