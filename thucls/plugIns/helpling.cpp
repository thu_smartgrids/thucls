/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "helpling.h"

#include <QIODevice>
#include <QCoreApplication>
#include <QTimer>
#include <QNetworkInterface>
#include "targetlayer.h"

HelPling::HelPling(QObject* parent) :
	GeLing(parent), socket(this), usock(this)
{
}

HelPling::~HelPling()
{
	socket.close();
}

/* Gegenstelle:
 *
 * sudo socat -v tcp-listen:1234 reuseaddr
 * bzw. Windows:
 * ncat -l 1234
 */

bool HelPling::init()
{
	qDebug() << getName();
	TargetLayer::getSwRevision();
	return true;
}

void HelPling::start()
{
	socket.connectToHost(ip, 1234, QIODevice::WriteOnly);
	//socket.open(QIODevice::WriteOnly);
	if( !socket.waitForConnected(1000) ){
		qDebug() << "HelPling::init() could not connect to " << ip << ": 1234";
		QTimer::singleShot(2000, this, SLOT(start()));
		return;
	}

	QString out = "Hello World!\n";
//	out = "I am " + QCoreApplication::applicationName() + "\n";
	out += "Running at " + TargetLayer::getMac() + "\n";
	out += "IP is " + TargetLayer::getIP() + "\n";
//	//out+= "I live in " + QString(" in ") + QCoreApplication::applicationFilePath() + "\n";
	socket.write(out.toLatin1().data());
	socket.flush();

	//TODO: QHostAddress:parseSubnet(123.123.123.0/24); // how to get the subnet?
}

void HelPling::setEmPercent(QString id, uint percent)
{
	socket.write("Set to ");
	socket.write(QString::number(percent).toLatin1());
	socket.write(" % \n");
	socket.flush();
	sendPluginOK;
}
