/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "jaling.h"

JaLing::JaLing(QObject *parent): AppLing(parent), meter(this)
{
}

bool JaLing::init()
{
	bool okay;

	okay = AppLing::init();
	if( ! okay ){
		return false;
	}

	okay =  connect(&updatimer, &QTimer::timeout, this, &JaLing::slotUpdate)
	     && connect(&montimer, &QTimer::timeout, &meter, &Modler::checkHealth);

	if( ! okay ){
		qDebug() << "JaLing: init of signals & slots failed";
		return false;
	}

	return true;
}

bool JaLing::connectDevice()
{
	bool okay = meter.init(ip, deviceId);
	if( ! okay ){
		qDebug() << "initialisation of Modbus layer failed";
		return false;
	}

	meter.addBlock("measures1", 25000, "PhV_phsA", "Hz");
	meter.addBlock("ident1", 25000, "SerialNumber", "HardwareIndex");
	dino->initJanitzaMeter(&meter);
	meter.enterCyclingMode();

	qDebug() << "JaLing::setupObject successful for " << ip;
	return true;

}

void JaLing::slotUpdate()
{
	dino->updateJanitzaMeter(&meter);
}

void JaLing::updateHealth()
{
	/* do nothing */
}
