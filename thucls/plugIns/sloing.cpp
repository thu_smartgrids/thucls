#include "sloing.h"
#include <QDateTime>

Sloing::Sloing(QObject* parent) : AppLing(parent), modler(this)
{
	currentPercent = 0xFF; /* invalid value */
}

bool Sloing::init()
{
	bool okay;

	okay = AppLing::init();
	if( ! okay ){
		return false;
	}

	okay =  connect(dino, &ClsProfile::signalSetPercent, this, &Sloing::slotSetDinoPercent)
	     && connect(&updatimer, &QTimer::timeout, this, &Sloing::slotUpdate)
	     && connect(&montimer, &QTimer::timeout, &modler, &Modler::checkHealth);

	if( ! okay ){
		qDebug() << "Sloing: init of signals & slots failed";
		return false;
	}

	return true;
}

bool Sloing::connectDevice()
{
	bool okay = modler.init(ip, deviceId);
	if( ! okay ){
		qDebug() << "initialisation of Modbus layer failed";
		return false;
	}

#warning TODO: will it work without modler.addBlock?

	dino->initSLog(&modler);
	modler.enterCyclingMode();

	qDebug() << "Sloing::setupObject successful for " << ip;
	return true;
}

int Sloing::setPercent(int percent)
{
	if( percent < 0 || 100 < percent ){
		return SETEM_WRITE_FAILED;
	}

	currentPercent = percent;
	bool okay = slotUpdatePwr();
	setimer.start(180000);  // refresh each 3 min
	endtimer.start();

	if( !okay ){
		return SETEM_WRITE_FAILED;
	}

	QVariant reply = modler.read("PLimitPercN");
	if( !reply.isValid() ){
		return SETEM_READ_FAILED;
	}
	return reply.toInt();
}

void Sloing::slotSetDinoPercent(QVariant value)
{
	uint percent = value.toUInt();
	bool okay = modler.write("PLimitPerc", QVariant(percent));
	okay &= modler.write("WatchDog_Tag", QVariant(1));

	updateDinoPercent(percent);

	qDebug() << "slotSetDinoPercent"  << percent << (okay? " okay": " failed");
}

void Sloing::slotUpdate()
{
	dino->updateSLog(&modler);
/*	if( oli ){
		oli->update(-modler.read("W").toFloat(), modler.read("WMaxLimPct"));
	}*/
}

bool Sloing::slotUpdatePwr()
{
	if( 100 < currentPercent ){
		return false;
	}

	bool okay = modler.write("PLimitType", 2);
	#warning TODO: use 3 by default?
	okay &= modler.write("PLimitPerc", currentPercent);
	okay &= modler.write("WatchDog_Tag", quint32(QDateTime::currentSecsSinceEpoch()) );

	return okay;
}
