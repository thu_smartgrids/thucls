/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU
 ** Lesser General Public License version 3. A copy of the license
 ** should be part of this distribution package.
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details.
 **
 *******************************************************************/

#ifndef AGGREGATOR_H
#define AGGREGATOR_H

#include <QObject>
#include "../io/genio.h"

typedef enum {
	avgVal = 0,
	minVal = 1,
	maxVal = 2
} Aggregation;

class SetVal
{
	/* initial implementation with range of -4000 .. +4000 at steps of 500 */
public:
	SetVal() {delta = 0; tail = 0;}
	void reset() {delta = 0; tail = 0;}
	//void vote(int expected, Aggregation agg = avgVal) { val = expected; }
	void ramp(int step) { delta += step; }
	int eval();

protected:
	/*int votes[2];
	Aggregation aggVotes[2];
	int nVotes;*/
	int delta, tail;
};


class NoDevice : public GenIo
{
public:
	explicit NoDevice(QObject *parent = 0);

	QString getName() { return "Aggregator"; }

	bool readBlock(QString block) { Q_UNUSED(block) return false; }
	QVariant read(QString element, QString block = NULL) { Q_UNUSED(element) Q_UNUSED(block); return QVariant::Invalid; }
	bool write(QString element, QVariant value) { Q_UNUSED(element) Q_UNUSED(value); return false; }
	bool recover(quint16 failrate) { Q_UNUSED(failrate); return true; }
};

#endif // AGGREGATOR_H
