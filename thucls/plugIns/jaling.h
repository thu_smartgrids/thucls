/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef JALING_H
#define JALING_H

#include <QObject>
#include "appling.h"
#include "../io/modsun/mbjanitzaumg.h"

class JaLing : public AppLing
{
	Q_OBJECT
public:
	explicit JaLing(QObject *parent = 0);

	bool init();
	bool connectDevice();

	QString getName() { return "Janitza meter Plug-In"; }
	GenIo* getIoHandler() { return &meter; }
	QString getShortInfo() { return "Read meter values from Janitza UMG 96RM and foward using IEC 61850."; }

public slots:
	void slotUpdate();
	void updateHealth();

protected:
	MbJanitzaUmg meter;

};

#endif // JALING_H
