/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "healing.h"
#include <QDebug>

HeaLing::HeaLing(QObject* parent) :
	AppLing(parent), modler(this)
{
	emGranularity = 13;

	pwrValue = 0;
}

bool HeaLing::init()
{
	bool okay;

	okay = AppLing::init();
	if( !okay ){
		return false;
	}

	okay =  connect(dino, &ClsProfile::signalSetHeatPower, this, &HeaLing::slotSetPower)
	     && connect(dino, &ClsProfile::signalSetHomePower, this, &HeaLing::slotSetHomePower)
	     && connect(dino, &ClsProfile::signalSetHeatOpMod, this, &HeaLing::slotSetOpMod)
	     && connect(dino, &ClsProfile::signalSetHeatMaxTmp, this, &HeaLing::slotSetMaxTmp)
	     && connect(dino, &ClsProfile::signalSetHeatMinTmp, this, &HeaLing::slotSetMinTmp)
	     && connect(dino, &ClsProfile::signalSetHeatSetTmp, this, &HeaLing::slotSetTmp)

	     && connect(&updatimer, &QTimer::timeout, this, &HeaLing::slotUpdate)
	     && connect(&montimer,  &QTimer::timeout, &modler, &Modler::checkHealth);

	if( !okay ){
		qDebug() << "HeaLing: init of signals & slots failed";
		return false;
	}

	return true;
}

bool HeaLing::connectDevice()
{
	bool okay = modler.init(ip, deviceId);
	if( ! okay ) {
		qDebug() << "initialisation of Modbus layer failed";
		return false;
	}
	modler.addBlock("ident1", 25000, "ManufacturerID", "FirmwareVersion");
	modler.addBlock("measures1", 25000, "ActualTemperaturePCB", "TemperatureNominalValue");
	modler.addBlock("measures2", 25000, "PowerNominalValue", "HomeTotalPower");
	modler.addBlock("measures3", 25000, "TotalOperatingSeconds", "UserTemperatureNominalValue");

	dino->initHeat(&modler);
	modler.enterCyclingMode();

	qDebug() << "HeaLing::setupObject successful for " << ip;
	return true;
}

int HeaLing::setPercent(int percent)
{
	if( percent < 0 || 100 < percent ){
		return SETEM_WRITE_FAILED;
	}

	bool okay = slotSetPower(percent * 35); /* 3500W = 100% */
	if( !okay ){
		return SETEM_WRITE_FAILED;
	}

	QVariant power = modler.read("PowerNominalValue");
	if( !power.isValid() ){
		return SETEM_READ_FAILED;
	}
	if( power == -1 ){
		return 100; /* operation mode: self optimization */
	}
	return power.toInt() / 35;
}

bool HeaLing::slotSetPower(QVariant value)
{
	qint32 pwr = value.toInt();
	if( pwr < -1 || 4000 < pwr ){
		qDebug() << "invalid value for PowerNominalValue";
		return false;
	}

	pwrValue = pwr;
	bool result = slotUpdatePwr();
	setimer.start(30000);  // refresh each 30s
	endtimer.start();
	return result;
}

void HeaLing::slotSetOpMod(QVariant value)
{
	qint32 OpModHeat = value.toInt();
	qint16 pNom;
	pNom = modler.read("PowerNominalValue").toInt();
	if( OpModHeat < 1 || 2 < OpModHeat ){
		qDebug() << "invalid value for Operating Mode";
		return;
	}else if(OpModHeat==2){
		pwrValue =-1;
		slotUpdatePwr();
	}else if(OpModHeat==1 && pNom == -1){
		pwrValue = 0;
		slotUpdatePwr();
	}
}

void HeaLing::slotSetMaxTmp(QVariant value)
{
	quint16 tmp = value.toInt();
	if( tmp < 60 || 80 < tmp ){
		qDebug() << "invalid value for TemperatureMaxValue";
		return;
	}

	maxTmp = tmp;
	modler.write("TemperatureMaxValue", QVariant(maxTmp));
}

void HeaLing::slotSetMinTmp(QVariant value)
{
	quint16 tmp = value.toInt();
	if(80 < tmp){
		qDebug() << "invalid value for TemperatureMinValue";
		return;
	}

	minTmp = tmp;
	modler.write("TemperatureMinValue", QVariant(minTmp));
}

void HeaLing::slotSetTmp(QVariant value)
{
	quint16 tmp = value.toInt();
	quint16 userTmp;
	userTmp = modler.read("UserTemperatureNominalValue").toInt();
	if( tmp!=0 && (tmp<40||80 < tmp)){
		qDebug() << "invalid value for TemperatureNominalValue";
		return;
	} else if(tmp>userTmp){
		tmp = userTmp;
	}

	setTmp = tmp;
	modler.write("TemperatureNominalValue", QVariant(setTmp));
}

void HeaLing::slotSetHomePower(QVariant value)
{
	modler.write("HomeTotalPower", value);
}

bool HeaLing::slotUpdatePwr()
{
	qDebug() << "refresh power value" << pwrValue;
	return modler.write("PowerNominalValue", QVariant(pwrValue));
}

void HeaLing::slotUpdate()
{
	dino->updateHeat(&modler);
	if( oli ){
		oli->update(modler.read("RelaisStatus").toInt()*500);
	}
}

