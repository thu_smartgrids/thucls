/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef HEALING_H
#define HEALING_H

#include <QTimer>

#include "appling.h"
#include "../io/modsun/mbsmartheater.h"

/**
 * Smart Heater Application PlugIn.
 */
class HeaLing  : public AppLing
{
public:
	explicit HeaLing(QObject *parent = 0);

	bool init();
	bool connectDevice();

	QString getName() { return "Smart Heater Plug-In"; }
	GenIo* getIoHandler() { return &modler; }
	QString getShortInfo() { return "Communicate with Smart Heater via proprietary Modbus interface and foward values using IEC 61850."; }

signals:

public slots:
	bool slotSetPower(QVariant value);
	void slotSetOpMod(QVariant value);
	void slotSetMaxTmp(QVariant value);
	void slotSetMinTmp(QVariant value);
	void slotSetTmp(QVariant value);
	void slotSetHomePower(QVariant value);
	void slotUpdate();

protected slots:
	bool slotUpdatePwr();

protected:
	virtual int setPercent(int percent);

	qint16 pwrValue;
	quint16 maxTmp, minTmp, setTmp, userTmp;

	MbSmartHeater modler;
};

#endif // HEALING_H
