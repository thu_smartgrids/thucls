/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU
 ** Lesser General Public License version 3. A copy of the license
 ** should be part of this distribution package.
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details.
 **
 *******************************************************************/

#include "pbling.h"
#include "targetlayer.h"

PbLing::PbLing(QObject *parent) : AppLing(parent) , aggregator(this) , inverter(this) , battery(this) , heater(this)
{
	agent = 1;
}

bool PbLing::setupObject(QString swid, QVariantMap &data)
{
	bool result = AppLing::setupObject(swid, data);
	result &= inverter.setupObject(swid, data);

	QVariant ipHeat = data["IPheat"];
	data.remove("IPheat");
	QVariant batHeat = data["IPbat"];
	data.remove("IPbat");

	data.remove("IP");
	data["IP"] = ipHeat;

	result &= heater.setupObject(swid, data);

	data.remove("IP");
	data["IP"] = batHeat;

	result &= battery.setupObject(swid, data);
	return result;
}

bool PbLing::init()
{
	bool okay = heater.init()
		  & battery.init()
		  & inverter.init();

	bool okay2 = AppLing::init();
	dino->initMeter( "HS-Ulm Smart Grid Research Group",
			 TargetLayer::getSwRevision(),
			 "aggregation from PV scenario, battery and heater" );

	okay2 &= connect(&montimer, &QTimer::timeout, this, &PbLing::slotSetVals) != NULL;
	okay2 &= connect(&updatimer, &QTimer::timeout, this, &PbLing::slotUpdate) != NULL;
	okay2 &= connect(dino, &ClsProfile::signalSetAgent, this, &PbLing::slotSetAgent) != NULL;
	slotSetAgent(agent);

	qDebug() << "PbLing.init" << okay << okay2;
	return okay && okay2;
}

void PbLing::start()
{
	heater.start();
	battery.start();
	inverter.start();

	AppLing::start();
}

bool PbLing::connectDevice()
{
	return true;
}

void PbLing::slotSetVals()
{
	double timeRes = 90 / 3600.;

	//QVariant pvTotalW = inverter.getIoHandler()->read("W");
	double intPower = load.getWatts();
	double batSoc = battery.getIoHandler()->read("USOC", "status").toDouble();
	double batCap = 2000.0;
	//qint32 curWHeat = heater.getIoHandler()->read("RelaisStatus").toInt() * 500;
	//qint32 curWBat = - battery.getIoHandler()->read("Pac_total_W", "status").toDouble();
	//qint32 curV = inverter.getIoHandler()->read("PhVphA").toFloat() + 0.5;

	/*** calculation from Home_Storage_Lab.py get_p_kw ***/
	qint32 fin = -intPower;
	QString batState;
	qint32 batSetP;

	QString heatState;
	double heatSetP = 0;

	switch( agent ){
	case 1:
		if( fin > 0 ){ // excessive energy is available
			if( batSoc + (fin * timeRes * 100.)/batCap <= 100 ){ // storage is not full and will not get overfilled
				batState = "charge";
				batSetP = fin;
			} else if( batSoc < 100 && batSoc + (fin * timeRes * 100.)/batCap >= 100 ){
				batState = "charge-remaining";
				batSetP = (100 - batSoc) * batCap /(100 * timeRes);
			} else { // storage is full
				batState = "idle";
				batSetP = 0.;
			}
		} else if( fin < 0 ){ // excessive consumption is available
			if( batSoc + (fin * timeRes * 100.)/batCap >= 0 ){ // storage is not empty and will not under discharged
				batState = "discharge";
				batSetP = fin;
			} else if( batSoc > 0 && (batSoc + (fin * timeRes * 100.)/batCap <= 0) ){
				batState = "discharge-remaining";
				batSetP = -batSoc * batCap / (100 * timeRes);
			} else { // storage is empty
				batState = "idle";
				batSetP = 0.;
			}
		} else { // feed-in = consumption
			batState = "idle";
			batSetP = 0.;
		}

		batSetP = std::max(batSetP, -2000);
		batSetP = std::min(batSetP, 2000);
		qDebug() << "Battery state: " << batState << "soc" << batSoc;
		qDebug() << "Battery set power in W: " << batSetP;

		/*** calculation from P-t_H_Lab.py ***/


		fin = -intPower - batSetP;
		if( fin > 0 ){ // surrplus power is available
			heatSetP = int(abs(fin)/500)*500; //Rund fin allways lower and in 0.5kW steps
			if( heatSetP > 0 ){
				heatState = "heating";
			}
		}
		else {
			heatState = "idle";
			heatSetP = 0;
		}

		heatSetP = std::max(heatSetP, 0.);
		heatSetP = std::min(heatSetP, 3500.); //limit p_kw between 0.5 an 3.5kW

		qDebug() << "PtH state:" << heatState;
		qDebug() << "PtH set power in W:" << heatSetP;

		/***************/

		heater.slotSetPower(heatSetP);
		battery.slotSetBatPower(-batSetP);
		break;
	default:
		break;
	}
}

void PbLing::slotUpdate()
{
	/* already done by own timers
	heater.slotUpdate();
	inverter.slotUpdate();
	*/

	//QVariant pvTotalW = inverter.getIoHandler()->read("W");
	float intPower = load.getWatts();
	float curWHeat = heater.getIoHandler()->read("RelaisStatus").toInt() * 500;
	float curWBat = - battery.getIoHandler()->read("Pac_total_W", "status").toFloat();
	//qint32 curV = inverter.getIoHandler()->read("PhVphA").toFloat() + 0.5;
	//qint32 homeW = use only one number;

	float nvpPower = intPower + curWBat + curWHeat;
	dino->updateMeter(nvpPower, QUALITY_VALIDITY_GOOD);
	if( oli ){
		oli->update(nvpPower);
	}
}

void PbLing::checkHealth()
{
	/* already done by own timers
	heater.getIoHandler()->checkHealth();
	inverter.getIoHandler()->checkHealth();
	*/
}

void PbLing::slotSetAgent(qint32 agent)
{
	if( agent < 0 || 1 < agent ){
		qDebug() << "PbLing: unknown agent chosen";
		return;
	}
	this->agent = agent;
	dino->setAgent(agent);
}
