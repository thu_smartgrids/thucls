/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef HELPLING_H
#define HELPLING_H

#include <QString>
#include "geling.h"
#include <QTcpSocket>
#include <QUdpSocket>

/**
 * Hello World Plug-In for testing purposes.
 */
class HelPling : public GeLing
{
	Q_OBJECT
public:
	explicit HelPling(QObject *parent = 0);
	~HelPling();

	void setEmPercent(QString id, uint percent);

	bool init();
	void start();

	QString getName() { return "Hello World Plug-In"; }
	GenIo* getIoHandler() { return NULL; }
	QString getShortInfo() { return "Simple PlugIn sending \"Hello World\" in plain text to port 10035 of given IP"; }

public slots:

protected:
	QTcpSocket socket;
	QUdpSocket usock;
};


#endif // HELPLING_H
