The following configuration proposals may be of interest within the Smart Grids Lab.
Keep in mind, the QT json parser does not support comments.
BE CAREFULL: QT json parser fails on trailing comma.

Example for valid config.json:
{
    "kind": "hsu/pv",
    "IP":   "192.168.2.168"
}

Alternatively place the json within the environment variable CLS_CONFIG.
The setting within the environment variable overrules the content of config file.
When environment variable does not contain a valid json structure then the program
will fall back to the config file.

Testing purposes:
        "kind": "hsu/basevaltest",
        "IP": "127.0.0.1",
	"interactiveTest": true,
	"timTest": true,

        "kind": "hsu/hell",
        "IP": "192.168.2.202",
	"IP": "127.0.0.1",     // local

        "kind": "hsu/sample"   // to test iec61850 without device

Activate the "transparent channel"
        "chan": true

SunSpec compatible PV inverter:
        "kind": "hsu/pv",
        "IP": "192.168.2.168",
	"deviceID": 126,        // SMA inverter with "SpeedWire"
        "endPwrRefresh": 10

SolarLog PV data logger:
        "kind": "hsu/slog",
        "IP": "192.168.2.169",
        "endPwrRefresh": 10

Emob charger:
        "kind": "hsu/emob",
	"IP": "192.168.2.166",
	"IP": "192.168.2.167",

Power to heat:
        "kind": "hsu/heat",
	"IP": "192.168.2.147",
        "endPwrRefresh": 10

PV power to heat/bat:
        "kind": "hsu/p2bat",
	"IP":   "192.168.2.168",
        "IPheat": "192.168.2.147",
        "IPbat": "192.168.2.136"

PV power to bat:
        "kind": "hsu/p2heat",  //IPs like above

Sonnenbatterie:
        "kind": "hsu/bat",
        "IP": "192.168.2.136",
        "maxPower": 1500

Janitza Messbox:
        "kind": "hsu/jan",	// no other device
        "IP":   "192.168.2.4",

        "meter": {
	        "kind": "hsu/jan",
		"IP":   "192.168.2.4"
        }

Uplink to Oli-box:
        "oli": "192.168.2.159"
        "oliConf": 3
