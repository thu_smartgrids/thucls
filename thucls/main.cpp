/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include <QCoreApplication>
#include <QDebug>
#include <QHostAddress>
#include <QString>
#include <QTextStream>

extern "C" {
#include "errno.h"
}

#include "plugIns/hasuplug.h"
#include "targetlayer.h"
//#include "uTest/reinitest.h"

#ifndef PLUGIN
QTextStream cout(stdout);
QTextStream cin(stdin);

/*
 * just a wrapper around the plugin
 */
int main(int argc, char *argv[])
{
	QString inStr, eip;
	QHostAddress validateIp;
	HasuPlug plin;
	bool okay;
	quint32 inNum;

	// const char* config = "{\"kind\": \"hsu/pv\", \"IP\": \"192.168.2.168\"}";
	// data = QJsonDocument::fromJson(config).object().toVariantMap();

	QCoreApplication a(argc, argv);

	QVariantMap data = TargetLayer::readConfigFile();

	inStr = a.arguments().first();

	if( inStr == "-h" || inStr == "/h" ){
		cout << "This application communicates with a SunSpec inverter and forwards its parameters" << endl;
		cout << "using IEC61850 and/or other CLS-App functionality depending on config.json." << endl;
		cout << "parameters: hs-ulm-sunspec.exe <IP address>" << endl;
		cout << "passing an IP address will overwrite the configured one" << endl;
	}
	if( !inStr.isEmpty() && validateIp.setAddress(inStr) ){
		data["IP"] = inStr;
	}
	errno = 0; /* whatever... */


	okay = plin.setupObject(eip, data);
#ifdef INTERACTIVE
	if( okay ){
		TargetLayer::waitForOthers(1000);
		cout << "enter percentage (0..100) or 't' for test:" << endl;
		while( 1 ) {
			inStr = cin.readLine();

			inNum = inStr.toInt(&okay);
			if( okay ) {
				plin.slotSetPercent("console", eip, inNum);
				TargetLayer::waitForOthers(1000);
			} else {
				if( inStr.toLower() == "t" ) {
					data.insert("ioTest", true);
					plin.setupObject(eip, data);
					TargetLayer::waitForOthers(1000);
				}
				break;
			}
		}
	}
#endif

//	ReiniTest test(&plin);

//	cout << "goodbye" << endl;
//	return 0;

	cout << "run ied server" << endl;
	return a.exec(); /* handle timer events */
}
#endif
