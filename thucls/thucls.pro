#-------------------------------------------------
#
# Project created by QtCreator 2017-01-27T11:55:56
#
#-------------------------------------------------

QT       += core network

QT       -= gui

#for ZENNER Hessware build-chain use the target name: hs-ulm-sunspec
TARGET = thucls
#DEFINES += PLUGIN
#DEFINES += INTERACTIVE
DEFINES += QT_MESSAGELOGCONTEXT

contains(DEFINES, PLUGIN) {
    message(Compiling as plugin)
    CONFIG   += plugin
    TEMPLATE = lib

    target.path = $${PREFIX}/lib/control-client
    INSTALLS+=target
} else {
    message(Compiling as application)
    message($$OUT_PWD)
    CONFIG += console
    TEMPLATE = app

    #copy config.json and config_readme.txt to the output folder
    CONFIG(debug, debug|release) {
        DESTDIR = debug
    } else {
        DESTDIR = release
    }
    copydata.commands = $(COPY_FILE) \"$$shell_path($$PWD/config.json)\" \"$$shell_path($$DESTDIR)\" && \
			$(COPY_FILE) \"$$shell_path($$PWD/config_readme.txt)\" \"$$shell_path($$DESTDIR)\"
    first.depends = $(first) copydata
    export(first.depends)
    export(copydata.commands)
    QMAKE_EXTRA_TARGETS += first copydata
}

CONFIG   -= app_bundle
unix:CONFIG += unversioned_libname

# replaces += $$PWD/../../bsw-base/usr/include
INCLUDEPATH += $${SYSROOT}/$${PREFIX}/include
INCLUDEPATH += $${SYSROOT}/$${PREFIX}/include/control-client

INCLUDEPATH += io/soefler/libiec61850/src/common/inc
INCLUDEPATH += io/soefler/libiec61850/src/mms/inc
INCLUDEPATH += io/soefler/libiec61850/hal/inc
INCLUDEPATH += io/soefler/libiec61850/src/iec61850/inc
INCLUDEPATH += io/soefler/libiec61850/src/logging

linux:LIBS += -L${SYSROOT}/$${PREFIX}/lib
win32:LIBS += -lWS2_32
#linux:LIBS += -L${SYSROOT}/$${PREFIX}/lib -lmodbus
LIBS += -L$$PWD/../lib
LIBS += -liec61850
#linux:LIBS += -L${SYSROOT}/$${PREFIX}/lib/control-client -lhellout


SOURCES += \
    io/modsun/modbus/modbus-data.c \
    io/modsun/modbus/modbus-tcp.c \
    io/modsun/modbus/modbus.c \
    io/modsun/modbus/modbus-rtu.c

HEADERS += \
    io/modsun/modbus/modbus-private.h \
    io/modsun/modbus/modbus-tcp-private.h \
    io/modsun/modbus/modbus-version.h \
    io/modsun/modbus/modbus.h \
    io/modsun/modbus/modbus-tcp.h \
    io/modsun/modbus/modbus-rtu-private.h \
    io/modsun/modbus/modbus-rtu.h

SOURCES += \
    io/modsun/mbslogger.cpp \
    plugIns/sloing.cpp \
    io/modsun/sunslayer.cpp \
    io/rest/restsonneneco8.cpp \
    plugIns/aggregator.cpp \
    plugIns/hasuplug.cpp \
    plugIns/helpling.cpp \
    main.cpp \
    io/modsun/modler.cpp \
    plugIns/pbling.cpp \
    uTest/basevaltest.cpp \
    targetlayer.cpp \
    io/soefler/soefler.cpp \
    io/soefler/clsProfile.cpp \
    plugIns/chaling.cpp \
    io/modsun/mbphoenixccontrol.cpp \
    io/genio.cpp \
    io/rest/restler.cpp \
    plugIns/baling.cpp \
    uTest/loadscenario.cpp \
    uTest/timtest.cpp \
    plugIns/geling.cpp \
    uTest/iotest.cpp \
    io/uplink/tunnler.cpp \
    plugIns/healing.cpp \
    io/modsun/mbsmartheater.cpp \
    plugIns/appling.cpp \
    plugIns/sling.cpp \
    plugIns/phealing.cpp \
    io/modsun/mbjanitzaumg.cpp \
    plugIns/jaling.cpp \
    plugIns/sampling.cpp \
    io/soefler/static_model.c \
    io/soefler/scheler.cpp \
    io/modsun/mbolibox.cpp

HEADERS += \
    io/modsun/mbslogger.h \
    plugIns/sloing.h \
    io/rest/restsonneneco8.h \
    plugIns/aggregator.h \
    plugIns/hasuplug.h \
    plugIns/helpling.h \
    io/modsun/sunsmodls.h \
    io/modsun/sunsel.h \
    io/modsun/sunslayer.h \
    io/modsun/modler.h \
    io/modsun/mbel.h \
    plugIns/pbling.h \
    uTest/basevaltest.h \
    targetlayer.h \
    io/soefler/soefler.h \
    io/soefler/clsProfile.h \
    plugIns/chaling.h \
    io/modsun/mbphoenixccontrol.h \
    io/genio.h \
    io/rest/restler.h \
    plugIns/baling.h \
    uTest/loadscenario.h \
    uTest/timtest.h \
    plugIns/geling.h \
    uTest/iotest.h \
    io/uplink/tunnler.h \
    plugIns/healing.h \
    io/modsun/mbsmartheater.h \
    plugIns/appling.h \
    plugIns/sling.h \
    plugIns/phealing.h \
    io/modsun/mbjanitzaumg.h \
    plugIns/jaling.h \
    plugIns/sampling.h \
    io/soefler/static_model.h \
    io/soefler/scheler.h \
    io/modsun/mbolibox.h

OTHER_FILES += \
    config.json \
    config_readme.txt \
    hs-ulm-sunspec.json
