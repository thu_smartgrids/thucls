/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "tunnler.h"
#include <QObject>
//#include "doubleclient/doubleclient.h"
#include "targetlayer.h"

Tunnler::Tunnler()
{
}

bool Tunnler::start()
{
//	return getDoubleClient()->init(3, QHostAddress::LocalHost, IEC_PORT) == 0;
	return false;
}

bool Tunnler::stop()
{
//	return getDoubleClient()->cancel();
	return false;
}

DoubleClient* Tunnler::getDoubleClient()
{
//	static DoubleClient dc(NULL);
//
//	return &dc;
	return NULL;
}
