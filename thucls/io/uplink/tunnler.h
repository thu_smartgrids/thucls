/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef TUNNLER_H
#define TUNNLER_H

class DoubleClient;

/**
 * Handler to the doubleclient provided by Hessware.
 * Works like a Singleton.
 */
class Tunnler
{
public:
	Tunnler();
	static bool start();
	static bool stop();
private:
	static DoubleClient* getDoubleClient();
};

#endif // TUNNLER_H
