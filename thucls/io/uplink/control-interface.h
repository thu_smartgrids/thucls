/*
 * This file defines the Hessware Control Client API.
 *
 * Copyright (c) 2013, Sebastian Heß <info@hessware.de> Hessware GmbH
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * 3. Neither the name of the Hessware nor the names of its contributors may be
 *    used to endorse or promote products derived from this software without specific
 *    prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef CONTROLINTERFACE_H
#define CONTROLINTERFACE_H

/*! \file */

#include <QtPlugin>
#include <QString>
#include <QVariantMap>
#include <QDebug>

#ifdef SMPF_BUILD
#include <smg/protocols/meter_record.h>
#else
class MeterRecord {
public:
	MeterRecord() { }
};

#endif // SMPF_BUILD


#define HRTCP_MAP_KEY_MAC "..local.ownmac"

/*!
 * \brief The ControlServer class
 *
 * This class implements methods to send data from your plugin to the control
 * server. It also seperates the control-client core from the plugins. Only the
 * functions exported here define the stable control-client API, usage of any
 * other function or methode of the HRTCP Class may result in unexpected behavier.
 */
class ControlServer : public QObject {
private:
	QObject *searchbase;
	QObject *findHRTCP() {
		QObject *ret = NULL;
		QObject *hrtcpconnect = searchbase;
		while (hrtcpconnect != NULL) {
			if (QString(hrtcpconnect->metaObject()->className()) == "HRTCPConnector") {
				ret = hrtcpconnect;
				break;
			}
			hrtcpconnect = hrtcpconnect->parent();
		}
		return ret;
	}

public:
	/*!
	 * \brief ControlServer Constructor
	 * \param parent The parent object creating this instance. One of the parents of this object must be the control client.
	 */
	ControlServer(QObject *parent) : QObject(NULL) { searchbase = parent; }

	/*! \brief Send a result definition for a given id
	 *
	 * The HRTCP protocol is asynchon which means you must send the results of an operation to the
	 * server with the id the server provided. This way the server is able to verify that a request
	 * was a success or not.
	 *
	 * You may use sendPluginOK and sendPluginFAIL to shorten the code.
	 *
	 * \param id The ID provided by the server
	 * \param success Indicates if the function call was successful
	 * \param status A unique id for the error code (only used if success is false)
	 * \param text A error string which may help the administrator to understand the error
	 */
	bool sendResult(QString &id, bool success, quint32 status = 0, QString text = "") {
		/* Find the correct parent */
		QObject *hrtcpconnect = findHRTCP();
		if (!hrtcpconnect)
			return false;

		/* Call the HRTCPConnector */
		return QMetaObject::invokeMethod(hrtcpconnect, "sendResult", Qt::QueuedConnection,
						 Q_ARG(QString, id), Q_ARG(bool, success), Q_ARG(quint32, status),
						 Q_ARG(QString, text));
	}

	/*!
	 * \brief This is an overloaded function for sending different outcomes of commands than success or error
	 * \param id
	 * \param success an integer for the status
	 * \param status
	 * \param text
	 * \return
	 */
	bool sendResult(QString &id, int success, quint32 status = 0, QString text = "") {
		/* Find the correct parent */
		QObject *hrtcpconnect = findHRTCP();
		if (!hrtcpconnect)
			return false;

		QString outcome;
		switch (success) {
		case -1:outcome = "error";
			break;
		case 0: outcome = "success";
			break;
		default:
			outcome = "error";
		}
		/* Call the HRTCPConnector */
		return QMetaObject::invokeMethod(hrtcpconnect, "sendResult", Qt::QueuedConnection,
						 Q_ARG(QString, id), Q_ARG(QString, outcome), Q_ARG(quint32, status),
						 Q_ARG(QString, text));
	}

	/*!
	 * \brief The outcome of a setPercent action can have the outcome "adjusted". This outcome is respected here.
	 * \param id
	 * \param adjustedValue The value to which the setPercent is adjusted to. (e.g. setPercent wants to set to 100%
	 * the VNB allows only 30%, then 30% are returned.
	 * \return false if failure otherwise true.
	 */
	bool sendAdjusted(QString id, QString adjustedValue) {
		/* Find the correct parent */
		QObject *hrtcpconnect = findHRTCP();
		if (!hrtcpconnect) {
			return false;
		}

		/* String composition:
		 * { "r" : "adjusted", "id" : "aaa", "d" : { "p":"xx"} }
		 */

        QString data = QString("{\"r\":\"adjusted\",\"id\":\"%1\", \"d\":{\"p\":\"%2\"}}")
				.arg(id)
				.arg(adjustedValue);

		return QMetaObject::invokeMethod(hrtcpconnect, "sendRaw", Qt::QueuedConnection,
						 Q_ARG(QString, data));
	}

#ifdef SMPF_BUILD
	void sendMeterRecord(MeterRecord &mrec, const QString &meterPoint) {
		/* Find the correct parent */
		QObject *hrtcpconnect = findHRTCP();
		if (!hrtcpconnect)
			return;
		/* Call the HRTCPConnector */
		QMetaObject::invokeMethod(hrtcpconnect, "sendMeterRecord", Qt::QueuedConnection,
					  Q_ARG(MeterRecord&, mrec), Q_ARG(QString, meterPoint));
	}
#endif // SMPF_BUILD
	void sendRaw(QString &rawData, bool encrypt = false) {
		/* Find the correct parent */
		QObject *hrtcpconnect = findHRTCP();
		if (!hrtcpconnect)
			return;
		/* Call the HRTCPConnector */
		QMetaObject::invokeMethod(hrtcpconnect, "sendRaw", Qt::QueuedConnection,
					  Q_ARG(QString, rawData), Q_ARG(bool, encrypt));
	}

	/*! short version of ControlServer::sendResult to send a success message back to the server */
	#define sendPluginOK do { ControlServer CC(this); CC.sendResult(id, true); } while (0);

	/*! short version of ControlServer::sendResult to send a success message back to the server */
	#define sendPluginOKAdjusted(value) do { ControlServer CC(this); CC.sendAdjusted(id, value); } while (0);

	/*! short version of ControlServer::sendResult to send an error message back to the server */
	#define sendPluginFAIL(status, text) do { ControlServer CC(this); CC.sendResult(id, false, status, text); } while(0);

};


/*!
 * \brief The ControlInterface class
 *
 * This class is used to create plugins
 */
class ControlInterface {
public:
	virtual ~ControlInterface() { }

	/*!
	 * \brief setup the instanciated object
	 *
	 * The Qt plugin loader does not allow to use specialized constructors, therefore
	 * a class is instanciated and needs to be set up use another method of the now
	 * instanciated object. This function must be implemented by the plugin author
	 * to be able to act as a switching point plugin.
	 *
	 * This method may be called more than once, you should keep this in mind. If your
	 * plugin should not be instanciated more than once then return false here.
	 *
	 * \param swid The EIP is the Switching Point ID and is used to set the identifier
	 *             used by the server. In slotSetPercent(); you need to check not to
	 *             answer switching commands sent for another point.
	 * \param data The parameters send by the server to instanciate this object or change
	 *             it's behavier during runtime. Return false if runtime configuration is
	 *             not acceptable.
	 * \return You may return false here if your plugin needs hardware which is not available
	 *         or the data could not be parsed
	 */
	virtual bool setupObject(QString &swid, QVariantMap &data) = 0;

	/*!
	 * \brief allowMultiple
	 * \return Return true here if the core may call setupObject multiple times to add different
	 *         hardware components to the service. This may be used for Modbus for example.
	 */
	virtual bool allowMultiple() = 0;


	/*!
	 * \brief asyncInitialization
	 * \return Return true here if the core should ignore the return value of your setupObject()
	 *	   call. If you return true you need to take the element "__id" for the QVariantMap data
	 *	   and use sendPluginOK or sendPluginFAIL to inform the server about your status
	 */
	virtual bool asyncInitialization() = 0;

	/*!
	 * \brief Set percent for a switching point
	 *
	 * This slot is called as soon as the server sends a request to set a switching point.
	 * You MUST implement this as a slot otherwise your plugin may not get loaded!
	 *
	 * As this slot returns nothing you need to send the status of the operation success or error
	 * to the server using ControlServer::sendResult(). The server got a specific timeout waiting for
	 * answers, this may be around 30 seconds.
	 *
	 * \warning This slot is called on ALL active Plugins and it's your responsability to
	 *          check if the "swid" matches the one set during setupObject().
	 *
	 * \warning If you want to use sendPluginOK and sendPluginFAIL you need to make sure that
	 *          the id parameter is also called id in your implementation.
	 *
	 * \param id The ID of this call, it's needed to send the answer back to the server
	 * \param swid The ID of the switching point to be set
	 * \param percent The percent to be set. Your plugin needs to check if the setting is ok,
	 *                if not you MUST send an error to the server.
	 */
	virtual void slotSetPercent(QString id, QString swid, uint percent) = 0;
};
Q_DECLARE_INTERFACE(ControlInterface, "de.hessware.smpf.control-client.control")

class ControlPluginInterface {
public:
	virtual ~ControlPluginInterface() { }
	virtual bool setData(QVariantMap &data) = 0;
};

Q_DECLARE_INTERFACE(ControlPluginInterface, "de.hessware.smpf.control-client.plugin")

#endif // CONTROLINTERFACE_H
