/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "genio.h"
#include "targetlayer.h"

#include <QDateTime>
#include <QDebug>

extern "C" {
#ifdef _WIN32
#include <winsock2.h>
#else
#include <arpa/inet.h>
#endif
}

typedef union {
	quint32 asLong;
	quint16 asShort[2];
} dword;

static GenIo* atPCC = NULL;

//#if ( sizeof(dword) != sizeof(quint32) )
//	#warning "network translation does not work with current compiler settings"
//#endif

GenIo::GenIo(QObject* parent, bool metering) : QObject(parent)
{
	if( metering && atPCC == NULL ){
		atPCC = this;
	}

	failcount = kaycount = 0;
	toleratedFailRate = 10; // percent
	failsum = kaysum = lastHealthInfo = 0;
	connState = io_IsLost;
	connStaTime = 0;
}

bool GenIo::metersPCC()
{
	return (this == atPCC);
}

io_ConnState GenIo::getHealth(qint64* time)
{
	if( time!= NULL ){
		*time = connStaTime;
	}
	return connState;
}

io_ConnState GenIo::checkHealth()
{
	quint32 rate = failcount * 100 / (failcount + kaycount + 1);

	connStaTime = QDateTime::currentMSecsSinceEpoch();
	qDebug() << "I/O health" << getName() << ":" << kaycount << "good," << failcount << "failed";
	if( rate < toleratedFailRate ){
		connState = io_IsOkay;
	} else if( recover(rate) ){
		connState = io_IsUgly;
	} else {
		connState = io_IsLost;
	}

	kaysum += kaycount;
	failsum += failcount;
	quint32 timeslice = TargetLayer::getTimeSlice15min();
	if( lastHealthInfo != timeslice ){
		qInfo() << "summarized I/O health" << getName() << ":" << kaysum << "good," << failsum << "failed";
		lastHealthInfo = timeslice;
		kaysum = failsum = 0;
	}

	failcount = kaycount = 0;
	return connState;
}

const QString GenIo::io_getConnStateStr(io_ConnState state)
{
	static const QString description[] = {"Connection state unknown", "Connection good", "Connection unstable", "Connection lost"};
	if( state < 0 || 3 < state ){
		state = io_Unknwn;
	}
	return description[state];
}

quint16 GenIo::io_ntohs(quint16 netshort, ByteOrder netorder)
{
	switch( netorder ){
	case BigEndian:
	case HalfEndian:
		return ntohs(netshort);
	default:
		qDebug() << "given netorder not supported";
		return 0;
	}
}

quint32 GenIo::io_ntohl(quint32 netlong, ByteOrder netorder)
{
	switch( netorder ){
	case BigEndian:
		return ntohl(netlong);
	case HalfEndian:
		dword buffer;
		buffer.asLong = netlong;
		return ((quint32)ntohs(buffer.asShort[1]) << 16) + ntohs(buffer.asShort[0]);
	default:
		qDebug() << "given netorder not supported";
		return 0;
	}
}

quint16 GenIo::io_htons(quint16 hostshort, ByteOrder netorder)
{
	switch( netorder ){
	case BigEndian:
	case HalfEndian:
		return htons(hostshort);
	default:
		qDebug() << "given netorder not supported";
		return 0;
	}
}

quint32 GenIo::io_htonl(quint32 hostlong, ByteOrder netorder)
{
	switch( netorder ){
	case BigEndian:
		return htonl(hostlong);
	case HalfEndian:
		dword buffer;
		buffer.asShort[1] = hostlong >> 16;
		buffer.asShort[0] = hostlong & 0xFFFFu;
		return htons(buffer.asLong);
	default:
		qDebug() << "given netorder not supported";
		return 0;
	}
}
