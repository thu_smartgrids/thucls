/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "restler.h"

#include <QtCore/QUrl>
#include <QEventLoop>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QTimer>

#include <QDebug>

// see http://melannj.blogspot.de/2015/10/consuming-restful-web-service-with-c.html

Restler::Restler(QObject* parent): GenIo(parent)
{
	mgr = new QNetworkAccessManager(parent);
}

bool Restler::init(QString ip)
{
	//TODO: perform some check
	this->domain = ip;
	this->timeout = 1000ul; // 1s
	bool result = initDict();
	return result;
}

QNetworkReply* Restler::sendRequest(QUrl request)
{
	Q_ASSERT( mgr != NULL );
	QEventLoop eventLoop;
	QTimer timer;
	QObject::connect(mgr, &QNetworkAccessManager::finished, &eventLoop, &QEventLoop::quit);
	QObject::connect(&timer, &QTimer::timeout, &eventLoop, &QEventLoop::quit);

	QNetworkRequest req(request);
	QNetworkReply *reply = mgr->get(req);
	timer.start(timeout);
	eventLoop.exec();

	mgr->disconnect();

	return reply;
}

QJsonDocument Restler::readJson(QByteArray data, QString block)
{
	Q_UNUSED(block);
	return QJsonDocument::fromJson(data);
}

bool Restler::readBlock(QString block)
{
	QUrl request;
	QNetworkReply* reply;

	if( !blockRequests.contains(block) ){
		qDebug() << "block" << block << "is unknown";
		return false;
	}

	delete replyList[block]; // TODO: make thread safe
	replyList[block] = NULL;

	request = QUrl("http://" + domain + blockRequests[block]);
	reply = sendRequest(request);

	if( reply == NULL || !reply->isFinished() ){
		qDebug() << (reply?"readBlock timed out": "readBlock received empty reply");
		delete reply;
		failcount++;
		return false;
	}
	if( reply->error() != QNetworkReply::NoError ) {
		qDebug() << "readBlock failed" << reply->errorString();
		delete reply;
		failcount++;
		return false;
	}

	QByteArray data = reply->readAll();
	QJsonDocument jsonReply = readJson(data, block);
	replyList[block] = new QJsonDocument(jsonReply);

	//qDebug() << "received JSON from" << domain;
	delete reply;
	kaycount++;
	return true;
}

bool Restler::isAvail(QString element, QString block)
{
	if( element == NULL || element == "" ){
		qDebug("unspecified element");
		return false;
	}

	if( block == NULL || replyList[block] == NULL ){
		//qDebug("not available");
		return false;
	}

	return replyList[block]->object().toVariantMap().contains(element);
}

QVariant Restler::read(QString element, QString block)
{
	if( ! isAvail(element, block) ){
		qDebug() << "element" << element << "not found within" << block;
		return QVariant(QVariant::Invalid);
	}

	QVariantMap container = replyList[block]->object().toVariantMap();
	return container[element];
}

bool Restler::write(QString element, QVariant value)
{
	if( ! value.isValid() ){
		qDebug() << "Restler: invalid out value";
		return false;
	}

	if( ! writeRequests.contains(element) ){
		qDebug() << "Restler: request for" << element << "unknown";
		return false;
	}

	QUrl request = QUrl("http://" + domain + writeRequests[element] + value.toString());
	QNetworkReply* reply = sendRequest(request);

	if( reply == NULL ){
		qDebug() << "no answer from Device" << domain;
		return false;
	}
	if( reply->error() != QNetworkReply::NoError ) {
		qDebug() << "writing" << element << "failed" << reply->errorString();
		delete reply;
		return false;
	}
	delete reply;
	return true;
}

bool Restler::recover(quint16 failrate)
{
	/* there is nothing to recover */

	if( failrate < 25 ){
		return true;
	}
	return false;
}

