/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU
 ** Lesser General Public License version 3. A copy of the license
 ** should be part of this distribution package.
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details.
 **
 *******************************************************************/

#ifndef RESTSONNENECO8_H
#define RESTSONNENECO8_H

#include "restler.h"

class RestSonnenEco8 : public Restler
{
	Q_OBJECT
public:
	RestSonnenEco8(QObject* parent = NULL);
	QVariant read(QString element, QString block = NULL);
protected:
	bool initDict();
};

#endif // RESTSONNENECO8_H
