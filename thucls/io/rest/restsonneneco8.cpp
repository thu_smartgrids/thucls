/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU
 ** Lesser General Public License version 3. A copy of the license
 ** should be part of this distribution package.
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details.
 **
 *******************************************************************/

#include "restsonneneco8.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QDebug>

RestSonnenEco8::RestSonnenEco8(QObject* parent): Restler(parent)
{
}

QVariant RestSonnenEco8::read(QString element, QString block)
{
	if( block == "status"){
		return Restler::read(element, block);
	}

	/* REMOVED for legal reasons */

	qDebug() << "element" << element << "not found within" << block;
	return QVariant(QVariant::Invalid);
}

bool RestSonnenEco8::initDict()
{
	blockRequests["status"] = ":8080/api/v1/status";
	/* REMOVED for legal reasons */

	qDebug() << "supported server requests: " << blockRequests.keys();

	writeRequests["charge"] = ":8080/api/v1/setpoint/charge/";
	writeRequests["discharge"] = ":8080/api/v1/setpoint/discharge/";
	/* REMOVED for legal reasons */

	return true;
}
