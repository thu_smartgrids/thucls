/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef RESTLER_H
#define RESTLER_H

#include <io/genio.h>
class QNetworkReply;
class QNetworkAccessManager;

class Restler : public GenIo
{
	Q_OBJECT
public:
	Restler(QObject* parent = NULL);
	bool init(QString ip);
	QString getName() { return domain; }

	bool readBlock(QString block);

	bool isAvail(QString element, QString block);
	QVariant read(QString element, QString block = NULL);
	bool write(QString element, QVariant value);
	bool write(QString element, QString value) { return write(element, QVariant(value)); }

//	io_ConnState getHealth(qint64* time = NULL);
//	io_ConnState checkHealth();

protected:
	QNetworkAccessManager* mgr;
	QString domain;
	quint32 timeout;
	QMap<QString, QString> blockRequests, writeRequests;
	//QMap<QString, QString> addressBook;
	QMap<QString, QJsonDocument*> replyList;

	virtual bool initDict() = 0;
	virtual QJsonDocument readJson(QByteArray data, QString block);
	virtual bool recover(quint16 failrate);
	QNetworkReply* sendRequest(QUrl request);
};

#endif // RESTLER_H
