/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef GENIO_H
#define GENIO_H

/**
  * Generic interface for I/O operations
  */

#include <QString>
#include <QVariant>

typedef enum {
	io_Unknwn = 0,
	io_IsOkay = 1,
	io_IsUgly = 2,
	io_IsLost = 3
} io_ConnState;


/**
 * Generic I/O handler to read/write single parameters by name (and block)
 * Contains preparations for endianess conversion and health monitoring.
 */

class GenIo : public QObject
{
	Q_OBJECT
public:
	typedef enum {
		BigEndian = 0, // first word high, first byte high
		HalfEndian = 1  // first word low, first byte high
	//	MixedEndian = 2 // first word high, first byte low
	//	LittleEndian = 3 // first word low, first byte low
	} ByteOrder;


	GenIo(QObject* parent = NULL, bool metering = true);
	//bool init(QString ip);

	virtual QString getName() = 0;
	virtual bool readBlock(QString block) = 0;

	virtual QVariant read(QString element, QString block = NULL) = 0;
	virtual bool write(QString element, QVariant value) = 0;
	virtual bool write(QString element, QString value) { return write(element, QVariant(value)); }

	bool metersPCC();

	io_ConnState getHealth(qint64* time = NULL);
	virtual bool recover(quint16 failrate) = 0;
public slots:
	virtual io_ConnState checkHealth();
protected:
	quint32 failcount, kaycount;
	quint32 toleratedFailRate;
	quint32 failsum, kaysum, lastHealthInfo;
	io_ConnState connState;
	qint64 connStaTime;
public:
	static const QString io_getConnStateStr(io_ConnState state);

	static quint16 io_ntohs(quint16 netshort, ByteOrder netorder);
	static quint32 io_ntohl(quint32 netlong, ByteOrder netorder);
	static quint16 io_htons(quint16 hostshort, ByteOrder netorder);
	static quint32 io_htonl(quint32 hostlong, ByteOrder netorder);
};

#endif // GENIO_H
