/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef SOEFLER_H
#define SOEFLER_H

#include <QObject>
#include "iec61850_common.h"
//#include "mms_common.h"
#include "io/genio.h"
#include <QVariant>

extern "C" {
typedef struct sIedServer* IedServer;
typedef struct sLogicalNode LogicalNode;
typedef struct sModelNode ModelNode;
//typedef struct sDataObject DataObject;
typedef struct sDataAttribute DataAttribute;
typedef struct sClientConnection* ClientConnection;
}
class SoefReader;
class QThread;

#define CURRENT_TIME 1

/**
 * IEC 61850 Layer (Sixty One Eight Fifty LayER).
 * Based on libiec61850 and utilizes a static model. Realised as singleton.
 */
class Soefler : public GenIo
{
	Q_OBJECT
public:
	explicit Soefler(QObject *parent = 0);
	~Soefler();

	QString getName() { return "61850 layer"; }

	void start();
//	void init(QString name, QString info);

	void addBlock(QString block, LogicalNode* lnode);
	SoefReader* addReader(QString element, QVariant min, QVariant max, QString block = NULL);
	bool addReader(QString element, SoefReader* reader, QString block = NULL);
	static const QVariant noCheck;
	SoefReader* getReader(QString element);
	QString listReaders();

	bool readBlock(QString block);
	QVariant read(QString element, QString block = NULL);
	bool write(QString element, QVariant value) { return write(element, value, NULL); }
	bool write(QString element, QVariant value, QString block);
	bool update(QString element, QVariant value,
		    uint64_t time=CURRENT_TIME, qint32 qualityGood=QUALITY_VALIDITY_GOOD, QString block = NULL);

	bool updateBeh(qint32 ldMode, qint32 lnMode,
		    uint64_t time=CURRENT_TIME, qint32 qualityGood=QUALITY_VALIDITY_GOOD, QString block = NULL);

	bool addReaders(QString element, quint32 digits, QString suffix, SoefReader* reader, QString block = NULL);
	QVariant readList(QString element, quint32 digits, QString suffix, quint32 count, QString block = NULL);
	bool writeList(QString element, quint32 digits, QString suffix, QVariantList values, QString block = NULL);
	bool writeElement(QString element, quint32 idx, quint32 digits, QString suffix, QVariant value, QString block);

	bool lock(QString block);
	bool select(QString block);
	void unlock();

	bool recover(quint16 failrate) { Q_UNUSED(failrate); return false; }
public slots:
	io_ConnState checkHealth() { return io_Unknwn; }

protected:
	friend class ClsProfile;
	friend class ControlObject;
	friend class SchedController;
	IedServer iedServer;
	quint16 tcpPort;

	ModelNode* currentNode;
	//bool locked;
	QThread* lockedBy;
	QMap<QString, LogicalNode*> blocks;
	QMap<QString, SoefReader*> readerMap;

	ModelNode* getNode(const char* object, const char* suffix, QString block, bool dbg = true);
	ModelNode* getNode(ModelNode* object, const char* suffix);
	MmsType getType(ModelNode *node);
	DataAttribute *getFirstAttr(ModelNode *node);

	bool addReaderForNode(ModelNode* node, SoefReader* reader, bool schedulable=true);
	QVariant readNode(ModelNode* node);
	qint32 writeNode(ModelNode* node, QVariant value);
	bool updateNode(ModelNode* node, const char* suffix, QVariant value,
			uint64_t time=Hal_getTimeInMs(), qint32 qualityGood=QUALITY_VALIDITY_GOOD);

	bool selectNode(LogicalNode* node);
};

class SoefReader : public QObject
{
	Q_OBJECT
public:
	explicit SoefReader(QObject* parent, MmsType type,
			    QVariant min=QVariant(QVariant::Invalid), QVariant max=QVariant(QVariant::Invalid));
	~SoefReader();

	void setNode(ModelNode* node);
	ModelNode* getNode();
protected:
	ModelNode* node;

	friend class Soefler;
	static MmsDataAccessError handleWriteAccess(DataAttribute *dataAttribute, MmsValue *value, ClientConnection connection, void *parameter);
	static QVector<SoefReader*> readers;
signals:
	void updated(QVariant value);

public slots:
	virtual MmsDataAccessError handleWrite (MmsType type, QVariant value);

protected:
	MmsType valType;
	QVariant minVal, maxVal;
};

typedef enum {
	Beh_on = 1,
	Beh_blocked = 2,
	Beh_test = 3,
	Beh_test_blocked = 4,
	Beh_off = 5
} Beh;

#endif // SOEFLER_H
