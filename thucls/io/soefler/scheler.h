/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef SCHELER_H
#define SCHELER_H

#include "iec61850_common.h"
#include "iec61850_server.h"
#include <io/genio.h>
#include <QObject>
#include <QTimer>
#include <QVariant>
#include <QVector>
#include "soefler.h"

extern "C" {
typedef struct sDataAttribute DataAttribute;
typedef struct sLogicalNode LogicalNode;
typedef struct sModelNode ModelNode;
}

typedef struct {
	qint32 numberEntries;
	QList <float> valASG;
} Schedule;

typedef enum {
	SchdNotReady = 1,
	SchdNotRunning = 2, /* start time required */
	SchdReady = 3,
	SchdRunning = 4
} SchdState;


class ControlObject : public QObject {
	Q_OBJECT
public:
	ControlObject(Soefler* parent, const char* obName, QString ln);

	void init();
	void trigger();
protected:
	Soefler* soefler;
	const char* obName;
	QString lnode;
};

/**
 * Stores schedule information for Data Object FSCH and FSCC
 */
class Scheler : public QObject {
	Q_OBJECT
public:
	Scheler(Soefler* parent, QString ln);

	CheckHandlerResult checkHandler(void* parameter, MmsValue* ctlVal, bool test, bool interlockCheck);
	ControlHandlerResult controlHandler(void* parameter, MmsValue* value, bool test);

	void init();
	static Scheler* getScheler(QString ln);
public slots:
	void updateState();
	void updateZRequest(QVariant value);
signals:
	void scheduleEnabled();
	void stateUpdated();
	void toggle(MmsType type, QVariant value);
protected:
	friend class SchelReader;
	SchdState getState() { return currentState; }
	void setState(SchdState state, bool test);
	/* change state */
	bool enableSchedule(bool test);
	bool disableSchedule(bool test);
protected slots:
	void slotStartSchedule();
	void slotToggleSchedule();
private:
	SchdState currentState;
	Soefler* soefler;
	QString lnode;
	ControlObject enaReq, dsaReq;

	qint64 intv;
	quint32 entries, index;
	QVector<QVariant> values;
	QVariant unit;
	QTimer togtimer;
	//volatile qint64 updatime;
};

class SchelReader : public SoefReader
{
	Q_OBJECT
public:
	explicit SchelReader(Scheler* parent, MmsType type,
		    QVariant min=QVariant(QVariant::Invalid), QVariant max=QVariant(QVariant::Invalid));
	MmsDataAccessError handleWrite (MmsType type, QVariant value);

protected:
	Scheler* scheler;
};

class SchedController : public QObject {
	Q_OBJECT
public:
	SchedController(Soefler* parent, QString ln);
	void init(QString cSchedule, QString controlledEntity);
public slots:
	void updateSched(QVariant schd);
	void updateEntity(QVariant ent);
	void toggleValue(MmsType type, QVariant value);
private:
	Soefler* soefler;
	QString lnode, lpath;

	Scheler* schedule;
	//QString entity;
	SoefReader* entity;
};

class SchedElementReader : public SoefReader
{
	Q_OBJECT
public:
	explicit SchedElementReader(Soefler* parent);
	MmsDataAccessError handleWrite (MmsType type, QVariant value);
protected:
};

class ElementReader : public SoefReader
{
	Q_OBJECT
public:
	explicit ElementReader(Soefler* parent);
	MmsDataAccessError handleWrite (MmsType type, QVariant value);
protected:
	Soefler* soefler;
};

#endif // SCHELER_H
