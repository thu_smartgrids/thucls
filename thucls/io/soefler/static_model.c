/*
 * static_model.c
 *
 * automatically generated from CLS-App_v2.11.cid
 */
#include "static_model.h"

static void initializeValues();

extern DataSet iedModelds_CLS_LLN0_CLS_Stammdaten;
extern DataSet iedModelds_CLS_LLN0_CLS_Betriebsstatus;
extern DataSet iedModelds_CLS_LLN0_CLS_Fahrplan1;
extern DataSet iedModelds_CLS_LLN0_CLS_Fahrplan2;
extern DataSet iedModelds_CLS_LLN0_CLS_FahrplanCtl;
extern DataSet iedModelds_CLS_LLN0_CLS_Zell;
extern DataSet iedModelds_CLS_LLN0_CLS_Netzmessungen;
extern DataSet iedModelds_PV_LLN0_PV_Stammdaten;
extern DataSet iedModelds_PV_LLN0_PV_Betriebsstatus;
extern DataSet iedModelds_PV_LLN0_PV_Messungen;
extern DataSet iedModelds_PV2_LLN0_PV_Stammdaten;
extern DataSet iedModelds_PV2_LLN0_PV_Betriebsstatus;
extern DataSet iedModelds_PV2_LLN0_PV_Messungen;
extern DataSet iedModelds_BAT_LLN0_BAT_Stammdaten;
extern DataSet iedModelds_BAT_LLN0_BAT_Betriebsstatus;
extern DataSet iedModelds_BAT_LLN0_BAT_Messungen;
extern DataSet iedModelds_HEAT_LLN0_HEAT_Stammdaten;
extern DataSet iedModelds_HEAT_LLN0_HEAT_Betriebsstatus;
extern DataSet iedModelds_HEAT_LLN0_HEAT_Messungen;
extern DataSet iedModelds_EMOB_LLN0_EMOB_Stammdaten;
extern DataSet iedModelds_EMOB_LLN0_EMOB_Betriebsstatus;
extern DataSet iedModelds_EMOB_LLN0_EMOB_Messungen;
extern DataSet iedModelds_MEAS_LLN0_MEAS_Stammdaten;
extern DataSet iedModelds_MEAS_LLN0_MEAS_Betriebsstatus;
extern DataSet iedModelds_MEAS_LLN0_MEAS_Messungen;
extern DataSet iedModelds_MEAS2_LLN0_MEAS_Stammdaten;
extern DataSet iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus;
extern DataSet iedModelds_MEAS2_LLN0_MEAS_Messungen;


extern DataSetEntry iedModelds_CLS_LLN0_CLS_Stammdaten_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Stammdaten_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Stammdaten_fcda2;

DataSetEntry iedModelds_CLS_LLN0_CLS_Stammdaten_fcda0 = {
  "CLS",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Stammdaten_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Stammdaten_fcda1 = {
  "CLS",
  false,
  "CLS_LPHD1$DC$PhyNam",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Stammdaten_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Stammdaten_fcda2 = {
  "CLS",
  false,
  "PCC_MMXU1$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_Stammdaten = {
  "CLS",
  "LLN0$CLS_Stammdaten",
  3,
  &iedModelds_CLS_LLN0_CLS_Stammdaten_fcda0,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus
};

extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda2;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda3;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda4;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda5;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda6;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda7;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda8;

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda0 = {
  "CLS",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda1 = {
  "CLS",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda2 = {
  "CLS",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda3
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda3 = {
  "CLS",
  false,
  "CLS_LPHD1$ST$PhyHealth",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda4
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda4 = {
  "CLS",
  false,
  "CLS_LPHD1$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda5
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda5 = {
  "CLS",
  false,
  "CLS_LPHD1$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda6
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda6 = {
  "CLS",
  false,
  "PCC_MMXU1$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda7
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda7 = {
  "CLS",
  false,
  "PCC_MMXU1$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda8
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda8 = {
  "CLS",
  false,
  "PCC_MMXU1$ST$Beh",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_Betriebsstatus = {
  "CLS",
  "LLN0$CLS_Betriebsstatus",
  9,
  &iedModelds_CLS_LLN0_CLS_Betriebsstatus_fcda0,
  &iedModelds_CLS_LLN0_CLS_Fahrplan1
};

extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda2;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda3;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda4;

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda0 = {
  "CLS",
  false,
  "SCHD_FSCH1$ST$SchdSt",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda1 = {
  "CLS",
  false,
  "SCHD_FSCH1$SP$NumEntr",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda2 = {
  "CLS",
  false,
  "SCHD_FSCH1$MX$ValMV",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda3
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda3 = {
  "CLS",
  false,
  "SCHD_FSCH1$SP$SchdPrio",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda4
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda4 = {
  "CLS",
  false,
  "SCHD_FSCH1$SP$SchdReuse",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_Fahrplan1 = {
  "CLS",
  "LLN0$CLS_Fahrplan1",
  5,
  &iedModelds_CLS_LLN0_CLS_Fahrplan1_fcda0,
  &iedModelds_CLS_LLN0_CLS_Fahrplan2
};

extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda2;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda3;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda4;

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda0 = {
  "CLS",
  false,
  "SCHD_FSCH2$ST$SchdSt",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda1 = {
  "CLS",
  false,
  "SCHD_FSCH2$SP$NumEntr",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda2 = {
  "CLS",
  false,
  "SCHD_FSCH2$MX$ValMV",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda3
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda3 = {
  "CLS",
  false,
  "SCHD_FSCH2$SP$SchdPrio",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda4
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda4 = {
  "CLS",
  false,
  "SCHD_FSCH2$SP$SchdReuse",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_Fahrplan2 = {
  "CLS",
  "LLN0$CLS_Fahrplan2",
  5,
  &iedModelds_CLS_LLN0_CLS_Fahrplan2_fcda0,
  &iedModelds_CLS_LLN0_CLS_FahrplanCtl
};

extern DataSetEntry iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda2;

DataSetEntry iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda0 = {
  "CLS",
  false,
  "SCHD_FSCC1$ST$ActSchdRef",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda1 = {
  "CLS",
  false,
  "SCHD_FSCC1$SP$CtlEnt",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda2 = {
  "CLS",
  false,
  "SCHD_FSCC1$MX$ValMV",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_FahrplanCtl = {
  "CLS",
  "LLN0$CLS_FahrplanCtl",
  3,
  &iedModelds_CLS_LLN0_CLS_FahrplanCtl_fcda0,
  &iedModelds_CLS_LLN0_CLS_Zell
};

extern DataSetEntry iedModelds_CLS_LLN0_CLS_Zell_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Zell_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Zell_fcda2;

DataSetEntry iedModelds_CLS_LLN0_CLS_Zell_fcda0 = {
  "CLS",
  false,
  "ZELL_ZELL1$SP$LocAgent",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Zell_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Zell_fcda1 = {
  "CLS",
  false,
  "ZELL_ZELL1$SP$MinW",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Zell_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Zell_fcda2 = {
  "CLS",
  false,
  "ZELL_ZELL1$SP$MaxW",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_Zell = {
  "CLS",
  "LLN0$CLS_Zell",
  3,
  &iedModelds_CLS_LLN0_CLS_Zell_fcda0,
  &iedModelds_CLS_LLN0_CLS_Netzmessungen
};

extern DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda0;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda1;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda2;
extern DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda3;

DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda0 = {
  "CLS",
  false,
  "PCC_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda1
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda1 = {
  "CLS",
  false,
  "PCC_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda2
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda2 = {
  "CLS",
  false,
  "PCC_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  &iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda3
};

DataSetEntry iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda3 = {
  "CLS",
  false,
  "PCC_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_CLS_LLN0_CLS_Netzmessungen = {
  "CLS",
  "LLN0$CLS_Netzmessungen",
  4,
  &iedModelds_CLS_LLN0_CLS_Netzmessungen_fcda0,
  &iedModelds_PV_LLN0_PV_Stammdaten
};

extern DataSetEntry iedModelds_PV_LLN0_PV_Stammdaten_fcda0;
extern DataSetEntry iedModelds_PV_LLN0_PV_Stammdaten_fcda1;

DataSetEntry iedModelds_PV_LLN0_PV_Stammdaten_fcda0 = {
  "PV",
  false,
  "PV_ZINV1$SP$WRtg",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Stammdaten_fcda1
};

DataSetEntry iedModelds_PV_LLN0_PV_Stammdaten_fcda1 = {
  "PV",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_PV_LLN0_PV_Stammdaten = {
  "PV",
  "LLN0$PV_Stammdaten",
  2,
  &iedModelds_PV_LLN0_PV_Stammdaten_fcda0,
  &iedModelds_PV_LLN0_PV_Betriebsstatus
};

extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda2;
extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda3;
extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda4;
extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda5;
extern DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda6;

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda0 = {
  "PV",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda1 = {
  "PV",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda2 = {
  "PV",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda3
};

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda3 = {
  "PV",
  false,
  "PV_ZINV1$SP$OutWSet",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda4
};

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda4 = {
  "PV",
  false,
  "PV_ZINV1$SP$OutVarSet",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda5
};

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda5 = {
  "PV",
  false,
  "PV_ZINV1$SP$OutPFSet",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda6
};

DataSetEntry iedModelds_PV_LLN0_PV_Betriebsstatus_fcda6 = {
  "PV",
  false,
  "PV_ZINV1$ST$GridModSt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_PV_LLN0_PV_Betriebsstatus = {
  "PV",
  "LLN0$PV_Betriebsstatus",
  7,
  &iedModelds_PV_LLN0_PV_Betriebsstatus_fcda0,
  &iedModelds_PV_LLN0_PV_Messungen
};

extern DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda0;
extern DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda1;
extern DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda2;
extern DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda3;

DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda0 = {
  "PV",
  false,
  "PV_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Messungen_fcda1
};

DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda1 = {
  "PV",
  false,
  "PV_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Messungen_fcda2
};

DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda2 = {
  "PV",
  false,
  "PV_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  &iedModelds_PV_LLN0_PV_Messungen_fcda3
};

DataSetEntry iedModelds_PV_LLN0_PV_Messungen_fcda3 = {
  "PV",
  false,
  "PV_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_PV_LLN0_PV_Messungen = {
  "PV",
  "LLN0$PV_Messungen",
  4,
  &iedModelds_PV_LLN0_PV_Messungen_fcda0,
  &iedModelds_PV2_LLN0_PV_Stammdaten
};

extern DataSetEntry iedModelds_PV2_LLN0_PV_Stammdaten_fcda0;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Stammdaten_fcda1;

DataSetEntry iedModelds_PV2_LLN0_PV_Stammdaten_fcda0 = {
  "PV2",
  false,
  "PV_ZINV1$SP$WRtg",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Stammdaten_fcda1
};

DataSetEntry iedModelds_PV2_LLN0_PV_Stammdaten_fcda1 = {
  "PV2",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_PV2_LLN0_PV_Stammdaten = {
  "PV2",
  "LLN0$PV_Stammdaten",
  2,
  &iedModelds_PV2_LLN0_PV_Stammdaten_fcda0,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus
};

extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda2;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda3;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda4;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda5;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda6;

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda0 = {
  "PV2",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda1 = {
  "PV2",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda2 = {
  "PV2",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda3
};

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda3 = {
  "PV2",
  false,
  "PV_ZINV1$SP$OutWSet",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda4
};

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda4 = {
  "PV2",
  false,
  "PV_ZINV1$SP$OutVarSet",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda5
};

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda5 = {
  "PV2",
  false,
  "PV_ZINV1$SP$OutPFSet",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda6
};

DataSetEntry iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda6 = {
  "PV2",
  false,
  "PV_ZINV1$ST$GridModSt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_PV2_LLN0_PV_Betriebsstatus = {
  "PV2",
  "LLN0$PV_Betriebsstatus",
  7,
  &iedModelds_PV2_LLN0_PV_Betriebsstatus_fcda0,
  &iedModelds_PV2_LLN0_PV_Messungen
};

extern DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda0;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda1;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda2;
extern DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda3;

DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda0 = {
  "PV2",
  false,
  "PV_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Messungen_fcda1
};

DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda1 = {
  "PV2",
  false,
  "PV_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Messungen_fcda2
};

DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda2 = {
  "PV2",
  false,
  "PV_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  &iedModelds_PV2_LLN0_PV_Messungen_fcda3
};

DataSetEntry iedModelds_PV2_LLN0_PV_Messungen_fcda3 = {
  "PV2",
  false,
  "PV_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_PV2_LLN0_PV_Messungen = {
  "PV2",
  "LLN0$PV_Messungen",
  4,
  &iedModelds_PV2_LLN0_PV_Messungen_fcda0,
  &iedModelds_BAT_LLN0_BAT_Stammdaten
};

extern DataSetEntry iedModelds_BAT_LLN0_BAT_Stammdaten_fcda0;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Stammdaten_fcda1;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Stammdaten_fcda2;

DataSetEntry iedModelds_BAT_LLN0_BAT_Stammdaten_fcda0 = {
  "BAT",
  false,
  "BAT_DSTO1$SP$TotEnNom",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Stammdaten_fcda1
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Stammdaten_fcda1 = {
  "BAT",
  false,
  "BAT_ZINV1$SP$WRtg",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Stammdaten_fcda2
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Stammdaten_fcda2 = {
  "BAT",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_BAT_LLN0_BAT_Stammdaten = {
  "BAT",
  "LLN0$BAT_Stammdaten",
  3,
  &iedModelds_BAT_LLN0_BAT_Stammdaten_fcda0,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus
};

extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda2;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda3;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda4;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda5;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda6;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda7;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda8;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda9;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda10;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda11;

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda0 = {
  "BAT",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda1 = {
  "BAT",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda2 = {
  "BAT",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda3
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda3 = {
  "BAT",
  false,
  "BAT_DSTO1$ST$SoCAhr",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda4
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda4 = {
  "BAT",
  false,
  "BAT_DSTO1$ST$ChaCyclCnt",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda5
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda5 = {
  "BAT",
  false,
  "BAT_DSTO1$ST$OpTmh",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda6
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda6 = {
  "BAT",
  false,
  "BAT_DSTO1$MX$OutWh",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda7
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda7 = {
  "BAT",
  false,
  "BAT_DSTO1$SP$OpMod",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda8
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda8 = {
  "BAT",
  false,
  "BAT_ZINV1$SP$OutWSet",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda9
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda9 = {
  "BAT",
  false,
  "BAT_ZINV1$SP$OutVarSet",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda10
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda10 = {
  "BAT",
  false,
  "BAT_DBAT1$SP$MaxBatA",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda11
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda11 = {
  "BAT",
  false,
  "BAT_DBAT1$SP$MaxBatV",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_BAT_LLN0_BAT_Betriebsstatus = {
  "BAT",
  "LLN0$BAT_Betriebsstatus",
  12,
  &iedModelds_BAT_LLN0_BAT_Betriebsstatus_fcda0,
  &iedModelds_BAT_LLN0_BAT_Messungen
};

extern DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda0;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda1;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda2;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda3;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda4;
extern DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda5;

DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda0 = {
  "BAT",
  false,
  "BAT_DBAT1$MX$Vol",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Messungen_fcda1
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda1 = {
  "BAT",
  false,
  "BAT_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Messungen_fcda2
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda2 = {
  "BAT",
  false,
  "BAT_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Messungen_fcda3
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda3 = {
  "BAT",
  false,
  "BAT_MMXU1$MX$TotVA",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Messungen_fcda4
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda4 = {
  "BAT",
  false,
  "BAT_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  &iedModelds_BAT_LLN0_BAT_Messungen_fcda5
};

DataSetEntry iedModelds_BAT_LLN0_BAT_Messungen_fcda5 = {
  "BAT",
  false,
  "BAT_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_BAT_LLN0_BAT_Messungen = {
  "BAT",
  "LLN0$BAT_Messungen",
  6,
  &iedModelds_BAT_LLN0_BAT_Messungen_fcda0,
  &iedModelds_HEAT_LLN0_HEAT_Stammdaten
};

extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Stammdaten_fcda0;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Stammdaten_fcda1;

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Stammdaten_fcda0 = {
  "HEAT",
  false,
  "HEAT_DCTS1$MX$ThrmCapTot",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Stammdaten_fcda1
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Stammdaten_fcda1 = {
  "HEAT",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_HEAT_LLN0_HEAT_Stammdaten = {
  "HEAT",
  "LLN0$HEAT_Stammdaten",
  2,
  &iedModelds_HEAT_LLN0_HEAT_Stammdaten_fcda0,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus
};

extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda2;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda3;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda4;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda5;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda6;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda7;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda8;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda9;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda10;

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda0 = {
  "HEAT",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda1 = {
  "HEAT",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda2 = {
  "HEAT",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda3
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda3 = {
  "HEAT",
  false,
  "HEAT_DCTS1$ST$OpTmh",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda4
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda4 = {
  "HEAT",
  false,
  "HEAT_STMP1$SP$MaxTmp",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda5
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda5 = {
  "HEAT",
  false,
  "HEAT_STMP1$SP$MinTmp",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda6
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda6 = {
  "HEAT",
  false,
  "HEAT_ZCTS1$SP$TmpRtg",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda7
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda7 = {
  "HEAT",
  false,
  "HEAT_ZCTS1$SP$TmpSet",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda8
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda8 = {
  "HEAT",
  false,
  "HEAT_ZCTS1$SP$OpMod",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda9
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda9 = {
  "HEAT",
  false,
  "HEAT_ZCTS1$SP$OutWSet",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda10
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda10 = {
  "HEAT",
  false,
  "HEAT_ZCTS1$SP$InWSet",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_HEAT_LLN0_HEAT_Betriebsstatus = {
  "HEAT",
  "LLN0$HEAT_Betriebsstatus",
  11,
  &iedModelds_HEAT_LLN0_HEAT_Betriebsstatus_fcda0,
  &iedModelds_HEAT_LLN0_HEAT_Messungen
};

extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Messungen_fcda0;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Messungen_fcda1;
extern DataSetEntry iedModelds_HEAT_LLN0_HEAT_Messungen_fcda2;

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Messungen_fcda0 = {
  "HEAT",
  false,
  "HEAT_DCTS1$MX$ThrmCapPct",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Messungen_fcda1
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Messungen_fcda1 = {
  "HEAT",
  false,
  "HEAT_STMP1$MX$Tmp",
  -1,
  NULL,
  NULL,
  &iedModelds_HEAT_LLN0_HEAT_Messungen_fcda2
};

DataSetEntry iedModelds_HEAT_LLN0_HEAT_Messungen_fcda2 = {
  "HEAT",
  false,
  "HEAT_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_HEAT_LLN0_HEAT_Messungen = {
  "HEAT",
  "LLN0$HEAT_Messungen",
  3,
  &iedModelds_HEAT_LLN0_HEAT_Messungen_fcda0,
  &iedModelds_EMOB_LLN0_EMOB_Stammdaten
};

extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda0;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda1;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda2;

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda0 = {
  "EMOB",
  false,
  "EMOB_DESE1$DC$EVSENam",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda1
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda1 = {
  "EMOB",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda2
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda2 = {
  "EMOB",
  false,
  "EMOB_DESE1$SP$ChaPwrRtg",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_EMOB_LLN0_EMOB_Stammdaten = {
  "EMOB",
  "LLN0$EMOB_Stammdaten",
  3,
  &iedModelds_EMOB_LLN0_EMOB_Stammdaten_fcda0,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus
};

extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda2;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda3;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda4;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda5;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda6;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda7;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda8;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda9;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda10;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda11;

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda0 = {
  "EMOB",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda1 = {
  "EMOB",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda2 = {
  "EMOB",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda3
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda3 = {
  "EMOB",
  false,
  "EMOB_DESE1$SP$ChaPwrLim",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda4
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda4 = {
  "EMOB",
  false,
  "EMOB_DEAO1$ST$ConnSt",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda5
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda5 = {
  "EMOB",
  false,
  "EMOB_DEAO1$ST$CabRtgAC",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda6
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda6 = {
  "EMOB",
  false,
  "EMOB_DEAO1$SP$ChaARtg",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda7
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda7 = {
  "EMOB",
  false,
  "EMOB_DEAO1$SP$ChaAMax",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda8
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda8 = {
  "EMOB",
  false,
  "EMOB_DEAO2$ST$ConnSt",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda9
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda9 = {
  "EMOB",
  false,
  "EMOB_DEAO2$ST$CabRtgAC",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda10
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda10 = {
  "EMOB",
  false,
  "EMOB_DEAO2$SP$ChaARtg",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda11
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda11 = {
  "EMOB",
  false,
  "EMOB_DEAO2$SP$ChaAMax",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_EMOB_LLN0_EMOB_Betriebsstatus = {
  "EMOB",
  "LLN0$EMOB_Betriebsstatus",
  12,
  &iedModelds_EMOB_LLN0_EMOB_Betriebsstatus_fcda0,
  &iedModelds_EMOB_LLN0_EMOB_Messungen
};

extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda0;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda1;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda2;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda3;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda4;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda5;
extern DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda6;

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda0 = {
  "EMOB",
  false,
  "EMOB_DESE1$MX$ChaA",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda1
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda1 = {
  "EMOB",
  false,
  "EMOB_DESE1$MX$ChaV",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda2
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda2 = {
  "EMOB",
  false,
  "EMOB_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda3
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda3 = {
  "EMOB",
  false,
  "EMOB_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda4
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda4 = {
  "EMOB",
  false,
  "EMOB_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda5
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda5 = {
  "EMOB",
  false,
  "EMOB_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda6
};

DataSetEntry iedModelds_EMOB_LLN0_EMOB_Messungen_fcda6 = {
  "EMOB",
  false,
  "EMOB_MMXU1$MX$A",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_EMOB_LLN0_EMOB_Messungen = {
  "EMOB",
  "LLN0$EMOB_Messungen",
  7,
  &iedModelds_EMOB_LLN0_EMOB_Messungen_fcda0,
  &iedModelds_MEAS_LLN0_MEAS_Stammdaten
};

extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Stammdaten_fcda0;

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Stammdaten_fcda0 = {
  "MEAS",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_MEAS_LLN0_MEAS_Stammdaten = {
  "MEAS",
  "LLN0$MEAS_Stammdaten",
  1,
  &iedModelds_MEAS_LLN0_MEAS_Stammdaten_fcda0,
  &iedModelds_MEAS_LLN0_MEAS_Betriebsstatus
};

extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda2;

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda0 = {
  "MEAS",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda1 = {
  "MEAS",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda2 = {
  "MEAS",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_MEAS_LLN0_MEAS_Betriebsstatus = {
  "MEAS",
  "LLN0$MEAS_Betriebsstatus",
  3,
  &iedModelds_MEAS_LLN0_MEAS_Betriebsstatus_fcda0,
  &iedModelds_MEAS_LLN0_MEAS_Messungen
};

extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda0;
extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda1;
extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda2;
extern DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda3;

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda0 = {
  "MEAS",
  false,
  "MEAS_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS_LLN0_MEAS_Messungen_fcda1
};

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda1 = {
  "MEAS",
  false,
  "MEAS_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS_LLN0_MEAS_Messungen_fcda2
};

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda2 = {
  "MEAS",
  false,
  "MEAS_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS_LLN0_MEAS_Messungen_fcda3
};

DataSetEntry iedModelds_MEAS_LLN0_MEAS_Messungen_fcda3 = {
  "MEAS",
  false,
  "MEAS_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_MEAS_LLN0_MEAS_Messungen = {
  "MEAS",
  "LLN0$MEAS_Messungen",
  4,
  &iedModelds_MEAS_LLN0_MEAS_Messungen_fcda0,
  &iedModelds_MEAS2_LLN0_MEAS_Stammdaten
};

extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Stammdaten_fcda0;

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Stammdaten_fcda0 = {
  "MEAS2",
  false,
  "LLN0$DC$NamPlt",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_MEAS2_LLN0_MEAS_Stammdaten = {
  "MEAS2",
  "LLN0$MEAS_Stammdaten",
  1,
  &iedModelds_MEAS2_LLN0_MEAS_Stammdaten_fcda0,
  &iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus
};

extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda0;
extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda1;
extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda2;

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda0 = {
  "MEAS2",
  false,
  "LLN0$ST$Health",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda1
};

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda1 = {
  "MEAS2",
  false,
  "LLN0$ST$Mod",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda2
};

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda2 = {
  "MEAS2",
  false,
  "LLN0$ST$Beh",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus = {
  "MEAS2",
  "LLN0$MEAS_Betriebsstatus",
  3,
  &iedModelds_MEAS2_LLN0_MEAS_Betriebsstatus_fcda0,
  &iedModelds_MEAS2_LLN0_MEAS_Messungen
};

extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda0;
extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda1;
extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda2;
extern DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda3;

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda0 = {
  "MEAS2",
  false,
  "MEAS_MMXU1$MX$TotW",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda1
};

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda1 = {
  "MEAS2",
  false,
  "MEAS_MMXU1$MX$TotVAr",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda2
};

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda2 = {
  "MEAS2",
  false,
  "MEAS_MMXU1$MX$PhV",
  -1,
  NULL,
  NULL,
  &iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda3
};

DataSetEntry iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda3 = {
  "MEAS2",
  false,
  "MEAS_MMXU1$MX$Hz",
  -1,
  NULL,
  NULL,
  NULL
};

DataSet iedModelds_MEAS2_LLN0_MEAS_Messungen = {
  "MEAS2",
  "LLN0$MEAS_Messungen",
  4,
  &iedModelds_MEAS2_LLN0_MEAS_Messungen_fcda0,
  NULL
};

LogicalDevice iedModel_CLS = {
    LogicalDeviceModelType,
    "CLS",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_PV,
    (ModelNode*) &iedModel_CLS_LLN0
};

LogicalNode iedModel_CLS_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_CLS,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1,
    (ModelNode*) &iedModel_CLS_LLN0_Mod,
};

DataObject iedModel_CLS_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_CLS_LLN0,
    (ModelNode*) &iedModel_CLS_LLN0_Beh,
    (ModelNode*) &iedModel_CLS_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_CLS_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_LLN0_Mod,
    (ModelNode*) &iedModel_CLS_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_LLN0_Mod,
    (ModelNode*) &iedModel_CLS_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_LLN0_Mod,
    (ModelNode*) &iedModel_CLS_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_CLS_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_CLS_LLN0,
    (ModelNode*) &iedModel_CLS_LLN0_Health,
    (ModelNode*) &iedModel_CLS_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_CLS_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_LLN0_Beh,
    (ModelNode*) &iedModel_CLS_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_LLN0_Beh,
    (ModelNode*) &iedModel_CLS_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_CLS_LLN0,
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt,
    (ModelNode*) &iedModel_CLS_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_CLS_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_LLN0_Health,
    (ModelNode*) &iedModel_CLS_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_LLN0_Health,
    (ModelNode*) &iedModel_CLS_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_LLN0_Health,
    (ModelNode*) &iedModel_CLS_LLN0_Health_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_CLS_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_CLS_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_CLS_LLN0,
    (ModelNode*) &iedModel_CLS_LLN0_Loc,
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_CLS_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt,
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt,
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt,
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt_configRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_NamPlt_configRev = {
    DataAttributeModelType,
    "configRev",
    (ModelNode*) &iedModel_CLS_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_CLS_LLN0_Loc = {
    DataObjectModelType,
    "Loc",
    (ModelNode*) &iedModel_CLS_LLN0,
    NULL,
    (ModelNode*) &iedModel_CLS_LLN0_Loc_stVal,
    0
};

DataAttribute iedModel_CLS_LLN0_Loc_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_LLN0_Loc,
    (ModelNode*) &iedModel_CLS_LLN0_Loc_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Loc_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_LLN0_Loc,
    (ModelNode*) &iedModel_CLS_LLN0_Loc_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_LLN0_Loc_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_LLN0_Loc,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

LogicalNode iedModel_CLS_CLS_LPHD1 = {
    LogicalNodeModelType,
    "CLS_LPHD1",
    (ModelNode*) &iedModel_CLS,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh,
};

DataObject iedModel_CLS_CLS_LPHD1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh_stVal,
    0
};

DataAttribute iedModel_CLS_CLS_LPHD1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_CLS_LPHD1_PhyNam = {
    DataObjectModelType,
    "PhyNam",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam_vendor,
    0
};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyNam_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam_hwRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyNam_hwRev = {
    DataAttributeModelType,
    "hwRev",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyNam_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam_serNum,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyNam_serNum = {
    DataAttributeModelType,
    "serNum",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam_model,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyNam_model = {
    DataAttributeModelType,
    "model",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyNam,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_CLS_CLS_LPHD1_PhyHealth = {
    DataObjectModelType,
    "PhyHealth",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth_stVal,
    0
};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyHealth_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyHealth_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyHealth_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_PhyHealth_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_PhyHealth,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_CLS_CLS_LPHD1_Proxy = {
    DataObjectModelType,
    "Proxy",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy_stVal,
    0
};

DataAttribute iedModel_CLS_CLS_LPHD1_Proxy_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Proxy_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Proxy_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Proxy,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_CLS_LPHD1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1,
    NULL,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod_stVal,
    0
};

DataAttribute iedModel_CLS_CLS_LPHD1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod,
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_CLS_LPHD1_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_CLS_LPHD1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

LogicalNode iedModel_CLS_PCC_MMXU1 = {
    LogicalNodeModelType,
    "PCC_MMXU1",
    (ModelNode*) &iedModel_CLS,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh,
};

DataObject iedModel_CLS_PCC_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health_stVal,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt_vendor,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_q,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotVAr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_TotVAr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_TotVAr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_q,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_CLS_PCC_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_PCC_MMXU1_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1,
    NULL,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod_stVal,
    0
};

DataAttribute iedModel_CLS_PCC_MMXU1_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod,
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_PCC_MMXU1_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_PCC_MMXU1_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

LogicalNode iedModel_CLS_SCHD_FSCH1 = {
    LogicalNodeModelType,
    "SCHD_FSCH1",
    (ModelNode*) &iedModel_CLS,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh,
};

DataObject iedModel_CLS_SCHD_FSCH1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh_stVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_SchdSt = {
    DataObjectModelType,
    "SchdSt",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_NumEntr,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt_stVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_NumEntr = {
    DataObjectModelType,
    "NumEntr",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_NumEntr_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_NumEntr_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_NumEntr,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_SchdIntv = {
    DataObjectModelType,
    "SchdIntv",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdIntv_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv_units,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdIntv_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdIntv_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdIntv_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValMV = {
    DataObjectModelType,
    "ValMV",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdPrio,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV_mag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValMV_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV_q,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValMV_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValMV_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValMV_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValMV,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_SchdPrio = {
    DataObjectModelType,
    "SchdPrio",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdPrio_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdPrio_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdPrio,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_StrTm = {
    DataObjectModelType,
    "StrTm",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvPer,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal = {
    DataAttributeModelType,
    "setCal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_occ,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_occ = {
    DataAttributeModelType,
    "occ",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_occType,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT16U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_occType = {
    DataAttributeModelType,
    "occType",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_occPer,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_occPer = {
    DataAttributeModelType,
    "occPer",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_weekDay,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_weekDay = {
    DataAttributeModelType,
    "weekDay",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_month,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_month = {
    DataAttributeModelType,
    "month",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_day,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_day = {
    DataAttributeModelType,
    "day",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_hr,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_hr = {
    DataAttributeModelType,
    "hr",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal_mn,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_StrTm_setCal_mn = {
    DataAttributeModelType,
    "mn",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_StrTm_setCal,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_IntvPer = {
    DataObjectModelType,
    "IntvPer",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvPer_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_IntvPer_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvPer,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_IntvTyp = {
    DataObjectModelType,
    "IntvTyp",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EvTrg,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_IntvTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_IntvTyp_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_IntvTyp_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_IntvTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_EvTrg = {
    DataObjectModelType,
    "EvTrg",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_InSyn,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EvTrg_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_EvTrg_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EvTrg,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_InSyn = {
    DataObjectModelType,
    "InSyn",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_InSyn_setSrcRef,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_InSyn_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_InSyn,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_EnaReq = {
    DataObjectModelType,
    "EnaReq",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_ctlModel,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_Check,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_Oper,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_EnaReq_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_EnaReq,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_DsaReq = {
    DataObjectModelType,
    "DsaReq",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdReuse,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_ctlModel,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_Check,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_Oper,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_DsaReq_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_DsaReq,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_SchdReuse = {
    DataObjectModelType,
    "SchdReuse",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ZReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdReuse_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_SchdReuse_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_SchdReuse,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ZReq = {
    DataObjectModelType,
    "ZReq",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0001,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ZReq_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ZReq_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ZReq,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0001 = {
    DataObjectModelType,
    "ValASG0001",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0002,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0001_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0001_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0001,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0001_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0001_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0001_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0002 = {
    DataObjectModelType,
    "ValASG0002",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0003,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0002_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0002_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0002,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0002_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0002_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0002_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0003 = {
    DataObjectModelType,
    "ValASG0003",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0004,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0003_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0003_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0003,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0003_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0003_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0003_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0004 = {
    DataObjectModelType,
    "ValASG0004",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0005,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0004_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0004_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0004,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0004_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0004_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0004_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0005 = {
    DataObjectModelType,
    "ValASG0005",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0006,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0005_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0005_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0005,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0005_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0005_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0005_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0006 = {
    DataObjectModelType,
    "ValASG0006",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0007,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0006_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0006_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0006,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0006_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0006_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0006_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0007 = {
    DataObjectModelType,
    "ValASG0007",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0008,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0007_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0007_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0007,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0007_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0007_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0007_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0008 = {
    DataObjectModelType,
    "ValASG0008",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0009,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0008_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0008_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0008,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0008_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0008_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0008_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0009 = {
    DataObjectModelType,
    "ValASG0009",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0010,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0009_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0009_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0009,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0009_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0009_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0009_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0010 = {
    DataObjectModelType,
    "ValASG0010",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0011,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0010_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0010_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0010,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0010_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0010_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0010_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0011 = {
    DataObjectModelType,
    "ValASG0011",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0012,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0011_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0011_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0011,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0011_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0011_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0011_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0012 = {
    DataObjectModelType,
    "ValASG0012",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0013,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0012_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0012_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0012,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0012_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0012_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0012_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0013 = {
    DataObjectModelType,
    "ValASG0013",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0014,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0013_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0013_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0013,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0013_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0013_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0013_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0014 = {
    DataObjectModelType,
    "ValASG0014",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0015,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0014_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0014_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0014,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0014_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0014_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0014_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0015 = {
    DataObjectModelType,
    "ValASG0015",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0016,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0015_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0015_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0015,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0015_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0015_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0015_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0016 = {
    DataObjectModelType,
    "ValASG0016",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0017,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0016_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0016_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0016,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0016_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0016_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0016_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0017 = {
    DataObjectModelType,
    "ValASG0017",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0018,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0017_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0017_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0017,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0017_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0017_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0017_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0018 = {
    DataObjectModelType,
    "ValASG0018",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0019,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0018_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0018_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0018,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0018_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0018_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0018_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0019 = {
    DataObjectModelType,
    "ValASG0019",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0020,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0019_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0019_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0019,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0019_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0019_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0019_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0020 = {
    DataObjectModelType,
    "ValASG0020",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0021,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0020_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0020_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0020,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0020_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0020_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0020_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0021 = {
    DataObjectModelType,
    "ValASG0021",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0022,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0021_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0021_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0021,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0021_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0021_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0021_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0022 = {
    DataObjectModelType,
    "ValASG0022",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0023,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0022_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0022_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0022,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0022_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0022_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0022_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0023 = {
    DataObjectModelType,
    "ValASG0023",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0024,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0023_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0023_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0023,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0023_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0023_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0023_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0024 = {
    DataObjectModelType,
    "ValASG0024",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0025,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0024_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0024_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0024,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0024_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0024_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0024_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0025 = {
    DataObjectModelType,
    "ValASG0025",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0026,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0025_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0025_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0025,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0025_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0025_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0025_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0026 = {
    DataObjectModelType,
    "ValASG0026",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0027,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0026_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0026_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0026,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0026_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0026_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0026_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0027 = {
    DataObjectModelType,
    "ValASG0027",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0028,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0027_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0027_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0027,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0027_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0027_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0027_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0028 = {
    DataObjectModelType,
    "ValASG0028",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0029,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0028_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0028_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0028,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0028_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0028_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0028_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0029 = {
    DataObjectModelType,
    "ValASG0029",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0030,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0029_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0029_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0029,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0029_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0029_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0029_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0030 = {
    DataObjectModelType,
    "ValASG0030",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0031,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0030_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0030_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0030,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0030_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0030_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0030_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0031 = {
    DataObjectModelType,
    "ValASG0031",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0032,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0031_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0031_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0031,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0031_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0031_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0031_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0032 = {
    DataObjectModelType,
    "ValASG0032",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0033,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0032_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0032_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0032,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0032_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0032_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0032_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0033 = {
    DataObjectModelType,
    "ValASG0033",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0034,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0033_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0033_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0033,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0033_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0033_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0033_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0034 = {
    DataObjectModelType,
    "ValASG0034",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0035,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0034_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0034_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0034,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0034_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0034_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0034_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0035 = {
    DataObjectModelType,
    "ValASG0035",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0036,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0035_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0035_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0035,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0035_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0035_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0035_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0036 = {
    DataObjectModelType,
    "ValASG0036",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0037,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0036_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0036_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0036,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0036_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0036_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0036_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0037 = {
    DataObjectModelType,
    "ValASG0037",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0038,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0037_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0037_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0037,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0037_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0037_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0037_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0038 = {
    DataObjectModelType,
    "ValASG0038",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0039,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0038_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0038_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0038,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0038_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0038_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0038_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0039 = {
    DataObjectModelType,
    "ValASG0039",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0040,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0039_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0039_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0039,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0039_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0039_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0039_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0040 = {
    DataObjectModelType,
    "ValASG0040",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0041,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0040_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0040_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0040,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0040_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0040_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0040_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0041 = {
    DataObjectModelType,
    "ValASG0041",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0042,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0041_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0041_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0041,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0041_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0041_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0041_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0042 = {
    DataObjectModelType,
    "ValASG0042",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0043,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0042_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0042_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0042,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0042_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0042_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0042_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0043 = {
    DataObjectModelType,
    "ValASG0043",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0044,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0043_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0043_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0043,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0043_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0043_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0043_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0044 = {
    DataObjectModelType,
    "ValASG0044",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0045,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0044_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0044_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0044,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0044_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0044_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0044_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0045 = {
    DataObjectModelType,
    "ValASG0045",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0046,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0045_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0045_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0045,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0045_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0045_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0045_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0046 = {
    DataObjectModelType,
    "ValASG0046",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0047,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0046_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0046_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0046,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0046_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0046_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0046_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0047 = {
    DataObjectModelType,
    "ValASG0047",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0048,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0047_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0047_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0047,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0047_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0047_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0047_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0048 = {
    DataObjectModelType,
    "ValASG0048",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0049,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0048_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0048_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0048,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0048_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0048_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0048_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0049 = {
    DataObjectModelType,
    "ValASG0049",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0050,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0049_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0049_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0049,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0049_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0049_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0049_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0050 = {
    DataObjectModelType,
    "ValASG0050",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0051,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0050_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0050_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0050,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0050_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0050_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0050_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0051 = {
    DataObjectModelType,
    "ValASG0051",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0052,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0051_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0051_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0051,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0051_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0051_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0051_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0052 = {
    DataObjectModelType,
    "ValASG0052",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0053,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0052_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0052_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0052,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0052_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0052_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0052_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0053 = {
    DataObjectModelType,
    "ValASG0053",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0054,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0053_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0053_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0053,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0053_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0053_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0053_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0054 = {
    DataObjectModelType,
    "ValASG0054",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0055,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0054_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0054_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0054,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0054_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0054_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0054_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0055 = {
    DataObjectModelType,
    "ValASG0055",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0056,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0055_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0055_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0055,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0055_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0055_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0055_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0056 = {
    DataObjectModelType,
    "ValASG0056",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0057,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0056_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0056_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0056,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0056_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0056_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0056_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0057 = {
    DataObjectModelType,
    "ValASG0057",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0058,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0057_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0057_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0057,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0057_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0057_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0057_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0058 = {
    DataObjectModelType,
    "ValASG0058",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0059,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0058_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0058_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0058,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0058_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0058_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0058_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0059 = {
    DataObjectModelType,
    "ValASG0059",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0060,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0059_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0059_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0059,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0059_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0059_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0059_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0060 = {
    DataObjectModelType,
    "ValASG0060",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0061,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0060_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0060_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0060,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0060_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0060_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0060_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0061 = {
    DataObjectModelType,
    "ValASG0061",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0062,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0061_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0061_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0061,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0061_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0061_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0061_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0062 = {
    DataObjectModelType,
    "ValASG0062",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0063,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0062_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0062_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0062,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0062_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0062_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0062_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0063 = {
    DataObjectModelType,
    "ValASG0063",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0064,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0063_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0063_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0063,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0063_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0063_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0063_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0064 = {
    DataObjectModelType,
    "ValASG0064",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0065,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0064_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0064_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0064,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0064_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0064_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0064_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0065 = {
    DataObjectModelType,
    "ValASG0065",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0066,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0065_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0065_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0065,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0065_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0065_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0065_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0066 = {
    DataObjectModelType,
    "ValASG0066",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0067,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0066_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0066_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0066,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0066_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0066_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0066_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0067 = {
    DataObjectModelType,
    "ValASG0067",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0068,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0067_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0067_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0067,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0067_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0067_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0067_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0068 = {
    DataObjectModelType,
    "ValASG0068",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0069,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0068_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0068_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0068,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0068_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0068_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0068_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0069 = {
    DataObjectModelType,
    "ValASG0069",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0070,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0069_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0069_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0069,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0069_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0069_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0069_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0070 = {
    DataObjectModelType,
    "ValASG0070",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0071,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0070_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0070_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0070,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0070_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0070_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0070_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0071 = {
    DataObjectModelType,
    "ValASG0071",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0072,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0071_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0071_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0071,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0071_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0071_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0071_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0072 = {
    DataObjectModelType,
    "ValASG0072",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0073,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0072_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0072_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0072,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0072_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0072_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0072_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0073 = {
    DataObjectModelType,
    "ValASG0073",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0074,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0073_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0073_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0073,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0073_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0073_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0073_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0074 = {
    DataObjectModelType,
    "ValASG0074",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0075,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0074_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0074_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0074,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0074_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0074_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0074_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0075 = {
    DataObjectModelType,
    "ValASG0075",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0076,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0075_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0075_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0075,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0075_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0075_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0075_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0076 = {
    DataObjectModelType,
    "ValASG0076",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0077,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0076_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0076_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0076,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0076_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0076_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0076_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0077 = {
    DataObjectModelType,
    "ValASG0077",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0078,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0077_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0077_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0077,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0077_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0077_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0077_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0078 = {
    DataObjectModelType,
    "ValASG0078",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0079,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0078_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0078_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0078,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0078_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0078_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0078_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0079 = {
    DataObjectModelType,
    "ValASG0079",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0080,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0079_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0079_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0079,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0079_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0079_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0079_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0080 = {
    DataObjectModelType,
    "ValASG0080",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0081,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0080_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0080_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0080,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0080_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0080_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0080_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0081 = {
    DataObjectModelType,
    "ValASG0081",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0082,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0081_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0081_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0081,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0081_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0081_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0081_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0082 = {
    DataObjectModelType,
    "ValASG0082",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0083,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0082_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0082_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0082,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0082_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0082_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0082_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0083 = {
    DataObjectModelType,
    "ValASG0083",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0084,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0083_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0083_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0083,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0083_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0083_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0083_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0084 = {
    DataObjectModelType,
    "ValASG0084",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0085,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0084_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0084_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0084,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0084_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0084_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0084_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0085 = {
    DataObjectModelType,
    "ValASG0085",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0086,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0085_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0085_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0085,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0085_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0085_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0085_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0086 = {
    DataObjectModelType,
    "ValASG0086",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0087,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0086_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0086_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0086,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0086_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0086_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0086_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0087 = {
    DataObjectModelType,
    "ValASG0087",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0088,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0087_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0087_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0087,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0087_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0087_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0087_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0088 = {
    DataObjectModelType,
    "ValASG0088",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0089,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0088_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0088_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0088,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0088_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0088_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0088_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0089 = {
    DataObjectModelType,
    "ValASG0089",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0090,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0089_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0089_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0089,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0089_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0089_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0089_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0090 = {
    DataObjectModelType,
    "ValASG0090",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0091,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0090_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0090_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0090,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0090_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0090_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0090_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0091 = {
    DataObjectModelType,
    "ValASG0091",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0092,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0091_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0091_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0091,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0091_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0091_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0091_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0092 = {
    DataObjectModelType,
    "ValASG0092",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0093,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0092_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0092_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0092,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0092_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0092_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0092_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0093 = {
    DataObjectModelType,
    "ValASG0093",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0094,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0093_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0093_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0093,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0093_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0093_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0093_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0094 = {
    DataObjectModelType,
    "ValASG0094",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0095,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0094_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0094_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0094,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0094_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0094_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0094_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0095 = {
    DataObjectModelType,
    "ValASG0095",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0096,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0095_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0095_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0095,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0095_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0095_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0095_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0096 = {
    DataObjectModelType,
    "ValASG0096",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0097,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0096_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0096_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0096,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0096_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0096_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0096_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0097 = {
    DataObjectModelType,
    "ValASG0097",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0098,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0097_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0097_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0097,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0097_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0097_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0097_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0098 = {
    DataObjectModelType,
    "ValASG0098",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0099,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0098_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0098_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0098,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0098_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0098_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0098_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0099 = {
    DataObjectModelType,
    "ValASG0099",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0100,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0099_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0099_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0099,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0099_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0099_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0099_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH1_ValASG0100 = {
    DataObjectModelType,
    "ValASG0100",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0100_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0100_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0100,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0100_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH1_ValASG0100_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH1_ValASG0100_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_CLS_SCHD_FSCH2 = {
    LogicalNodeModelType,
    "SCHD_FSCH2",
    (ModelNode*) &iedModel_CLS,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh,
};

DataObject iedModel_CLS_SCHD_FSCH2_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh_stVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_SchdSt = {
    DataObjectModelType,
    "SchdSt",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_NumEntr,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt_stVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_NumEntr = {
    DataObjectModelType,
    "NumEntr",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_NumEntr_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_NumEntr_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_NumEntr,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_SchdIntv = {
    DataObjectModelType,
    "SchdIntv",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdIntv_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv_units,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdIntv_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdIntv_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdIntv_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValMV = {
    DataObjectModelType,
    "ValMV",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdPrio,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV_mag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValMV_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV_q,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValMV_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValMV_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValMV_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValMV,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_SchdPrio = {
    DataObjectModelType,
    "SchdPrio",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdPrio_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdPrio_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdPrio,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_StrTm = {
    DataObjectModelType,
    "StrTm",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvPer,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal = {
    DataAttributeModelType,
    "setCal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_occ,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_occ = {
    DataAttributeModelType,
    "occ",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_occType,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT16U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_occType = {
    DataAttributeModelType,
    "occType",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_occPer,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_occPer = {
    DataAttributeModelType,
    "occPer",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_weekDay,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_weekDay = {
    DataAttributeModelType,
    "weekDay",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_month,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_month = {
    DataAttributeModelType,
    "month",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_day,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_day = {
    DataAttributeModelType,
    "day",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_hr,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_hr = {
    DataAttributeModelType,
    "hr",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal_mn,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_StrTm_setCal_mn = {
    DataAttributeModelType,
    "mn",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_StrTm_setCal,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_IntvPer = {
    DataObjectModelType,
    "IntvPer",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvPer_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_IntvPer_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvPer,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_IntvTyp = {
    DataObjectModelType,
    "IntvTyp",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EvTrg,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_IntvTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_IntvTyp_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_IntvTyp_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_IntvTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_EvTrg = {
    DataObjectModelType,
    "EvTrg",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_InSyn,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EvTrg_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_EvTrg_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EvTrg,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_InSyn = {
    DataObjectModelType,
    "InSyn",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_InSyn_setSrcRef,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_InSyn_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_InSyn,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_EnaReq = {
    DataObjectModelType,
    "EnaReq",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_ctlModel,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_Check,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_Oper,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_EnaReq_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_EnaReq,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_DsaReq = {
    DataObjectModelType,
    "DsaReq",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdReuse,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper = {
    DataAttributeModelType,
    "Oper",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_ctlModel,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_ctlVal,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_ctlVal = {
    DataAttributeModelType,
    "ctlVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin_orCat,
    0,
    IEC61850_FC_CO,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_T,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_T = {
    DataAttributeModelType,
    "T",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_Test,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_Test = {
    DataAttributeModelType,
    "Test",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_Check,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_BOOLEAN,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_Oper_Check = {
    DataAttributeModelType,
    "Check",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_Oper,
    NULL,
    NULL,
    0,
    IEC61850_FC_CO,
    IEC61850_CHECK,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_origin,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_origin = {
    DataAttributeModelType,
    "origin",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_ctlNum,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_origin_orCat,
    0,
    IEC61850_FC_ST,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_origin_orCat = {
    DataAttributeModelType,
    "orCat",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_origin,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_origin_orIdent,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_origin_orIdent = {
    DataAttributeModelType,
    "orIdent",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_origin,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_OCTET_STRING_64,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_ctlNum = {
    DataAttributeModelType,
    "ctlNum",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_stVal,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT8U,
    0,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_DsaReq_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_DsaReq,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_SchdReuse = {
    DataObjectModelType,
    "SchdReuse",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ZReq,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdReuse_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_SchdReuse_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_SchdReuse,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ZReq = {
    DataObjectModelType,
    "ZReq",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0001,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ZReq_setVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ZReq_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ZReq,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0001 = {
    DataObjectModelType,
    "ValASG0001",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0002,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0001_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0001_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0001,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0001_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0001_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0001_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0002 = {
    DataObjectModelType,
    "ValASG0002",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0003,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0002_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0002_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0002,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0002_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0002_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0002_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0003 = {
    DataObjectModelType,
    "ValASG0003",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0004,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0003_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0003_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0003,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0003_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0003_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0003_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0004 = {
    DataObjectModelType,
    "ValASG0004",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0005,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0004_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0004_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0004,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0004_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0004_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0004_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0005 = {
    DataObjectModelType,
    "ValASG0005",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0006,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0005_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0005_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0005,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0005_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0005_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0005_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0006 = {
    DataObjectModelType,
    "ValASG0006",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0007,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0006_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0006_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0006,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0006_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0006_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0006_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0007 = {
    DataObjectModelType,
    "ValASG0007",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0008,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0007_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0007_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0007,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0007_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0007_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0007_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0008 = {
    DataObjectModelType,
    "ValASG0008",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0009,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0008_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0008_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0008,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0008_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0008_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0008_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0009 = {
    DataObjectModelType,
    "ValASG0009",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0010,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0009_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0009_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0009,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0009_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0009_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0009_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0010 = {
    DataObjectModelType,
    "ValASG0010",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0011,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0010_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0010_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0010,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0010_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0010_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0010_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0011 = {
    DataObjectModelType,
    "ValASG0011",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0012,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0011_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0011_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0011,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0011_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0011_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0011_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0012 = {
    DataObjectModelType,
    "ValASG0012",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0013,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0012_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0012_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0012,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0012_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0012_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0012_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0013 = {
    DataObjectModelType,
    "ValASG0013",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0014,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0013_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0013_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0013,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0013_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0013_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0013_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0014 = {
    DataObjectModelType,
    "ValASG0014",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0015,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0014_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0014_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0014,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0014_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0014_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0014_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0015 = {
    DataObjectModelType,
    "ValASG0015",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0016,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0015_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0015_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0015,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0015_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0015_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0015_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0016 = {
    DataObjectModelType,
    "ValASG0016",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0017,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0016_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0016_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0016,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0016_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0016_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0016_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0017 = {
    DataObjectModelType,
    "ValASG0017",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0018,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0017_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0017_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0017,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0017_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0017_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0017_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0018 = {
    DataObjectModelType,
    "ValASG0018",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0019,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0018_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0018_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0018,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0018_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0018_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0018_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0019 = {
    DataObjectModelType,
    "ValASG0019",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0020,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0019_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0019_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0019,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0019_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0019_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0019_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0020 = {
    DataObjectModelType,
    "ValASG0020",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0021,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0020_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0020_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0020,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0020_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0020_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0020_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0021 = {
    DataObjectModelType,
    "ValASG0021",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0022,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0021_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0021_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0021,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0021_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0021_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0021_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0022 = {
    DataObjectModelType,
    "ValASG0022",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0023,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0022_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0022_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0022,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0022_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0022_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0022_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0023 = {
    DataObjectModelType,
    "ValASG0023",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0024,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0023_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0023_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0023,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0023_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0023_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0023_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0024 = {
    DataObjectModelType,
    "ValASG0024",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0025,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0024_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0024_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0024,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0024_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0024_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0024_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0025 = {
    DataObjectModelType,
    "ValASG0025",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0026,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0025_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0025_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0025,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0025_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0025_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0025_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0026 = {
    DataObjectModelType,
    "ValASG0026",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0027,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0026_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0026_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0026,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0026_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0026_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0026_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0027 = {
    DataObjectModelType,
    "ValASG0027",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0028,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0027_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0027_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0027,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0027_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0027_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0027_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0028 = {
    DataObjectModelType,
    "ValASG0028",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0029,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0028_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0028_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0028,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0028_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0028_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0028_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0029 = {
    DataObjectModelType,
    "ValASG0029",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0030,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0029_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0029_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0029,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0029_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0029_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0029_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0030 = {
    DataObjectModelType,
    "ValASG0030",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0031,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0030_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0030_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0030,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0030_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0030_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0030_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0031 = {
    DataObjectModelType,
    "ValASG0031",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0032,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0031_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0031_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0031,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0031_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0031_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0031_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0032 = {
    DataObjectModelType,
    "ValASG0032",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0033,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0032_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0032_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0032,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0032_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0032_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0032_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0033 = {
    DataObjectModelType,
    "ValASG0033",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0034,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0033_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0033_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0033,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0033_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0033_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0033_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0034 = {
    DataObjectModelType,
    "ValASG0034",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0035,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0034_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0034_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0034,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0034_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0034_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0034_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0035 = {
    DataObjectModelType,
    "ValASG0035",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0036,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0035_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0035_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0035,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0035_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0035_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0035_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0036 = {
    DataObjectModelType,
    "ValASG0036",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0037,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0036_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0036_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0036,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0036_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0036_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0036_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0037 = {
    DataObjectModelType,
    "ValASG0037",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0038,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0037_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0037_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0037,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0037_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0037_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0037_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0038 = {
    DataObjectModelType,
    "ValASG0038",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0039,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0038_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0038_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0038,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0038_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0038_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0038_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0039 = {
    DataObjectModelType,
    "ValASG0039",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0040,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0039_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0039_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0039,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0039_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0039_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0039_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0040 = {
    DataObjectModelType,
    "ValASG0040",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0041,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0040_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0040_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0040,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0040_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0040_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0040_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0041 = {
    DataObjectModelType,
    "ValASG0041",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0042,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0041_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0041_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0041,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0041_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0041_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0041_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0042 = {
    DataObjectModelType,
    "ValASG0042",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0043,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0042_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0042_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0042,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0042_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0042_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0042_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0043 = {
    DataObjectModelType,
    "ValASG0043",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0044,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0043_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0043_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0043,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0043_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0043_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0043_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0044 = {
    DataObjectModelType,
    "ValASG0044",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0045,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0044_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0044_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0044,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0044_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0044_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0044_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0045 = {
    DataObjectModelType,
    "ValASG0045",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0046,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0045_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0045_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0045,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0045_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0045_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0045_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0046 = {
    DataObjectModelType,
    "ValASG0046",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0047,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0046_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0046_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0046,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0046_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0046_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0046_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0047 = {
    DataObjectModelType,
    "ValASG0047",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0048,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0047_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0047_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0047,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0047_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0047_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0047_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0048 = {
    DataObjectModelType,
    "ValASG0048",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0049,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0048_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0048_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0048,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0048_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0048_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0048_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0049 = {
    DataObjectModelType,
    "ValASG0049",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0050,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0049_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0049_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0049,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0049_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0049_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0049_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0050 = {
    DataObjectModelType,
    "ValASG0050",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0051,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0050_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0050_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0050,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0050_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0050_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0050_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0051 = {
    DataObjectModelType,
    "ValASG0051",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0052,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0051_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0051_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0051,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0051_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0051_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0051_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0052 = {
    DataObjectModelType,
    "ValASG0052",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0053,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0052_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0052_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0052,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0052_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0052_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0052_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0053 = {
    DataObjectModelType,
    "ValASG0053",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0054,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0053_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0053_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0053,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0053_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0053_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0053_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0054 = {
    DataObjectModelType,
    "ValASG0054",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0055,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0054_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0054_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0054,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0054_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0054_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0054_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0055 = {
    DataObjectModelType,
    "ValASG0055",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0056,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0055_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0055_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0055,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0055_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0055_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0055_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0056 = {
    DataObjectModelType,
    "ValASG0056",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0057,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0056_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0056_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0056,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0056_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0056_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0056_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0057 = {
    DataObjectModelType,
    "ValASG0057",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0058,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0057_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0057_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0057,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0057_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0057_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0057_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0058 = {
    DataObjectModelType,
    "ValASG0058",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0059,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0058_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0058_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0058,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0058_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0058_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0058_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0059 = {
    DataObjectModelType,
    "ValASG0059",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0060,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0059_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0059_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0059,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0059_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0059_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0059_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0060 = {
    DataObjectModelType,
    "ValASG0060",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0061,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0060_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0060_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0060,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0060_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0060_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0060_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0061 = {
    DataObjectModelType,
    "ValASG0061",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0062,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0061_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0061_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0061,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0061_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0061_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0061_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0062 = {
    DataObjectModelType,
    "ValASG0062",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0063,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0062_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0062_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0062,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0062_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0062_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0062_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0063 = {
    DataObjectModelType,
    "ValASG0063",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0064,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0063_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0063_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0063,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0063_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0063_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0063_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0064 = {
    DataObjectModelType,
    "ValASG0064",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0065,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0064_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0064_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0064,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0064_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0064_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0064_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0065 = {
    DataObjectModelType,
    "ValASG0065",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0066,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0065_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0065_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0065,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0065_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0065_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0065_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0066 = {
    DataObjectModelType,
    "ValASG0066",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0067,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0066_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0066_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0066,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0066_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0066_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0066_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0067 = {
    DataObjectModelType,
    "ValASG0067",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0068,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0067_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0067_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0067,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0067_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0067_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0067_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0068 = {
    DataObjectModelType,
    "ValASG0068",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0069,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0068_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0068_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0068,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0068_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0068_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0068_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0069 = {
    DataObjectModelType,
    "ValASG0069",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0070,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0069_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0069_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0069,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0069_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0069_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0069_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0070 = {
    DataObjectModelType,
    "ValASG0070",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0071,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0070_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0070_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0070,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0070_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0070_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0070_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0071 = {
    DataObjectModelType,
    "ValASG0071",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0072,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0071_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0071_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0071,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0071_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0071_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0071_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0072 = {
    DataObjectModelType,
    "ValASG0072",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0073,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0072_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0072_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0072,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0072_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0072_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0072_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0073 = {
    DataObjectModelType,
    "ValASG0073",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0074,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0073_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0073_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0073,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0073_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0073_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0073_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0074 = {
    DataObjectModelType,
    "ValASG0074",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0075,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0074_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0074_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0074,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0074_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0074_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0074_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0075 = {
    DataObjectModelType,
    "ValASG0075",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0076,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0075_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0075_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0075,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0075_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0075_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0075_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0076 = {
    DataObjectModelType,
    "ValASG0076",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0077,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0076_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0076_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0076,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0076_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0076_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0076_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0077 = {
    DataObjectModelType,
    "ValASG0077",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0078,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0077_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0077_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0077,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0077_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0077_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0077_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0078 = {
    DataObjectModelType,
    "ValASG0078",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0079,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0078_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0078_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0078,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0078_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0078_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0078_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0079 = {
    DataObjectModelType,
    "ValASG0079",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0080,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0079_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0079_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0079,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0079_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0079_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0079_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0080 = {
    DataObjectModelType,
    "ValASG0080",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0081,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0080_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0080_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0080,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0080_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0080_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0080_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0081 = {
    DataObjectModelType,
    "ValASG0081",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0082,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0081_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0081_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0081,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0081_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0081_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0081_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0082 = {
    DataObjectModelType,
    "ValASG0082",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0083,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0082_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0082_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0082,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0082_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0082_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0082_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0083 = {
    DataObjectModelType,
    "ValASG0083",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0084,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0083_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0083_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0083,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0083_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0083_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0083_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0084 = {
    DataObjectModelType,
    "ValASG0084",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0085,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0084_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0084_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0084,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0084_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0084_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0084_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0085 = {
    DataObjectModelType,
    "ValASG0085",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0086,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0085_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0085_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0085,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0085_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0085_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0085_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0086 = {
    DataObjectModelType,
    "ValASG0086",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0087,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0086_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0086_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0086,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0086_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0086_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0086_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0087 = {
    DataObjectModelType,
    "ValASG0087",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0088,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0087_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0087_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0087,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0087_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0087_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0087_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0088 = {
    DataObjectModelType,
    "ValASG0088",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0089,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0088_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0088_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0088,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0088_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0088_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0088_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0089 = {
    DataObjectModelType,
    "ValASG0089",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0090,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0089_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0089_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0089,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0089_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0089_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0089_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0090 = {
    DataObjectModelType,
    "ValASG0090",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0091,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0090_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0090_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0090,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0090_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0090_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0090_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0091 = {
    DataObjectModelType,
    "ValASG0091",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0092,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0091_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0091_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0091,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0091_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0091_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0091_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0092 = {
    DataObjectModelType,
    "ValASG0092",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0093,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0092_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0092_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0092,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0092_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0092_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0092_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0093 = {
    DataObjectModelType,
    "ValASG0093",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0094,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0093_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0093_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0093,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0093_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0093_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0093_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0094 = {
    DataObjectModelType,
    "ValASG0094",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0095,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0094_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0094_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0094,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0094_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0094_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0094_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0095 = {
    DataObjectModelType,
    "ValASG0095",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0096,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0095_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0095_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0095,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0095_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0095_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0095_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0096 = {
    DataObjectModelType,
    "ValASG0096",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0097,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0096_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0096_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0096,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0096_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0096_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0096_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0097 = {
    DataObjectModelType,
    "ValASG0097",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0098,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0097_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0097_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0097,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0097_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0097_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0097_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0098 = {
    DataObjectModelType,
    "ValASG0098",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0099,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0098_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0098_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0098,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0098_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0098_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0098_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0099 = {
    DataObjectModelType,
    "ValASG0099",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0100,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0099_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0099_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0099,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0099_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0099_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0099_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCH2_ValASG0100 = {
    DataObjectModelType,
    "ValASG0100",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0100_setMag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0100_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0100,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0100_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCH2_ValASG0100_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCH2_ValASG0100_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_CLS_SCHD_FSCC1 = {
    LogicalNodeModelType,
    "SCHD_FSCC1",
    (ModelNode*) &iedModel_CLS,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ActSchdRef,
};

DataObject iedModel_CLS_SCHD_FSCC1_ActSchdRef = {
    DataObjectModelType,
    "ActSchdRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ActSchdRef_srcRef,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCC1_ActSchdRef_srcRef = {
    DataAttributeModelType,
    "srcRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ActSchdRef,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCC1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_CtlEnt,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh_stVal,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCC1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCC1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCC1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCC1_CtlEnt = {
    DataObjectModelType,
    "CtlEnt",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_CtlEnt_setSrcRef,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCC1_CtlEnt_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_CtlEnt,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCC1_ValMV = {
    DataObjectModelType,
    "ValMV",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Schd01,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV_mag,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCC1_ValMV_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV_q,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCC1_ValMV_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCC1_ValMV_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_SCHD_FSCC1_ValMV_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_ValMV,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCC1_Schd01 = {
    DataObjectModelType,
    "Schd01",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Schd02,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Schd01_setSrcRef,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCC1_Schd01_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Schd01,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_SCHD_FSCC1_Schd02 = {
    DataObjectModelType,
    "Schd02",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1,
    NULL,
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Schd02_setSrcRef,
    0
};

DataAttribute iedModel_CLS_SCHD_FSCC1_Schd02_setSrcRef = {
    DataAttributeModelType,
    "setSrcRef",
    (ModelNode*) &iedModel_CLS_SCHD_FSCC1_Schd02,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_VISIBLE_STRING_129,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_CLS_ZELL_ZELL1 = {
    LogicalNodeModelType,
    "ZELL_ZELL1",
    (ModelNode*) &iedModel_CLS,
    NULL,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_LocAgent,
};

DataObject iedModel_CLS_ZELL_ZELL1_LocAgent = {
    DataObjectModelType,
    "LocAgent",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_LocAgent_setVal,
    0
};

DataAttribute iedModel_CLS_ZELL_ZELL1_LocAgent_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_LocAgent,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_ZELL_ZELL1_MinW = {
    DataObjectModelType,
    "MinW",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW_setMag,
    0
};

DataAttribute iedModel_CLS_ZELL_ZELL1_MinW_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW_units,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_ZELL_ZELL1_MinW_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_ZELL_ZELL1_MinW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW,
    NULL,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_ZELL_ZELL1_MinW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MinW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_CLS_ZELL_ZELL1_MaxW = {
    DataObjectModelType,
    "MaxW",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1,
    NULL,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW_setMag,
    0
};

DataAttribute iedModel_CLS_ZELL_ZELL1_MaxW_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW_units,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_ZELL_ZELL1_MaxW_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_ZELL_ZELL1_MaxW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW,
    NULL,
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_CLS_ZELL_ZELL1_MaxW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_CLS_ZELL_ZELL1_MaxW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_PV = {
    LogicalDeviceModelType,
    "PV",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_PV2,
    (ModelNode*) &iedModel_PV_LLN0
};

LogicalNode iedModel_PV_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_PV,
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_LLN0_Mod,
};

DataObject iedModel_PV_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_PV_LLN0,
    (ModelNode*) &iedModel_PV_LLN0_Beh,
    (ModelNode*) &iedModel_PV_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_PV_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV_LLN0_Mod,
    (ModelNode*) &iedModel_PV_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_LLN0_Mod,
    (ModelNode*) &iedModel_PV_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_LLN0_Mod,
    (ModelNode*) &iedModel_PV_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_PV_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_PV_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_PV_LLN0,
    (ModelNode*) &iedModel_PV_LLN0_Health,
    (ModelNode*) &iedModel_PV_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_PV_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV_LLN0_Beh,
    (ModelNode*) &iedModel_PV_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_LLN0_Beh,
    (ModelNode*) &iedModel_PV_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_PV_LLN0,
    (ModelNode*) &iedModel_PV_LLN0_NamPlt,
    (ModelNode*) &iedModel_PV_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_PV_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV_LLN0_Health,
    (ModelNode*) &iedModel_PV_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_LLN0_Health,
    (ModelNode*) &iedModel_PV_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_PV_LLN0,
    NULL,
    (ModelNode*) &iedModel_PV_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_PV_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_PV_LLN0_NamPlt,
    (ModelNode*) &iedModel_PV_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_PV_LLN0_NamPlt,
    (ModelNode*) &iedModel_PV_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_PV_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_PV_PV_ZINV1 = {
    LogicalNodeModelType,
    "PV_ZINV1",
    (ModelNode*) &iedModel_PV,
    (ModelNode*) &iedModel_PV_PV_MMXU1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh,
};

DataObject iedModel_PV_PV_ZINV1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg,
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh_stVal,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh,
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh,
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_ZINV1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_WRtg = {
    DataObjectModelType,
    "WRtg",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_ACTyp,
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg_setMag,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_WRtg_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg,
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg_units,
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_WRtg_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_WRtg_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg,
    NULL,
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_WRtg_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_ZINV1_WRtg_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_ACTyp = {
    DataObjectModelType,
    "ACTyp",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_ACTyp_setVal,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_ACTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_PV_PV_ZINV1_ACTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_PQVLimSet = {
    DataObjectModelType,
    "PQVLimSet",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_crvPts,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_crvPts = {
    DataAttributeModelType,
    "crvPts",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_maxPts,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_crvPts_xVal,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_crvPts_xVal = {
    DataAttributeModelType,
    "xVal",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_crvPts,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_crvPts_yVal,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_crvPts_yVal = {
    DataAttributeModelType,
    "yVal",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_crvPts,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_maxPts = {
    DataAttributeModelType,
    "maxPts",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_xD,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT16U,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_xD = {
    DataAttributeModelType,
    "xD",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_xUnits,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_xUnits = {
    DataAttributeModelType,
    "xUnits",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_yD,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_xUnits_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_xUnits_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_xUnits,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_yD = {
    DataAttributeModelType,
    "yD",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_yUnits,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_yUnits = {
    DataAttributeModelType,
    "yUnits",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet,
    NULL,
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_yUnits_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_PQVLimSet_yUnits_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_ZINV1_PQVLimSet_yUnits,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_OutWSet = {
    DataObjectModelType,
    "OutWSet",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet_setMag,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_OutWSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet_units,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutWSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutWSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet,
    NULL,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutWSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutWSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_OutVarSet = {
    DataObjectModelType,
    "OutVarSet",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutPFSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet_setMag,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_OutVarSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet_units,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutVarSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutVarSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet,
    NULL,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutVarSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutVarSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_OutPFSet = {
    DataObjectModelType,
    "OutPFSet",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutPFSet_setMag,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_OutPFSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutPFSet,
    NULL,
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutPFSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_OutPFSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_ZINV1_OutPFSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_ZINV1_GridModSt = {
    DataObjectModelType,
    "GridModSt",
    (ModelNode*) &iedModel_PV_PV_ZINV1,
    NULL,
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt_stVal,
    0
};

DataAttribute iedModel_PV_PV_ZINV1_GridModSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_GridModSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_GridModSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_ZINV1_GridModSt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_PV_PV_ZINV1_GridModSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_PV_PV_MMXU1 = {
    LogicalNodeModelType,
    "PV_MMXU1",
    (ModelNode*) &iedModel_PV,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh,
};

DataObject iedModel_PV_PV_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_PV_PV_MMXU1,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV_PV_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_PV_PV_MMXU1,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_q,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_PV_PV_MMXU1,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotVAr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_TotVAr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_MMXU1_TotVAr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_PV_PV_MMXU1,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_PV_PV_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV_PV_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_PV_PV_MMXU1,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_PV_PV_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_q,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV_PV_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV_PV_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_PV2 = {
    LogicalDeviceModelType,
    "PV2",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_BAT,
    (ModelNode*) &iedModel_PV2_LLN0
};

LogicalNode iedModel_PV2_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_PV2,
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_LLN0_Mod,
};

DataObject iedModel_PV2_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_PV2_LLN0,
    (ModelNode*) &iedModel_PV2_LLN0_Beh,
    (ModelNode*) &iedModel_PV2_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_PV2_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV2_LLN0_Mod,
    (ModelNode*) &iedModel_PV2_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_LLN0_Mod,
    (ModelNode*) &iedModel_PV2_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_LLN0_Mod,
    (ModelNode*) &iedModel_PV2_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_PV2_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_PV2_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_PV2_LLN0,
    (ModelNode*) &iedModel_PV2_LLN0_Health,
    (ModelNode*) &iedModel_PV2_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_PV2_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV2_LLN0_Beh,
    (ModelNode*) &iedModel_PV2_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_LLN0_Beh,
    (ModelNode*) &iedModel_PV2_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV2_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_PV2_LLN0,
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt,
    (ModelNode*) &iedModel_PV2_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_PV2_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV2_LLN0_Health,
    (ModelNode*) &iedModel_PV2_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_LLN0_Health,
    (ModelNode*) &iedModel_PV2_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV2_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_PV2_LLN0,
    NULL,
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_PV2_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt,
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt,
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_PV2_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_PV2_PV_ZINV1 = {
    LogicalNodeModelType,
    "PV_ZINV1",
    (ModelNode*) &iedModel_PV2,
    (ModelNode*) &iedModel_PV2_PV_MMXU1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh,
};

DataObject iedModel_PV2_PV_ZINV1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh_stVal,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_WRtg = {
    DataObjectModelType,
    "WRtg",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_ACTyp,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg_setMag,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_WRtg_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg_units,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_WRtg_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_WRtg_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_WRtg_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_WRtg_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_ACTyp = {
    DataObjectModelType,
    "ACTyp",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_ACTyp_setVal,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_ACTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_ACTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_PQVLimSet = {
    DataObjectModelType,
    "PQVLimSet",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts = {
    DataAttributeModelType,
    "crvPts",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_maxPts,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts_xVal,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts_xVal = {
    DataAttributeModelType,
    "xVal",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts_yVal,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts_yVal = {
    DataAttributeModelType,
    "yVal",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_crvPts,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_maxPts = {
    DataAttributeModelType,
    "maxPts",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_xD,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT16U,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_xD = {
    DataAttributeModelType,
    "xD",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_xUnits,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_xUnits = {
    DataAttributeModelType,
    "xUnits",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_yD,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_xUnits_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_xUnits_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_xUnits,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_yD = {
    DataAttributeModelType,
    "yD",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_yUnits,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_yUnits = {
    DataAttributeModelType,
    "yUnits",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_yUnits_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_PQVLimSet_yUnits_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_PQVLimSet_yUnits,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_OutWSet = {
    DataObjectModelType,
    "OutWSet",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet_setMag,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_OutWSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet_units,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutWSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutWSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutWSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutWSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_OutVarSet = {
    DataObjectModelType,
    "OutVarSet",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutPFSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet_setMag,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_OutVarSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet_units,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutVarSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutVarSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutVarSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutVarSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_OutPFSet = {
    DataObjectModelType,
    "OutPFSet",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutPFSet_setMag,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_OutPFSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutPFSet,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutPFSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_OutPFSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_OutPFSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_ZINV1_GridModSt = {
    DataObjectModelType,
    "GridModSt",
    (ModelNode*) &iedModel_PV2_PV_ZINV1,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt_stVal,
    0
};

DataAttribute iedModel_PV2_PV_ZINV1_GridModSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_GridModSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_GridModSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt,
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_ZINV1_GridModSt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_PV2_PV_ZINV1_GridModSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_PV2_PV_MMXU1 = {
    LogicalNodeModelType,
    "PV_MMXU1",
    (ModelNode*) &iedModel_PV2,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh,
};

DataObject iedModel_PV2_PV_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_PV2_PV_MMXU1,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_PV2_PV_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_PV2_PV_MMXU1,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_q,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_PV2_PV_MMXU1,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotVAr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_TotVAr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_TotVAr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_PV2_PV_MMXU1,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_PV2_PV_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_PV2_PV_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_PV2_PV_MMXU1,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_PV2_PV_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_q,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_PV2_PV_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_PV2_PV_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_BAT = {
    LogicalDeviceModelType,
    "BAT",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_HEAT,
    (ModelNode*) &iedModel_BAT_LLN0
};

LogicalNode iedModel_BAT_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_BAT,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_LLN0_Mod,
};

DataObject iedModel_BAT_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_BAT_LLN0,
    (ModelNode*) &iedModel_BAT_LLN0_Beh,
    (ModelNode*) &iedModel_BAT_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_BAT_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_LLN0_Mod,
    (ModelNode*) &iedModel_BAT_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_LLN0_Mod,
    (ModelNode*) &iedModel_BAT_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_LLN0_Mod,
    (ModelNode*) &iedModel_BAT_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_BAT_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_BAT_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_BAT_LLN0,
    (ModelNode*) &iedModel_BAT_LLN0_Health,
    (ModelNode*) &iedModel_BAT_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_BAT_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_LLN0_Beh,
    (ModelNode*) &iedModel_BAT_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_LLN0_Beh,
    (ModelNode*) &iedModel_BAT_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_BAT_LLN0,
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt,
    (ModelNode*) &iedModel_BAT_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_BAT_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_LLN0_Health,
    (ModelNode*) &iedModel_BAT_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_LLN0_Health,
    (ModelNode*) &iedModel_BAT_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_BAT_LLN0,
    NULL,
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_BAT_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt,
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt,
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_BAT_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_BAT_BAT_DSTO1 = {
    LogicalNodeModelType,
    "BAT_DSTO1",
    (ModelNode*) &iedModel_BAT,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh,
};

DataObject iedModel_BAT_BAT_DSTO1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_TotEnNom = {
    DataObjectModelType,
    "TotEnNom",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnNom_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom_units,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnNom_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnNom_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnNom_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnNom_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_TotEnAbs = {
    DataObjectModelType,
    "TotEnAbs",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnAbs_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs_units,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnAbs_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnAbs_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnAbs_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnAbs_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_SoCAhr = {
    DataObjectModelType,
    "SoCAhr",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCAhr_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCAhr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCAhr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCAhr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCAhr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCAhr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_TotEnCh = {
    DataObjectModelType,
    "TotEnCh",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh_mag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnCh_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh_q,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnCh_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnCh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_TotEnCh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_TotEnCh,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_OutWh = {
    DataObjectModelType,
    "OutWh",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_mag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_OutWh_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_q,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OutWh_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OutWh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OutWh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OutWh_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OutWh_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OutWh_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_ChaCyclCnt = {
    DataObjectModelType,
    "ChaCyclCnt",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_ChaCyclCnt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_ChaCyclCnt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_ChaCyclCnt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_ChaCyclCnt,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_OpTmh = {
    DataObjectModelType,
    "OpTmh",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_OpTmh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OpTmh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OpTmh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OpTmh_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_OpTmh_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpTmh_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_SoCHiAls = {
    DataObjectModelType,
    "SoCHiAls",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCHiAls_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls_units,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCHiAls_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCHiAls_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCHiAls_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCHiAls_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_SoCLoAls = {
    DataObjectModelType,
    "SoCLoAls",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpMod,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCLoAls_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls_units,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCLoAls_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCLoAls_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_SoCLoAls_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_SoCLoAls_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_OpMod = {
    DataObjectModelType,
    "OpMod",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpMod_setVal,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_OpMod_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_OpMod,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DSTO1_EnOut = {
    DataObjectModelType,
    "EnOut",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_mag,
    0
};

DataAttribute iedModel_BAT_BAT_DSTO1_EnOut_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_q,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_EnOut_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_EnOut_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_EnOut_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_EnOut_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DSTO1_EnOut_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DSTO1_EnOut_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_BAT_BAT_ZINV1 = {
    LogicalNodeModelType,
    "BAT_ZINV1",
    (ModelNode*) &iedModel_BAT,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh,
};

DataObject iedModel_BAT_BAT_ZINV1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_WRtg,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_ZINV1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_ZINV1_WRtg = {
    DataObjectModelType,
    "WRtg",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_ACTyp,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_WRtg_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_ZINV1_WRtg_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_WRtg,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_WRtg_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_WRtg_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_WRtg_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_ZINV1_ACTyp = {
    DataObjectModelType,
    "ACTyp",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_ACTyp_setVal,
    0
};

DataAttribute iedModel_BAT_BAT_ZINV1_ACTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_ACTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_ZINV1_PQVLimSet = {
    DataObjectModelType,
    "PQVLimSet",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_maxPts,
    0
};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_maxPts = {
    DataAttributeModelType,
    "maxPts",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_xD,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_INT16U,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_xD = {
    DataAttributeModelType,
    "xD",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_xUnits,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_xUnits = {
    DataAttributeModelType,
    "xUnits",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_yD,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_xUnits_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_xUnits_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_xUnits,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_yD = {
    DataAttributeModelType,
    "yD",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_yUnits,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_yUnits = {
    DataAttributeModelType,
    "yUnits",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_yUnits_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_PQVLimSet_yUnits_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_PQVLimSet_yUnits,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_ZINV1_OutWSet = {
    DataObjectModelType,
    "OutWSet",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_ZINV1_OutWSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet_units,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_OutWSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_OutWSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_OutWSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutWSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_ZINV1_OutVarSet = {
    DataObjectModelType,
    "OutVarSet",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_ZINV1_OutVarSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet_units,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_OutVarSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_OutVarSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_ZINV1_OutVarSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_ZINV1_OutVarSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_BAT_BAT_DBAT1 = {
    LogicalNodeModelType,
    "BAT_DBAT1",
    (ModelNode*) &iedModel_BAT,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatTyp,
};

DataObject iedModel_BAT_BAT_DBAT1_BatTyp = {
    DataObjectModelType,
    "BatTyp",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatTyp_setVal,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_BatTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DBAT1_BatSt = {
    DataObjectModelType,
    "BatSt",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_BatSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_BatSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_BatSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DBAT1_Vol = {
    DataObjectModelType,
    "Vol",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_mag,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_Vol_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_q,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Vol_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Vol_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Vol_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Vol_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Vol_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Vol_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DBAT1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatVNom,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DBAT1_BatVNom = {
    DataObjectModelType,
    "BatVNom",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatA,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatVNom_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_BatVNom_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatVNom,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatVNom_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_BatVNom_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_BatVNom_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DBAT1_MaxBatA = {
    DataObjectModelType,
    "MaxBatA",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatV,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatA_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_MaxBatA_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatA,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatA_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_MaxBatA_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatA_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_DBAT1_MaxBatV = {
    DataObjectModelType,
    "MaxBatV",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatV_setMag,
    0
};

DataAttribute iedModel_BAT_BAT_DBAT1_MaxBatV_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatV,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatV_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_DBAT1_MaxBatV_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_DBAT1_MaxBatV_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_BAT_BAT_MMXU1 = {
    LogicalNodeModelType,
    "BAT_MMXU1",
    (ModelNode*) &iedModel_BAT,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh,
};

DataObject iedModel_BAT_BAT_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVAr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVAr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVAr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_TotVA = {
    DataObjectModelType,
    "TotVA",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_mag,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVA_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVA_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_TotVA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_TotVA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_BAT_BAT_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_BAT_BAT_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_BAT_BAT_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_BAT_BAT_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_HEAT = {
    LogicalDeviceModelType,
    "HEAT",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_HEAT_LLN0
};

LogicalNode iedModel_HEAT_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_HEAT,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1,
    (ModelNode*) &iedModel_HEAT_LLN0_Mod,
};

DataObject iedModel_HEAT_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_HEAT_LLN0,
    (ModelNode*) &iedModel_HEAT_LLN0_Beh,
    (ModelNode*) &iedModel_HEAT_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_HEAT_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_LLN0_Mod,
    (ModelNode*) &iedModel_HEAT_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_LLN0_Mod,
    (ModelNode*) &iedModel_HEAT_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_LLN0_Mod,
    (ModelNode*) &iedModel_HEAT_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_HEAT_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_HEAT_LLN0,
    (ModelNode*) &iedModel_HEAT_LLN0_Health,
    (ModelNode*) &iedModel_HEAT_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_HEAT_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_LLN0_Beh,
    (ModelNode*) &iedModel_HEAT_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_LLN0_Beh,
    (ModelNode*) &iedModel_HEAT_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_HEAT_LLN0,
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt,
    (ModelNode*) &iedModel_HEAT_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_HEAT_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_LLN0_Health,
    (ModelNode*) &iedModel_HEAT_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_LLN0_Health,
    (ModelNode*) &iedModel_HEAT_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_HEAT_LLN0,
    NULL,
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_HEAT_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt,
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt,
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_HEAT_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_HEAT_HEAT_DCTS1 = {
    LogicalNodeModelType,
    "HEAT_DCTS1",
    (ModelNode*) &iedModel_HEAT,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh,
};

DataObject iedModel_HEAT_HEAT_DCTS1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmStoTyp,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh_stVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_DCTS1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_DCTS1_ThrmStoTyp = {
    DataObjectModelType,
    "ThrmStoTyp",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmStoTyp_setVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmStoTyp_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmStoTyp,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_DCTS1_ThrmCapTot = {
    DataObjectModelType,
    "ThrmCapTot",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_mag,
    0
};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_q,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_DCTS1_ThrmCapPct = {
    DataObjectModelType,
    "ThrmCapPct",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_mag,
    0
};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_q,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_ThrmCapPct_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_ThrmCapPct,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_DCTS1_OpTmh = {
    DataObjectModelType,
    "OpTmh",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh_stVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_DCTS1_OpTmh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_OpTmh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_OpTmh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_OpTmh_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_DCTS1_OpTmh_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_DCTS1_OpTmh_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_HEAT_HEAT_STMP1 = {
    LogicalNodeModelType,
    "HEAT_STMP1",
    (ModelNode*) &iedModel_HEAT,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh,
};

DataObject iedModel_HEAT_HEAT_STMP1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh_stVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_STMP1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_STMP1_Tmp = {
    DataObjectModelType,
    "Tmp",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_mag,
    0
};

DataAttribute iedModel_HEAT_HEAT_STMP1_Tmp_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_q,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Tmp_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Tmp_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Tmp_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Tmp_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_Tmp_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_Tmp_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_STMP1_MaxTmp = {
    DataObjectModelType,
    "MaxTmp",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp_setMag,
    0
};

DataAttribute iedModel_HEAT_HEAT_STMP1_MaxTmp_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp_units,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_MaxTmp_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_MaxTmp_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_MaxTmp_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MaxTmp_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_STMP1_MinTmp = {
    DataObjectModelType,
    "MinTmp",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp_setMag,
    0
};

DataAttribute iedModel_HEAT_HEAT_STMP1_MinTmp_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp_units,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_MinTmp_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_MinTmp_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_STMP1_MinTmp_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_STMP1_MinTmp_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_HEAT_HEAT_ZCTS1 = {
    LogicalNodeModelType,
    "HEAT_ZCTS1",
    (ModelNode*) &iedModel_HEAT,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg,
};

DataObject iedModel_HEAT_HEAT_ZCTS1_TmpRtg = {
    DataObjectModelType,
    "TmpRtg",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg_setMag,
    0
};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpRtg_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg_units,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpRtg_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpRtg_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpRtg_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpRtg_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_ZCTS1_TmpSet = {
    DataObjectModelType,
    "TmpSet",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OpMod,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet_setMag,
    0
};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet_units,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_TmpSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_TmpSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_ZCTS1_OpMod = {
    DataObjectModelType,
    "OpMod",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OpMod_setVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_OpMod_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OpMod,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_ZCTS1_OutWSet = {
    DataObjectModelType,
    "OutWSet",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet_setMag,
    0
};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_OutWSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet_units,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_OutWSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_OutWSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_OutWSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_OutWSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_ZCTS1_InWSet = {
    DataObjectModelType,
    "InWSet",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet_setMag,
    0
};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_InWSet_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet_units,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_InWSet_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_InWSet_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_ZCTS1_InWSet_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_ZCTS1_InWSet_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_HEAT_HEAT_MMXU1 = {
    LogicalNodeModelType,
    "HEAT_MMXU1",
    (ModelNode*) &iedModel_HEAT,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh,
};

DataObject iedModel_HEAT_HEAT_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_TotVAr,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_HEAT_HEAT_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_A = {
    DataObjectModelType,
    "A",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA,
    0
};

DataObject iedModel_HEAT_HEAT_MMXU1_A_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_A_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_HEAT_HEAT_MMXU1_A_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal,
    0
};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_q,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC,
    NULL,
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_HEAT_HEAT_MMXU1_A_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_HEAT_HEAT_MMXU1_A_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_EMOB = {
    LogicalDeviceModelType,
    "EMOB",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_MEAS,
    (ModelNode*) &iedModel_EMOB_LLN0
};

LogicalNode iedModel_EMOB_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    (ModelNode*) &iedModel_EMOB_LLN0_Mod,
};

DataObject iedModel_EMOB_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_EMOB_LLN0,
    (ModelNode*) &iedModel_EMOB_LLN0_Beh,
    (ModelNode*) &iedModel_EMOB_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_EMOB_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_LLN0_Mod,
    (ModelNode*) &iedModel_EMOB_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_LLN0_Mod,
    (ModelNode*) &iedModel_EMOB_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_LLN0_Mod,
    (ModelNode*) &iedModel_EMOB_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_EMOB_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_LLN0,
    (ModelNode*) &iedModel_EMOB_LLN0_Health,
    (ModelNode*) &iedModel_EMOB_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_LLN0_Beh,
    (ModelNode*) &iedModel_EMOB_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_LLN0_Beh,
    (ModelNode*) &iedModel_EMOB_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_EMOB_LLN0,
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt,
    (ModelNode*) &iedModel_EMOB_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_EMOB_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_LLN0_Health,
    (ModelNode*) &iedModel_EMOB_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_LLN0_Health,
    (ModelNode*) &iedModel_EMOB_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_EMOB_LLN0,
    NULL,
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_EMOB_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt,
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt,
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_EMOB_EMOB_DESE1 = {
    LogicalNodeModelType,
    "EMOB_DESE1",
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
};

DataObject iedModel_EMOB_EMOB_DESE1_EVSENam = {
    DataObjectModelType,
    "EVSENam",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam_vendor,
    0
};

DataAttribute iedModel_EMOB_EMOB_DESE1_EVSENam_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam_hwRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_EVSENam_hwRev = {
    DataAttributeModelType,
    "hwRev",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_EVSENam_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam_serNum,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_EVSENam_serNum = {
    DataAttributeModelType,
    "serNum",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam_model,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_EVSENam_model = {
    DataAttributeModelType,
    "model",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam_primeOper,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_EVSENam_primeOper = {
    DataAttributeModelType,
    "primeOper",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_EVSENam,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DESE1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrRtg,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DESE1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DESE1_ChaPwrRtg = {
    DataObjectModelType,
    "ChaPwrRtg",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrLim,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrRtg_setMag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaPwrRtg_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrRtg,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrRtg_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaPwrRtg_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrRtg_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DESE1_ChaPwrLim = {
    DataObjectModelType,
    "ChaPwrLim",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrLim_setMag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaPwrLim_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrLim,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrLim_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaPwrLim_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaPwrLim_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DESE1_ChaV = {
    DataObjectModelType,
    "ChaV",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaV_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV_q,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaV_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaV_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaV_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaV,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DESE1_ChaA = {
    DataObjectModelType,
    "ChaA",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaA_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA_q,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaA_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA,
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DESE1_ChaA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DESE1_ChaA,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

LogicalNode iedModel_EMOB_EMOB_DEAO1 = {
    LogicalNodeModelType,
    "EMOB_DEAO1",
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt,
};

DataObject iedModel_EMOB_EMOB_DEAO1_ConnSt = {
    DataObjectModelType,
    "ConnSt",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ConnSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ConnSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ConnSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ConnSt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_PlgStAC = {
    DataObjectModelType,
    "PlgStAC",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_PlgStAC_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_PlgStAC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_PlgStAC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_PlgStAC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_PlgStAC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_CabRtgAC = {
    DataObjectModelType,
    "CabRtgAC",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_CabRtgAC_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_CabRtgAC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_CabRtgAC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_CabRtgAC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_CabRtgAC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_Blk = {
    DataObjectModelType,
    "Blk",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Blk_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Blk_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Blk_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Blk,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnTypDC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_ConnTypDC = {
    DataObjectModelType,
    "ConnTypDC",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnTypPhs,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnTypDC_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ConnTypDC_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnTypDC,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_ConnTypPhs = {
    DataObjectModelType,
    "ConnTypPhs",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaARtg,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnTypPhs_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ConnTypPhs_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ConnTypPhs,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_ChaARtg = {
    DataObjectModelType,
    "ChaARtg",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaAMax,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaARtg_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaARtg_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaARtg,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_ChaAMax = {
    DataObjectModelType,
    "ChaAMax",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaAMax_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaAMax_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaAMax,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_ChaTm = {
    DataObjectModelType,
    "ChaTm",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaTm_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaTm_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaTm_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaTm_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaTm_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaTm_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO1_ChaEn = {
    DataObjectModelType,
    "ChaEn",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaEn_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaEn_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaEn_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaEn_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO1_ChaEn_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO1_ChaEn_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_EMOB_EMOB_DEEV1 = {
    LogicalNodeModelType,
    "EMOB_DEEV1",
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EVNam,
};

DataObject iedModel_EMOB_EMOB_DEEV1_EVNam = {
    DataObjectModelType,
    "EVNam",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EVNam_vendor,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV1_EVNam_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EVNam,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV1_ConnTypSel = {
    DataObjectModelType,
    "ConnTypSel",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV1_ConnTypSel_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_ConnTypSel_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_ConnTypSel_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_ConnTypSel_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_ConnTypSel,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV1_Soc = {
    DataObjectModelType,
    "Soc",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Soc_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc_q,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Soc_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Soc_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_Soc_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_Soc,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV1_DptTm = {
    DataObjectModelType,
    "DptTm",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EnAmnt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal = {
    DataAttributeModelType,
    "setCal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_occ,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_occ = {
    DataAttributeModelType,
    "occ",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_occType,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT16U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_occType = {
    DataAttributeModelType,
    "occType",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_occPer,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_occPer = {
    DataAttributeModelType,
    "occPer",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_weekDay,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_weekDay = {
    DataAttributeModelType,
    "weekDay",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_month,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_month = {
    DataAttributeModelType,
    "month",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_day,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_day = {
    DataAttributeModelType,
    "day",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_hr,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_hr = {
    DataAttributeModelType,
    "hr",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_mn,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_mn = {
    DataAttributeModelType,
    "mn",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_DptTm_setCal,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV1_EnAmnt = {
    DataObjectModelType,
    "EnAmnt",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EnAmnt_setMag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV1_EnAmnt_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EnAmnt,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EnAmnt_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV1_EnAmnt_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV1_EnAmnt_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_EMOB_EMOB_MMXU1 = {
    LogicalNodeModelType,
    "EMOB_MMXU1",
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh,
};

DataObject iedModel_EMOB_EMOB_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_TotVAr,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_EMOB_EMOB_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_A = {
    DataObjectModelType,
    "A",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA,
    0
};

DataObject iedModel_EMOB_EMOB_MMXU1_A_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_A_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_MMXU1_A_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_q,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_MMXU1_A_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_MMXU1_A_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_EMOB_EMOB_DEAO2 = {
    LogicalNodeModelType,
    "EMOB_DEAO2",
    (ModelNode*) &iedModel_EMOB,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt,
};

DataObject iedModel_EMOB_EMOB_DEAO2_ConnSt = {
    DataObjectModelType,
    "ConnSt",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ConnSt_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ConnSt_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ConnSt_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ConnSt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnSt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_PlgStAC = {
    DataObjectModelType,
    "PlgStAC",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_PlgStAC_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_PlgStAC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_PlgStAC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_PlgStAC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_PlgStAC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_CabRtgAC = {
    DataObjectModelType,
    "CabRtgAC",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_CabRtgAC_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_CabRtgAC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_CabRtgAC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_CabRtgAC_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_CabRtgAC,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_Blk = {
    DataObjectModelType,
    "Blk",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Blk_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Blk_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Blk_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Blk,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnTypDC,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_Health_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_ConnTypDC = {
    DataObjectModelType,
    "ConnTypDC",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnTypPhs,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnTypDC_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ConnTypDC_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnTypDC,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_ConnTypPhs = {
    DataObjectModelType,
    "ConnTypPhs",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaARtg,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnTypPhs_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ConnTypPhs_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ConnTypPhs,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_BOOLEAN,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_ChaARtg = {
    DataObjectModelType,
    "ChaARtg",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaAMax,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaARtg_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaARtg_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaARtg,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_ChaAMax = {
    DataObjectModelType,
    "ChaAMax",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaAMax_setVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaAMax_setVal = {
    DataAttributeModelType,
    "setVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaAMax,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_ChaTm = {
    DataObjectModelType,
    "ChaTm",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaTm_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaTm_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaTm_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaTm_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaTm_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaTm_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEAO2_ChaEn = {
    DataObjectModelType,
    "ChaEn",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaEn_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_INT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaEn_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaEn_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn_units,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaEn_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEAO2_ChaEn_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_EMOB_EMOB_DEAO2_ChaEn_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

LogicalNode iedModel_EMOB_EMOB_DEEV2 = {
    LogicalNodeModelType,
    "EMOB_DEEV2",
    (ModelNode*) &iedModel_EMOB,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EVNam,
};

DataObject iedModel_EMOB_EMOB_DEEV2_EVNam = {
    DataObjectModelType,
    "EVNam",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EVNam_vendor,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV2_EVNam_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EVNam,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV2_ConnTypSel = {
    DataObjectModelType,
    "ConnTypSel",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV2_ConnTypSel_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_ConnTypSel_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_ConnTypSel_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel_d,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_ConnTypSel_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_ConnTypSel,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV2_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh_stVal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV2_Soc = {
    DataObjectModelType,
    "Soc",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc_mag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Soc_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc_q,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Soc_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Soc_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_Soc_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_Soc,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV2_DptTm = {
    DataObjectModelType,
    "DptTm",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EnAmnt,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal = {
    DataAttributeModelType,
    "setCal",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_occ,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_occ = {
    DataAttributeModelType,
    "occ",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_occType,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT16U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_occType = {
    DataAttributeModelType,
    "occType",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_occPer,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_occPer = {
    DataAttributeModelType,
    "occPer",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_weekDay,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_weekDay = {
    DataAttributeModelType,
    "weekDay",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_month,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_month = {
    DataAttributeModelType,
    "month",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_day,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_day = {
    DataAttributeModelType,
    "day",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_hr,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_hr = {
    DataAttributeModelType,
    "hr",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_mn,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_mn = {
    DataAttributeModelType,
    "mn",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_DptTm_setCal,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_INT8U,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_EMOB_EMOB_DEEV2_EnAmnt = {
    DataObjectModelType,
    "EnAmnt",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EnAmnt_setMag,
    0
};

DataAttribute iedModel_EMOB_EMOB_DEEV2_EnAmnt_setMag = {
    DataAttributeModelType,
    "setMag",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EnAmnt,
    NULL,
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EnAmnt_setMag_f,
    0,
    IEC61850_FC_SP,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_EMOB_EMOB_DEEV2_EnAmnt_setMag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_EMOB_EMOB_DEEV2_EnAmnt_setMag,
    NULL,
    NULL,
    0,
    IEC61850_FC_SP,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_MEAS = {
    LogicalDeviceModelType,
    "MEAS",
    (ModelNode*) &iedModel,
    (ModelNode*) &iedModel_MEAS2,
    (ModelNode*) &iedModel_MEAS_LLN0
};

LogicalNode iedModel_MEAS_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_MEAS,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS_LLN0_Mod,
};

DataObject iedModel_MEAS_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_MEAS_LLN0,
    (ModelNode*) &iedModel_MEAS_LLN0_Beh,
    (ModelNode*) &iedModel_MEAS_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_MEAS_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS_LLN0_Mod,
    (ModelNode*) &iedModel_MEAS_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_LLN0_Mod,
    (ModelNode*) &iedModel_MEAS_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_LLN0_Mod,
    (ModelNode*) &iedModel_MEAS_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_MEAS_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_MEAS_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_MEAS_LLN0,
    (ModelNode*) &iedModel_MEAS_LLN0_Health,
    (ModelNode*) &iedModel_MEAS_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_MEAS_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS_LLN0_Beh,
    (ModelNode*) &iedModel_MEAS_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_LLN0_Beh,
    (ModelNode*) &iedModel_MEAS_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_MEAS_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_MEAS_LLN0,
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt,
    (ModelNode*) &iedModel_MEAS_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_MEAS_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS_LLN0_Health,
    (ModelNode*) &iedModel_MEAS_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_LLN0_Health,
    (ModelNode*) &iedModel_MEAS_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_MEAS_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_MEAS_LLN0,
    NULL,
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_MEAS_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt,
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt,
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_MEAS_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_MEAS_MEAS_MMXU1 = {
    LogicalNodeModelType,
    "MEAS_MMXU1",
    (ModelNode*) &iedModel_MEAS,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh,
};

DataObject iedModel_MEAS_MEAS_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_MEAS_MEAS_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_q,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS_MEAS_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotVAr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_TotVAr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_TotVAr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS_MEAS_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_MEAS_MEAS_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS_MEAS_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS_MEAS_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS_MEAS_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_q,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS_MEAS_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS_MEAS_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};


LogicalDevice iedModel_MEAS2 = {
    LogicalDeviceModelType,
    "MEAS2",
    (ModelNode*) &iedModel,
    NULL,
    (ModelNode*) &iedModel_MEAS2_LLN0
};

LogicalNode iedModel_MEAS2_LLN0 = {
    LogicalNodeModelType,
    "LLN0",
    (ModelNode*) &iedModel_MEAS2,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod,
};

DataObject iedModel_MEAS2_LLN0_Mod = {
    DataObjectModelType,
    "Mod",
    (ModelNode*) &iedModel_MEAS2_LLN0,
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh,
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod_stVal,
    0
};

DataAttribute iedModel_MEAS2_LLN0_Mod_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod,
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Mod_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod,
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Mod_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod,
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod_ctlModel,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Mod_ctlModel = {
    DataAttributeModelType,
    "ctlModel",
    (ModelNode*) &iedModel_MEAS2_LLN0_Mod,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0,
    NULL,
    0};

DataObject iedModel_MEAS2_LLN0_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_MEAS2_LLN0,
    (ModelNode*) &iedModel_MEAS2_LLN0_Health,
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh_stVal,
    0
};

DataAttribute iedModel_MEAS2_LLN0_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh,
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh,
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_LLN0_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_MEAS2_LLN0_Health = {
    DataObjectModelType,
    "Health",
    (ModelNode*) &iedModel_MEAS2_LLN0,
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt,
    (ModelNode*) &iedModel_MEAS2_LLN0_Health_stVal,
    0
};

DataAttribute iedModel_MEAS2_LLN0_Health_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS2_LLN0_Health,
    (ModelNode*) &iedModel_MEAS2_LLN0_Health_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Health_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_LLN0_Health,
    (ModelNode*) &iedModel_MEAS2_LLN0_Health_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_Health_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_LLN0_Health,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_MEAS2_LLN0_NamPlt = {
    DataObjectModelType,
    "NamPlt",
    (ModelNode*) &iedModel_MEAS2_LLN0,
    NULL,
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt_vendor,
    0
};

DataAttribute iedModel_MEAS2_LLN0_NamPlt_vendor = {
    DataAttributeModelType,
    "vendor",
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt,
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt_swRev,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_NamPlt_swRev = {
    DataAttributeModelType,
    "swRev",
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt,
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt_d,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_LLN0_NamPlt_d = {
    DataAttributeModelType,
    "d",
    (ModelNode*) &iedModel_MEAS2_LLN0_NamPlt,
    NULL,
    NULL,
    0,
    IEC61850_FC_DC,
    IEC61850_VISIBLE_STRING_255,
    0,
    NULL,
    0};

LogicalNode iedModel_MEAS2_MEAS_MMXU1 = {
    LogicalNodeModelType,
    "MEAS_MMXU1",
    (ModelNode*) &iedModel_MEAS2,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh,
};

DataObject iedModel_MEAS2_MEAS_MMXU1_Beh = {
    DataObjectModelType,
    "Beh",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh_stVal,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Beh_stVal = {
    DataAttributeModelType,
    "stVal",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh_q,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Beh_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh_t,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Beh_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Beh,
    NULL,
    NULL,
    0,
    IEC61850_FC_ST,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataObject iedModel_MEAS2_MEAS_MMXU1_TotW = {
    DataObjectModelType,
    "TotW",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_mag,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotW_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_q,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotW_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotW_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotW_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotW_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotW_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotW_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS2_MEAS_MMXU1_TotVAr = {
    DataObjectModelType,
    "TotVAr",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_mag,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotVAr_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_q,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotVAr_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotVAr_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotVAr_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotVAr_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_TotVAr_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_TotVAr_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS2_MEAS_MMXU1_PhV = {
    DataObjectModelType,
    "PhV",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA,
    0
};

DataObject iedModel_MEAS2_MEAS_MMXU1_PhV_phsA = {
    DataObjectModelType,
    "phsA",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_q,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS2_MEAS_MMXU1_PhV_phsB = {
    DataObjectModelType,
    "phsB",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_q,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS2_MEAS_MMXU1_PhV_phsC = {
    DataObjectModelType,
    "phsC",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal = {
    DataAttributeModelType,
    "cVal",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_q,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal_mag,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_cVal_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataObject iedModel_MEAS2_MEAS_MMXU1_Hz = {
    DataObjectModelType,
    "Hz",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_mag,
    0
};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Hz_mag = {
    DataAttributeModelType,
    "mag",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_q,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_mag_f,
    0,
    IEC61850_FC_MX,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Hz_mag_f = {
    DataAttributeModelType,
    "f",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_mag,
    NULL,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_FLOAT32,
    0 + TRG_OPT_DATA_CHANGED + TRG_OPT_DATA_UPDATE,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Hz_q = {
    DataAttributeModelType,
    "q",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_t,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_QUALITY,
    0 + TRG_OPT_QUALITY_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Hz_t = {
    DataAttributeModelType,
    "t",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_units,
    NULL,
    0,
    IEC61850_FC_MX,
    IEC61850_TIMESTAMP,
    0,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Hz_units = {
    DataAttributeModelType,
    "units",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz,
    NULL,
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_units_SIUnit,
    0,
    IEC61850_FC_CF,
    IEC61850_CONSTRUCTED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

DataAttribute iedModel_MEAS2_MEAS_MMXU1_Hz_units_SIUnit = {
    DataAttributeModelType,
    "SIUnit",
    (ModelNode*) &iedModel_MEAS2_MEAS_MMXU1_Hz_units,
    NULL,
    NULL,
    0,
    IEC61850_FC_CF,
    IEC61850_ENUMERATED,
    0 + TRG_OPT_DATA_CHANGED,
    NULL,
    0};

extern ReportControlBlock iedModel_CLS_LLN0_report0;
extern ReportControlBlock iedModel_CLS_LLN0_report1;
extern ReportControlBlock iedModel_CLS_LLN0_report2;
extern ReportControlBlock iedModel_CLS_LLN0_report3;
extern ReportControlBlock iedModel_CLS_LLN0_report4;
extern ReportControlBlock iedModel_CLS_LLN0_report5;
extern ReportControlBlock iedModel_CLS_LLN0_report6;
extern ReportControlBlock iedModel_PV_LLN0_report0;
extern ReportControlBlock iedModel_PV_LLN0_report1;
extern ReportControlBlock iedModel_PV_LLN0_report2;
extern ReportControlBlock iedModel_PV2_LLN0_report0;
extern ReportControlBlock iedModel_PV2_LLN0_report1;
extern ReportControlBlock iedModel_PV2_LLN0_report2;
extern ReportControlBlock iedModel_BAT_LLN0_report0;
extern ReportControlBlock iedModel_BAT_LLN0_report1;
extern ReportControlBlock iedModel_BAT_LLN0_report2;
extern ReportControlBlock iedModel_HEAT_LLN0_report0;
extern ReportControlBlock iedModel_HEAT_LLN0_report1;
extern ReportControlBlock iedModel_HEAT_LLN0_report2;
extern ReportControlBlock iedModel_EMOB_LLN0_report0;
extern ReportControlBlock iedModel_EMOB_LLN0_report1;
extern ReportControlBlock iedModel_EMOB_LLN0_report2;
extern ReportControlBlock iedModel_MEAS_LLN0_report0;
extern ReportControlBlock iedModel_MEAS_LLN0_report1;
extern ReportControlBlock iedModel_MEAS_LLN0_report2;
extern ReportControlBlock iedModel_MEAS2_LLN0_report0;
extern ReportControlBlock iedModel_MEAS2_LLN0_report1;
extern ReportControlBlock iedModel_MEAS2_LLN0_report2;

ReportControlBlock iedModel_CLS_LLN0_report0 = {&iedModel_CLS_LLN0, "CLS_Stammdaten01", "RC_clsPlate", false, "CLS_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_CLS_LLN0_report1};
ReportControlBlock iedModel_CLS_LLN0_report1 = {&iedModel_CLS_LLN0, "CLS_Betriebsstatus01", "RC_clsStatus", false, "CLS_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_CLS_LLN0_report2};
ReportControlBlock iedModel_CLS_LLN0_report2 = {&iedModel_CLS_LLN0, "CLS_Fahrplan101", "RC_schd01", false, "CLS_Fahrplan1", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_CLS_LLN0_report3};
ReportControlBlock iedModel_CLS_LLN0_report3 = {&iedModel_CLS_LLN0, "CLS_Fahrplan201", "RC_schd02", false, "CLS_Fahrplan2", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_CLS_LLN0_report4};
ReportControlBlock iedModel_CLS_LLN0_report4 = {&iedModel_CLS_LLN0, "CLS_FahrplanCtl01", "RC_schdCtl", false, "CLS_FahrplanCtl", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_CLS_LLN0_report5};
ReportControlBlock iedModel_CLS_LLN0_report5 = {&iedModel_CLS_LLN0, "CLS_Netzmessungen01", "RC_pccMX", false, "CLS_Netzmessungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_CLS_LLN0_report6};
ReportControlBlock iedModel_CLS_LLN0_report6 = {&iedModel_CLS_LLN0, "CLS_Zell01", "RC_cell", false, "CLS_Zell", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_PV_LLN0_report0};
ReportControlBlock iedModel_PV_LLN0_report0 = {&iedModel_PV_LLN0, "PV_Stammdaten01", "RC_pvPlate", false, "PV_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_PV_LLN0_report1};
ReportControlBlock iedModel_PV_LLN0_report1 = {&iedModel_PV_LLN0, "PV_Betriebsstatus01", "RC_pvStatus", false, "PV_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_PV_LLN0_report2};
ReportControlBlock iedModel_PV_LLN0_report2 = {&iedModel_PV_LLN0, "PV_Messungen01", "RC_pvMX", false, "PV_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_PV2_LLN0_report0};
ReportControlBlock iedModel_PV2_LLN0_report0 = {&iedModel_PV2_LLN0, "PV_Stammdaten01", "RC_pvPlate02", false, "PV_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_PV2_LLN0_report1};
ReportControlBlock iedModel_PV2_LLN0_report1 = {&iedModel_PV2_LLN0, "PV_Betriebsstatus01", "RC_pvStatus02", false, "PV_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_PV2_LLN0_report2};
ReportControlBlock iedModel_PV2_LLN0_report2 = {&iedModel_PV2_LLN0, "PV_Messungen01", "RC_pvMX02", false, "PV_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_BAT_LLN0_report0};
ReportControlBlock iedModel_BAT_LLN0_report0 = {&iedModel_BAT_LLN0, "BAT_Stammdaten01", "RC_batPlate", false, "BAT_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_BAT_LLN0_report1};
ReportControlBlock iedModel_BAT_LLN0_report1 = {&iedModel_BAT_LLN0, "BAT_Betriebsstatus01", "RC_batStatus", false, "BAT_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_BAT_LLN0_report2};
ReportControlBlock iedModel_BAT_LLN0_report2 = {&iedModel_BAT_LLN0, "BAT_Messungen01", "RC_batMX", false, "BAT_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_HEAT_LLN0_report0};
ReportControlBlock iedModel_HEAT_LLN0_report0 = {&iedModel_HEAT_LLN0, "HEAT_Stammdaten01", "RC_heatPlate", false, "HEAT_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_HEAT_LLN0_report1};
ReportControlBlock iedModel_HEAT_LLN0_report1 = {&iedModel_HEAT_LLN0, "HEAT_Betriebsstatus01", "RC_heatStatus", false, "HEAT_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_HEAT_LLN0_report2};
ReportControlBlock iedModel_HEAT_LLN0_report2 = {&iedModel_HEAT_LLN0, "HEAT_Messungen01", "RC_heatMX", false, "HEAT_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_EMOB_LLN0_report0};
ReportControlBlock iedModel_EMOB_LLN0_report0 = {&iedModel_EMOB_LLN0, "EMOB_Stammdaten01", "RC_emobPlate", false, "EMOB_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_EMOB_LLN0_report1};
ReportControlBlock iedModel_EMOB_LLN0_report1 = {&iedModel_EMOB_LLN0, "EMOB_Betriebsstatus01", "RC_emobStatus", false, "EMOB_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_EMOB_LLN0_report2};
ReportControlBlock iedModel_EMOB_LLN0_report2 = {&iedModel_EMOB_LLN0, "EMOB_Messungen01", "RC_emobMX", false, "EMOB_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_MEAS_LLN0_report0};
ReportControlBlock iedModel_MEAS_LLN0_report0 = {&iedModel_MEAS_LLN0, "MEAS_Stammdaten01", "RC_measPlate", false, "MEAS_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_MEAS_LLN0_report1};
ReportControlBlock iedModel_MEAS_LLN0_report1 = {&iedModel_MEAS_LLN0, "MEAS_Betriebsstatus01", "RC_measStatus", false, "MEAS_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_MEAS_LLN0_report2};
ReportControlBlock iedModel_MEAS_LLN0_report2 = {&iedModel_MEAS_LLN0, "MEAS_Messungen01", "RC_measMX", false, "MEAS_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_MEAS2_LLN0_report0};
ReportControlBlock iedModel_MEAS2_LLN0_report0 = {&iedModel_MEAS2_LLN0, "MEAS_Stammdaten01", "RC_measPlate02", false, "MEAS_Stammdaten", 1, 7, 255, 0, 99999999, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_MEAS2_LLN0_report1};
ReportControlBlock iedModel_MEAS2_LLN0_report1 = {&iedModel_MEAS2_LLN0, "MEAS_Betriebsstatus01", "RC_measStatus02", false, "MEAS_Betriebsstatus", 1, 15, 255, 0, 900000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, &iedModel_MEAS2_LLN0_report2};
ReportControlBlock iedModel_MEAS2_LLN0_report2 = {&iedModel_MEAS2_LLN0, "MEAS_Messungen01", "RC_measMX02", false, "MEAS_Messungen", 1, 8, 255, 0, 10000, {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0}, NULL};







IedModel iedModel = {
    "CLSGatewayTHU",
    &iedModel_CLS,
    &iedModelds_CLS_LLN0_CLS_Stammdaten,
    &iedModel_CLS_LLN0_report0,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    initializeValues
};

static void
initializeValues()
{

iedModel_CLS_LLN0_Mod_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_CLS_LLN0_Health_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_CLS_LLN0_NamPlt_vendor.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_LLN0_NamPlt_swRev.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_LLN0_NamPlt_d.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_LLN0_NamPlt_configRev.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_CLS_LPHD1_PhyNam_vendor.mmsValue = MmsValue_newVisibleString("iGrids");

iedModel_CLS_CLS_LPHD1_PhyNam_hwRev.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_CLS_LPHD1_PhyNam_swRev.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_CLS_LPHD1_PhyNam_serNum.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_CLS_LPHD1_PhyNam_model.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_CLS_LPHD1_PhyHealth_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_CLS_CLS_LPHD1_PhyHealth_d.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_CLS_LPHD1_Mod_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_CLS_PCC_MMXU1_Health_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_CLS_PCC_MMXU1_Health_d.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_PCC_MMXU1_NamPlt_vendor.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_PCC_MMXU1_NamPlt_swRev.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_PCC_MMXU1_NamPlt_d.mmsValue = MmsValue_newVisibleString("");

iedModel_CLS_PCC_MMXU1_TotW_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_CLS_PCC_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_CLS_PCC_MMXU1_TotVAr_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_CLS_PCC_MMXU1_TotVAr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_CLS_PCC_MMXU1_Hz_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_CLS_PCC_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);

iedModel_CLS_PCC_MMXU1_PhV_phsA_cVal_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_CLS_PCC_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_CLS_PCC_MMXU1_PhV_phsB_cVal_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_CLS_PCC_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_CLS_PCC_MMXU1_PhV_phsC_cVal_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_CLS_PCC_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_CLS_PCC_MMXU1_Mod_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_PV_PV_ZINV1_WRtg_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV_PV_ZINV1_WRtg_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_PV_PV_ZINV1_PQVLimSet_xUnits_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV_PV_ZINV1_PQVLimSet_yUnits_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(5);

iedModel_PV_PV_ZINV1_OutWSet_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV_PV_ZINV1_OutWSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(86);

iedModel_PV_PV_ZINV1_OutVarSet_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV_PV_ZINV1_OutVarSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_PV_PV_ZINV1_OutPFSet_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV_PV_ZINV1_GridModSt_stVal.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_PV_PV_MMXU1_TotW_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV_PV_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_PV_PV_MMXU1_TotVAr_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV_PV_MMXU1_TotVAr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_PV_PV_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV_PV_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV_PV_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV_PV_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);

iedModel_PV2_PV_ZINV1_WRtg_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV2_PV_ZINV1_WRtg_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_PV2_PV_ZINV1_PQVLimSet_xUnits_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV2_PV_ZINV1_PQVLimSet_yUnits_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(5);

iedModel_PV2_PV_ZINV1_OutWSet_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV2_PV_ZINV1_OutWSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(86);

iedModel_PV2_PV_ZINV1_OutVarSet_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV2_PV_ZINV1_OutVarSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_PV2_PV_ZINV1_OutPFSet_setMag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV2_PV_ZINV1_GridModSt_stVal.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_PV2_PV_MMXU1_TotW_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV2_PV_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_PV2_PV_MMXU1_TotVAr_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_PV2_PV_MMXU1_TotVAr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_PV2_PV_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV2_PV_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV2_PV_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_PV2_PV_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);

iedModel_BAT_BAT_DSTO1_TotEnNom_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(72);

iedModel_BAT_BAT_DSTO1_TotEnAbs_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(87);

iedModel_BAT_BAT_DSTO1_SoCAhr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(86);

iedModel_BAT_BAT_DSTO1_OutWh_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(86);

iedModel_BAT_BAT_DSTO1_OpTmh_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(4);

iedModel_BAT_BAT_DSTO1_SoCHiAls_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(86);

iedModel_BAT_BAT_DSTO1_SoCLoAls_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(86);

iedModel_BAT_BAT_DSTO1_EnOut_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(87);

iedModel_BAT_BAT_DBAT1_Vol_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_BAT_BAT_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_BAT_BAT_MMXU1_TotVAr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_BAT_BAT_MMXU1_TotVA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(61);

iedModel_BAT_BAT_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);

iedModel_BAT_BAT_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_BAT_BAT_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_BAT_BAT_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_HEAT_HEAT_DCTS1_ThrmCapTot_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(72);

iedModel_HEAT_HEAT_DCTS1_OpTmh_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(4);

iedModel_HEAT_HEAT_STMP1_Tmp_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(23);

iedModel_HEAT_HEAT_STMP1_MaxTmp_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(23);

iedModel_HEAT_HEAT_STMP1_MinTmp_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(23);

iedModel_HEAT_HEAT_ZCTS1_TmpRtg_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(23);

iedModel_HEAT_HEAT_ZCTS1_TmpSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(23);

iedModel_HEAT_HEAT_ZCTS1_OpMod_setVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_HEAT_HEAT_ZCTS1_OutWSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_HEAT_HEAT_ZCTS1_InWSet_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_HEAT_HEAT_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_HEAT_HEAT_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_HEAT_HEAT_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_HEAT_HEAT_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_HEAT_HEAT_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);

iedModel_EMOB_EMOB_DEAO1_ConnSt_stVal.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_EMOB_EMOB_DEAO1_CabRtgAC_stVal.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_EMOB_EMOB_DEAO1_Health_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_EMOB_EMOB_DEAO1_ChaTm_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(4);

iedModel_EMOB_EMOB_DEAO1_ChaEn_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(72);

iedModel_EMOB_EMOB_DEEV1_DptTm_setCal_mn.mmsValue = MmsValue_newUnsignedFromUint32(0);

iedModel_EMOB_EMOB_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_EMOB_EMOB_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_EMOB_EMOB_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_EMOB_EMOB_MMXU1_A_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(5);

iedModel_EMOB_EMOB_MMXU1_A_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(5);

iedModel_EMOB_EMOB_MMXU1_A_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(5);

iedModel_EMOB_EMOB_DEAO2_ConnSt_stVal.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_EMOB_EMOB_DEAO2_CabRtgAC_stVal.mmsValue = MmsValue_newIntegerFromInt32(0);

iedModel_EMOB_EMOB_DEAO2_Health_stVal.mmsValue = MmsValue_newIntegerFromInt32(1);

iedModel_EMOB_EMOB_DEAO2_ChaTm_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(4);

iedModel_EMOB_EMOB_DEAO2_ChaEn_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(72);

iedModel_EMOB_EMOB_DEEV2_DptTm_setCal_mn.mmsValue = MmsValue_newUnsignedFromUint32(0);

iedModel_MEAS_MEAS_MMXU1_TotW_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_MEAS_MEAS_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_MEAS_MEAS_MMXU1_TotVAr_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_MEAS_MEAS_MMXU1_TotVAr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_MEAS_MEAS_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_MEAS_MEAS_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_MEAS_MEAS_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_MEAS_MEAS_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);

iedModel_MEAS2_MEAS_MMXU1_TotW_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_MEAS2_MEAS_MMXU1_TotW_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(38);

iedModel_MEAS2_MEAS_MMXU1_TotVAr_mag_f.mmsValue = MmsValue_newFloat(0.0);

iedModel_MEAS2_MEAS_MMXU1_TotVAr_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(63);

iedModel_MEAS2_MEAS_MMXU1_PhV_phsA_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_MEAS2_MEAS_MMXU1_PhV_phsB_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_MEAS2_MEAS_MMXU1_PhV_phsC_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(29);

iedModel_MEAS2_MEAS_MMXU1_Hz_units_SIUnit.mmsValue = MmsValue_newIntegerFromInt32(33);
}
