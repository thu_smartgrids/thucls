/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "clsProfile.h"

#include <math.h>
#include "iec61850_server.h"
#include "static_model.h"
#include "targetlayer.h"
#include <QDebug>

static CheckHandlerResult checkHandler(void* parameter, MmsValue* ctlVal, bool test, bool interlockCheck, ClientConnection connection);
static ControlHandlerResult controlHandler(void* parameter, MmsValue* value, bool test);

static ClsProfile* todoRemove = NULL;
static Scheler* staticScheler = NULL;

static char objectRef[255];

//TODO: shift to ChaLing and make not static
typedef enum {
	phsUnknown = 0,
	phsSingle = 1,
	phsThree = 2
} PhsCount;
static PhsCount phsCount = phsUnknown;

//TODO: just some helpers to preserve version history
#define iedServer soefler.iedServer
void ClsProfile::setBatPower(QVariant value)
{
	qInfo() << "call signalSetBatPower" << value;
	if( value.isValid() )
		signalSetBatPower(value.toInt());
}

void ClsProfile::setLocAgent(QVariant value)
{
	qInfo() << "call signalAgent" << value;
	if( value.isValid() )
		signalSetAgent(value.toInt());
}

void ClsProfile::slotSetChaPwrLim(QVariant value)
{
	emit signalSetChaMaxI(value.toInt()/400);
}

ClsProfile::ClsProfile(QObject* parent) : QObject(parent), soefler(this), scheler(&soefler, "FSCH1")
{
	static int instances = 0;
	++instances;

	soefler.addBlock("CLS", (LogicalNode*)IEDMODEL_CLS);
	soefler.addBlock("LLN0", IEDMODEL_CLS_LLN0);
	soefler.addBlock("LPHD1", IEDMODEL_CLS_CLS_LPHD1);
	soefler.addBlock("ZELL1", IEDMODEL_CLS_ZELL_ZELL1);
	soefler.addBlock("NVP_MMXU1", IEDMODEL_CLS_PCC_MMXU1);
	soefler.addBlock("PV", (LogicalNode*)IEDMODEL_PV);
	soefler.addBlock("PV_ZINV1", IEDMODEL_PV_PV_ZINV1);
	soefler.addBlock("PV_MMXU1", IEDMODEL_PV_PV_MMXU1);
	soefler.addBlock("PV2", (LogicalNode*)IEDMODEL_PV2);
	soefler.addBlock("PV2_ZINV1", IEDMODEL_PV2_PV_ZINV1);
	soefler.addBlock("PV2_MMXU1", IEDMODEL_PV2_PV_MMXU1);
	soefler.addBlock("EMOB", (LogicalNode*)IEDMODEL_EMOB);
	soefler.addBlock("EMOB_DESE1", IEDMODEL_EMOB_EMOB_DESE1);
	soefler.addBlock("EMOB_DEAO1", IEDMODEL_EMOB_EMOB_DEAO1);
	soefler.addBlock("EMOB_DEEV1", IEDMODEL_EMOB_EMOB_DEEV1);
	soefler.addBlock("EMOB_MMXU1", IEDMODEL_EMOB_EMOB_MMXU1);
	soefler.addBlock("BAT", (LogicalNode*)IEDMODEL_BAT);
	soefler.addBlock("BAT_DSTO1", IEDMODEL_BAT_BAT_DSTO1);
	soefler.addBlock("BAT_ZINV1", IEDMODEL_BAT_BAT_ZINV1);
	soefler.addBlock("BAT_DBAT1", IEDMODEL_BAT_BAT_DBAT1);
	soefler.addBlock("BAT_MMXU1", IEDMODEL_BAT_BAT_MMXU1);
	soefler.addBlock("HEAT", (LogicalNode*)IEDMODEL_HEAT);
	soefler.addBlock("HEAT_DCTS1", IEDMODEL_HEAT_HEAT_DCTS1);
	soefler.addBlock("HEAT_STMP1", IEDMODEL_HEAT_HEAT_STMP1);
	soefler.addBlock("HEAT_ZCTS1", IEDMODEL_HEAT_HEAT_ZCTS1);
	soefler.addBlock("HEAT_MMXU1", IEDMODEL_HEAT_HEAT_MMXU1);
	soefler.addBlock("MEAS", (LogicalNode*)IEDMODEL_MEAS);
	soefler.addBlock("MEAS_MMXU1", IEDMODEL_MEAS_MEAS_MMXU1);
	soefler.addBlock("MEAS2", (LogicalNode*)IEDMODEL_MEAS2);
	soefler.addBlock("MEAS2_MMXU1", IEDMODEL_MEAS2_MEAS_MMXU1);

	soefler.addBlock("FSCH1", IEDMODEL_CLS_SCHD_FSCH1);
	soefler.addBlock("FSCH2", IEDMODEL_CLS_SCHD_FSCH2);
	soefler.addBlock("FSCC1", IEDMODEL_CLS_SCHD_FSCC1);

	/* Don't allow access to SP variables by default */
	IedServer_setWriteAccessPolicy(iedServer, IEC61850_FC_SP, ACCESS_POLICY_DENY);

	//IedServer_setWriteAccessPolicy(iedServer, IEC61850_FC_CF, ACCESS_POLICY_ALLOW);
	//IedServer_setWriteAccessPolicy(iedServer, IEC61850_FC_CO, ACCESS_POLICY_ALLOW);

	/* Instruct the server that we will be informed if a clients writes to a
	 * certain variables we are interested in.
	 */
	SoefReader* reader = NULL;
	reader = soefler.addReader("LocAgent.setVal", 0, 10, "ZELL1");
	connect(reader, SIGNAL(updated(QVariant)), this, SLOT(setLocAgent(QVariant)));
	reader = soefler.addReader("MinW.setMag.f", -100000, 100000, "ZELL1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetMinW(QVariant)));
	reader = soefler.addReader("MaxW.setMag.f", -100000, 100000, "ZELL1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetMaxW(QVariant)));

	reader = soefler.addReader("OutWSet.setMag.f", -2000, 2000, "BAT_ZINV1");
	connect(reader, SIGNAL(updated(QVariant)), this, SLOT(setBatPower(QVariant)));

	reader = soefler.addReader("OutWSet.setMag.f", 0, 100, "PV_ZINV1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetPercent(QVariant)));

	reader = soefler.addReader("SoCHiAls.setMag.f", 0, 100, "BAT_DSTO1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetBatMaxSOC(QVariant)));
	reader = soefler.addReader("SoCLoAls.setMag.f", 0, 100, "BAT_DSTO1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetBatMinSOC(QVariant)));
	reader = soefler.addReader("OpMod.setVal", 1, 10, "BAT_DSTO1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetBatOpMod(QVariant)));

	reader = soefler.addReader("ChaPwrLim.setMag.f", 0, 32000, "EMOB_DESE1");
	connect(reader, SIGNAL(updated(QVariant)), this, SLOT(slotSetChaPwrLim(QVariant)));
	reader = soefler.addReader("ChaAMax.setVal", 0, 80, "EMOB_DEAO1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetChaMaxI(QVariant)));

	reader = soefler.addReader("InWSet.setMag.f", -1, 4000, "HEAT_ZCTS1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetHomePower(QVariant)));
	reader = soefler.addReader("OutWSet.setMag.f", -1, 4000, "HEAT_ZCTS1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetHeatPower(QVariant)));
	reader = soefler.addReader("OpMod.setVal", 1, 2, "HEAT_ZCTS1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetHeatOpMod(QVariant)));
	reader = soefler.addReader("TmpSet.setMag.f", 0, 80, "HEAT_ZCTS1"); //TODO: either 0 or 40..80
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetHeatSetTmp(QVariant)));
	reader = soefler.addReader("MaxTmp.setMag.f", 60, 80, "HEAT_STMP1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetHeatMaxTmp(QVariant)));
	reader = soefler.addReader("MinTmp.setMag.f", 40, 80, "HEAT_STMP1");
	connect(reader, SIGNAL(updated(QVariant)), this, SIGNAL(signalSetHeatMinTmp(QVariant)));

	IedServer_setControlHandler(iedServer, IEDMODEL_CLS_SCHD_FSCH1_EnaReq, controlHandler, IEDMODEL_CLS_SCHD_FSCH1_EnaReq);
	IedServer_setPerformCheckHandler(iedServer, IEDMODEL_CLS_SCHD_FSCH1_EnaReq, checkHandler, IEDMODEL_CLS_SCHD_FSCH1_EnaReq);
	IedServer_setControlHandler(iedServer, IEDMODEL_CLS_SCHD_FSCH1_DsaReq, controlHandler, IEDMODEL_CLS_SCHD_FSCH1_DsaReq);
	IedServer_setPerformCheckHandler(iedServer, IEDMODEL_CLS_SCHD_FSCH1_DsaReq, checkHandler, IEDMODEL_CLS_SCHD_FSCH1_DsaReq);

	SchedController* ctrl = new SchedController(&soefler, "FSCC1");
	ctrl->init("FSCH1", "BAT_ZINV1.OutWSet.setMag.f");

	QVariantList valAsg({10,20,30,40,50});
	soefler.writeList("ValASG", 4, "setMag.f", valAsg, "FSCH1");
	todoRemove = this;
	staticScheler = &scheler;
}

ClsProfile* ClsProfile::getInstance(QObject* parent)
{
	static ClsProfile instance(parent);

	return &instance;
}

ClsProfile::~ClsProfile()
{
}

void ClsProfile::start()
{
	soefler.start();
}

static CheckHandlerResult checkHandler(void* parameter, MmsValue* ctlVal, bool test, bool interlockCheck, ClientConnection connection)
{
	Q_UNUSED(connection)

	if( parameter == NULL ){
		qDebug() << "check handler called with NULL param";
		return CONTROL_HARDWARE_FAULT;
	}
	qDebug() << "check handler called" << (interlockCheck? "with interlock check": "") << ModelNode_getObjectReference((ModelNode*) parameter, objectRef);

	return staticScheler->checkHandler(parameter, ctlVal, test, interlockCheck);
}

static ControlHandlerResult controlHandler(void* parameter, MmsValue* value, bool test)
{
	if( parameter == NULL ){
		qDebug() << "control handler called with NULL param";
		return CONTROL_RESULT_FAILED;
	}
	qDebug() << "control handler called" << ModelNode_getObjectReference((ModelNode*) parameter, objectRef);

	return staticScheler->controlHandler(parameter, value, test);
}


void ClsProfile::init(QString name, QString info)
{
	bool ok = soefler.lock("LLN0");
	if( !ok ){
		return;
	}
	soefler.write("NamPlt.vendor", "HS Ulm Smart Grid Research Group");
	soefler.write("NamPlt.swRev", TargetLayer::getSwRevision());
	soefler.write("NamPlt.d", info);
	soefler.write("NamPlt.configRev", name);

	soefler.update("Beh", Beh_on);
	soefler.update("Health", 1);

	soefler.select("LPHD1");
	soefler.write("PhyNam.serNum", TargetLayer::getMac());
	soefler.update("PhyHealth", 1);
	soefler.unlock();

	/*
	IEDMODEL_LD1_LPHD1_PhyNam_vendor
	IEDMODEL_LD1_LPHD1_PhyNam_hwRev
	IEDMODEL_LD1_LPHD1_PhyNam_swRev
	IEDMODEL_LD1_LPHD1_PhyNam_model
	IEDMODEL_LD1_LPHD1_PhyHealth_d
	*/

	scheler.init();
}

void ClsProfile::initDevice(QVariant vendor, QVariant swRev, QVariant d, qint32 ldMode, bool metersPCC)
{
	soefler.write("LLN0.NamPlt.vendor", vendor);
	soefler.write("LLN0.NamPlt.swRev", swRev);
	soefler.write("LLN0.NamPlt.d", d);

	soefler.update("LLN0.Mod", 1);
	soefler.update("LLN0.Beh", std::max(ldMode, 1));
	//soefler.updateBeh(ldMode, Beh_on);
	//soefler.update("Health", 1);

	if( metersPCC ){
		soefler.write("NamPlt.vendor", vendor, "NVP_MMXU1");
		soefler.write("NamPlt.swRev", swRev, "NVP_MMXU1");
		soefler.write("NamPlt.d", d, "NVP_MMXU1");

		soefler.update("Mod", 1, CURRENT_TIME, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
		soefler.updateBeh(1, 1, CURRENT_TIME, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
		//soefler.update("Health", 1, "NVP_MMXU1");
	}
}

void ClsProfile::initMeter(QVariant vendor, QVariant swRev, QVariant d)
{
       bool ok = soefler.lock("NVP_MMXU1");
       if( !ok ){
	       return;
       }
       soefler.write("NamPlt.vendor", vendor);
       soefler.write("NamPlt.swRev", swRev);
       soefler.write("NamPlt.d", d);

       soefler.update("Mod", 1);
       soefler.updateBeh(1, 1);
       soefler.unlock();
}

void ClsProfile::initMeterAt2(QVariant vendor, QVariant swRev, QVariant d)
{
	bool ok = soefler.lock("MEAS2");
	if( !ok ){
		return;
	}
	initDevice(vendor, swRev, d, 1, false);
	soefler.unlock();
}

void ClsProfile::initPV(Modler* source)
{
	/*only once*/
	source->reconnect();

	bool ok = soefler.lock("PV");
	if( !ok ){
		return;
	}

	initDevice( source->read("Mn"),
		    source->read("Md"),
		    source->read("Vr"),
		    Beh_on,
		    source->metersPCC() );

	soefler.write("PV_ZINV1.WRtg.setMag.f", source->read("WRtg"));
	soefler.unlock();
}

bool ClsProfile::initSLog(GenIo *source)
{
	bool ok = soefler.lock("PV");
	if( !ok ){
		return false;
	}

	initDevice( "SolarLog", "", "",
		    Beh_on,
		    source->metersPCC() );

	soefler.write("PV_ZINV1.WRtg.setMag.f", source->read("totalPower"));
	soefler.unlock();
	return true;
}

bool ClsProfile::initCharger(Modler* source)
{
	static const int aMax = 32; /* shall match the position of rotary switch "Preset Charge Current" */
		/* 6 A, 10 A, 13 A, 16 A, 20 A, 32 A, 63 A, 70 A, 80 A */
	static const float wMax = aMax * 250 * 3; /* 250V as described in manual */

	source->readBlock("status");
	bool ok = soefler.lock("EMOB");
	if( !ok ){
		return false;
	}
	initDevice( "HS-Ulm Smart Grid Research Group",
		    "Fancy handmade Charger based on Phoenix Contact Charge Control 104924",
		    source->read("FirmwareVersion"),
		    Beh_on,
		    source->metersPCC() );

	soefler.select("EMOB_DESE1");
	soefler.write("EVSENam.vendor", "HS-Ulm Smart Grid Research Group");
	soefler.write("EVSENam.model", "Fancy handmade Charger based on Phoenix Contact Charge Control 104924");
	soefler.write("EVSENam.hwRev", "0.1");
	soefler.write("EVSENam.swRev", source->read("FirmwareVersion"));
	soefler.write("EVSENam.serNum", source->read("SerialNo"));
	soefler.write("EVSENam.primeOper", "HS Ulm");
	soefler.write("ChaPwrRtg.setMag.f", wMax);
	soefler.updateBeh(1, 1);

	soefler.select("EMOB_DEAO1");
	soefler.write("ChaARtg.setVal", aMax);
	soefler.write("ConnTypDC.setVal", false);
	soefler.write("ConnTypPhs.setVal", true); /* 3 phase, compare with standard */

	soefler.select("EMOB_DEEV1");
	soefler.write("EVNam.vendor", "Unknown");
	/*
	 * 0 unknown
	 * 1 single phase charger
	 * 2 three phase charger
	 */
	soefler.update("ConnTypSel.stVal", 2); /* always 3 phase TBC */
	soefler.unlock();

	bool isBigEndian = source->read("ByteSequence").toBool();
	if( ! isBigEndian ){
		qWarning() << "incompatible set up";
		return false;
	}

	return true;
}

bool ClsProfile::initHeat(GenIo* source)
{
	QVariant value;

	source->readBlock("ident1");

	QString hardware = source->read("ProductName").toString()+ ", " + source->read("ProductID").toString() + "." + source->read("ProductVersion").toString() + ", " + source->read("SerialNumber").toString();

	bool ok = soefler.lock("HEAT");
	if( !ok ){
		return false;
	}

	initDevice( source->read("VendorName"),
		    source->read("FirmwareVersion"),
		    hardware,
		    Beh_on,
		    source->metersPCC() );

	value = 45000;
	soefler.write("ThrmCapTot.mag.f", value, "HEAT_DCTS1");

	soefler.unlock();

	return true;
}

bool ClsProfile::initJanitzaMeter(GenIo *source)
{
	source->readBlock("ident1");
	quint32 serialNumber = source->read("SerialNumber").toUInt();
	quint32 itemNumber = source->read("ItemNumber").toUInt();
	QString hardware;
	if( serialNumber == 0x30303030 && itemNumber == 0x30303030 ){
		hardware = source->read("HardwareIndex").toString();
	} else {
		hardware = source->read("DeviceName").toString() + ", " + source->read("SerialNumber").toString() + ", " + source->read("HardwareIndex").toString();
	}

	bool ok = soefler.lock("MEAS");
	if( !ok ){
		return false;
	}

	initDevice( "Janitza",
		    source->read("ReleaseBase"),
		    hardware,
		    Beh_on,
		    source->metersPCC() );

	soefler.unlock();
	return true;
}

bool ClsProfile::initBat(GenIo* source)
{
	QString hardware, software;

	/*  REMOVED for legal reasons */

	hardware = "SonnenBatterie Eco Series";
	software = "public API";

	bool ok = soefler.lock("BAT");
	if( !ok ){
		return false;
	}

	initDevice( "Sonnen",
		    software,
		    hardware,
		    Beh_on,
		    source->metersPCC() );


	/*  REMOVED for legal reasons */

	soefler.unlock();

	return true;
}

void ClsProfile::updateDevice(GenIo* device, io_ConnState health, QString healthMsg)
{
	qint64 ctime = 0;
	io_ConnState conn;

	conn = device->getHealth(&ctime);

	if ( health <= conn ){
		health = conn;
		healthMsg = GenIo::io_getConnStateStr(conn);
	} else {
		//ctime;
	}
	//todo: alternatively put connection state to health.q (good, questionable, invalid)


	soefler.update("LLN0.Health", health, ctime);
	//soefler.write("LLN0.Health.d", healthMsg);

	if( device->metersPCC() ){
		soefler.update("Health", health, ctime, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
		soefler.write("Health.d", healthMsg, "NVP_MMXU1");
	}
}

void ClsProfile::updateMeter(float power, qint32 quality)
{
	soefler.update("TotW", power, CURRENT_TIME, quality, "NVP_MMXU1");
}

const QString getEvt1Str(qint32 evt1) {
	static const QString str[] = {"Ground fault", "DC over voltage", "AC disconnect open",
		"DC disconnect open", "Grid shutdown", "Cabinet open", "Manual shutdown", "Over temperature",
		"Frequency above limit", "Frequency under limit", "AC Voltage above limit",
		"AC Voltage under limit", "Blown String fuse on input", "Under temperature",
		"Generic Memory or Communication error (internal)", "Hardware test failure"};
	if( 0<= evt1 && evt1 <= 15 ){
		return str[evt1];
	}
	return "";
}

void ClsProfile::updatePV(Modler* source)
{
	uint64_t timeval;
	QVariant value, W, VAr, grid;
	QString healthMsg, msg;
	qint32 gridSt, mod;
	bool metersPCC;
	io_ConnState health = io_Unknwn;

	timeval = Hal_getTimeInMs();

	source->readBlock("measures1");
	source->readBlock("measures2");
	metersPCC = source->metersPCC();


	bool ok = soefler.lock("PV");
	if( !ok ){
		return;
	}

	value = source->read("Evt1");
	if( value.isValid() && value.toInt() != 0 ){
		health = io_IsLost;
		healthMsg = getEvt1Str(value.toInt());
	}
	updateDevice(source, health, healthMsg);

	soefler.select("PV_MMXU1");
	value = source->read("Hz");
	soefler.update("Hz", value);
	if( metersPCC ){
		soefler.update("Hz", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("PhVphA");
	soefler.update("PhV.phsA", value);
	if( metersPCC ){
		soefler.update("PhV.phsA", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("PhVphB");
	soefler.update("PhV.phsB", value);
	if( metersPCC ){
		soefler.update("PhV.phsB", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("PhVphC");
	soefler.update("PhV.phsC", value);
	if( metersPCC ){
		soefler.update("PhV.phsC", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	W = source->read("W");
	VAr = source->read("VAr");
	soefler.update("TotW", W);
	soefler.update("TotVAr", VAr);
	//TODO: use of aggregated values if meter is not connected to CLS (in each case quality is questionable)
	if( metersPCC ){
		soefler.update("TotW", -W.toFloat(), timeval, QUALITY_VALIDITY_QUESTIONABLE, "NVP_MMXU1");
		soefler.update("TotVAr", -VAr.toFloat(), timeval, QUALITY_VALIDITY_QUESTIONABLE, "NVP_MMXU1");
	}

	//NVP Mode allways remains 1
	//IEDMODEL_CLS_NVP_MMXU1_Mod_stVal
	//IEDMODEL_CLS_NVP_MMXU1_Mod_q
	//IEDMODEL_CLS_NVP_MMXU1_Mod_t

	soefler.select("PV_ZINV1");
	grid = source->read("Conn");
	if( !grid.isValid() ){
		gridSt = 0;
		msg = "grid mode unknown";
	} else {
		if( !value.toBool() ){
			gridSt = 1;
			msg = "not connected to the grid";
		} else {
			if( W.toFloat() < 1.0 ){
				/* power below 1W is ignored */
				gridSt = 2;
				msg = "connected to grid but no power delivered";
			} else {
				gridSt = 3;
				msg = "connected to grid, delivering power";
			}
		}
	}
	soefler.update("GridModSt", gridSt);
	soefler.write("GridModSt.d", msg);

	value = source->read("St");
	switch( value.toInt() ){
	case 1:
		mod = 5; // disabled
		break;
	case 2:
	case 3:
	case 7:
	case 8:
		mod = 2; // blocked (TODO: is this the 61850 intention of blocked?)
		break;
	default:
		// 0 (invalid), 4, 5
		mod = 1; // enabled
	}
	//soefler.update("Mod", mod, timeval, grid.isValid()? QUALITY_VALIDITY_GOOD: QUALITY_VALIDITY_INVALID);

	soefler.update("OutWSet", source->read("WMaxLimPct"));
	soefler.unlock();
}

void ClsProfile::updateSLog(GenIo *source)
{
	QVariant value;
	bool metersPCC = source->metersPCC();
	uint64_t timeval = Hal_getTimeInMs();

	bool ok = soefler.lock("PV_MMXU1");
	if( !ok ){
		return;
	}

	value = source->read("Uac");
	soefler.update("PhV.phsA", value);
	if( metersPCC ){
		soefler.update("PhV.phsA", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("Pac");
	soefler.update("TotW", value);
	if( metersPCC ){
		soefler.update("TotW", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}

	soefler.unlock();
}

PhsCount getPhsCount (Modler* source)
{
	// TODO: static not good in this context
	static qint32 lastChargingTime = 0;
	static PhsCount lastPhsCount = phsUnknown;

	QVariant ix;
	QString registers[] = {"DispI1", "DispI2", "DispI3"};
	quint32 count = 0;

	QVariant chargingTime = source->read("ChargingTime");

	if( !chargingTime.isValid() ){
		return lastPhsCount;
	}
	if( chargingTime.toInt() < lastChargingTime ){
		lastPhsCount = phsUnknown;
	}

	if( lastPhsCount == phsUnknown || lastPhsCount == phsSingle ){
		for( int i=0; i < 3; i++ )
		{
			ix = source->read(registers[i]);
            if( ix.toFloat() >= 3.0 ){
				count++;
			}
		}
		/* if count equals 0 the old state remains */
		if( count == 1 ){
			lastPhsCount = phsSingle;
		}
		if( count == 2 ){
			lastPhsCount = phsUnknown;
		}
		if( count == 3 ){
			lastPhsCount = phsThree;
		}
	}
	return lastPhsCount;
}

void ClsProfile::updateCharger(Modler* source)
{
	static const float iSqrRoot3 = 1 / sqrt(3);
	static const float sqrRoot3 = sqrt(3);
	uint64_t timeval;
	qint16 pwmSignal;
	float chargingVoltage, chargingCurrent, chargingEnergy;
	float realPower, reactivePower, powerLim, powerFactor;
	char evStatus;
	bool metersPCC;
	quint16 errorCodes;
	qint32 chargingTime;
	bool lockDetection; //, stationAvail;
	QVariant value, value2;
	QString errDescription;

	qint32 connSt, plgStatAC = 0;

	timeval = Hal_getTimeInMs();
	source->readBlock("status");
	source->readBlock("measures1");
	source->readBlock("measures2");
	metersPCC = source->metersPCC();

	/*update*/
	/*
	IEDMODEL_CLS_EMOB_DESE1_ChaPwrRtg_setMag_f
	IEDMODEL_CLS_EMOB_DESE1_ChaPwrLim_setMag
	*/

	bool ok = soefler.lock("EMOB");
	if( !ok ){
		return;
	}
	/* soefler.update("Beh", ); */

	value = source->read("StationAvail");
	io_ConnState health = value.toBool()? io_IsOkay: io_IsUgly;

	value =  source->read("ErrorCodes");
	if( value.isValid() ){
		errorCodes = value.toInt();
		if( errorCodes == 0 ){
			errDescription = "no error present";
		} else {
			errDescription = "Error(s) present: " + QString::number(errorCodes);
		}
	} else {
		errDescription = "error status unknown";
	}
	updateDevice(source, health, errDescription);

	soefler.select("EMOB_MMXU1");
	value = source->read("DispFrequency");
	soefler.update("Hz", value);
	if( metersPCC ){
		soefler.update("Hz", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("DispV1");
	soefler.update("PhV.phsA", value);
	if( metersPCC ){
		soefler.update("PhV.phsA", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("DispV2");
	soefler.update("PhV.phsB", value);
	if( metersPCC ){
		soefler.update("PhV.phsB", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}
	value = source->read("DispV3");
	soefler.update("PhV.phsC", value);
	if( metersPCC ){
		soefler.update("PhV.phsC", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}

	value = source->read("DispI1");
	soefler.update("A.phsA", value);
	value = source->read("DispI2");
	soefler.update("A.phsB", value);
	value = source->read("DispI3");
	soefler.update("A.phsC", value);

	value = source->read("DispRealPower");
	soefler.update("TotW", value);
	if( metersPCC ){
		soefler.update("TotW", value, timeval, QUALITY_VALIDITY_QUESTIONABLE, "NVP_MMXU1");
	}
	/* value of "DispReactivePower" is invalid */
	value = source->read("DispApparentPower");
	value2 = source->read("DispPowerFactor");
	powerFactor = fabs(value2.toFloat());
	if( value.isValid() && value2.isValid() && powerFactor < 1){
		float apparentPower = value.toFloat() * (1 - powerFactor);
		value = QVariant(apparentPower);
	}
	soefler.update("TotVAr", value);
	if( metersPCC ){
		soefler.update("TotVAr", value, timeval, QUALITY_VALIDITY_QUESTIONABLE, "NVP_MMXU1");
	}

	/* soefler.update("Beh", ); */

	realPower = source->read("DispRealPower").toInt() * source->read("ConvRealPower").toFloat();
	soefler.update("TotW", realPower);

	reactivePower = source->read("DispReactivePower").toInt() * source->read("ConvReactivePower").toFloat();
	soefler.update("TotVAr", reactivePower);

	soefler.select("EMOB_DESE1");
//	soefler.update("Beh" ,);

	phsCount = getPhsCount(source);
	pwmSignal = source->read("PwmSignal").toInt();

	if( phsCount < phsThree ){
		powerLim = pwmSignal * 230;
	} else {
		powerLim = pwmSignal * 400 * sqrRoot3;
	}
	soefler.update("ChaPwrLim", powerLim);
/*	soefler.update("ChaPwrLim", pwmSignal * .4);*/

/*	chargingVoltage = source->read("DispV1").toFloat()
			+ source->read("DispV2").toFloat()
			+ source->read("DispV3").toFloat();
	chargingVoltage /= 3;
	soefler.update("ChaV", chargingVoltage);

	chargingCurrent = source->read("DispI1").toFloat()
			+ source->read("DispI2").toFloat()
			+ source->read("DispI3").toFloat();
	chargingCurrent *= iSqrRoot3;
	soefler.update("ChaA", chargingCurrent);*/

	soefler.select("EMOB_DEAO1");
	soefler.update("ChaAMax", pwmSignal);

	evStatus = source->read("EvStatus").toInt();
	chargingTime = source->read("ChargingTime").toInt();
	lockDetection = source->read("LockDetection").toBool();
	//stationAvail = source->read("StationAvail").toBool();

	if( 'A' <= evStatus && evStatus <= 'F' ){
		connSt = evStatus - 'A' + 1;
	} else {
		connSt = 0;
	}
	soefler.update("ConnSt", connSt);

	switch( evStatus ){
	case 'A':
	case 'F':
		plgStatAC = 1; // Disconnected
		break;
	case 'C':
	case 'D':
	case 'E':
		plgStatAC = 4; // Connected
		break;
	default:
		plgStatAC = 0; // N/A, Unknown
	}

	soefler.update("PlgStAC", plgStatAC);
	soefler.update("CabRtgAC", source->read("ProximityChargingCurrent"));
	soefler.update("Blk", source->read("ProximityChargingCurrent"));

	soefler.select("EMOB_DEEV1");

/*	rserved: "Soc", "DptTm"*/

	chargingEnergy = source->read("DispEnergyOfCurrentCharging").toInt() * source->read("ConvEnergyOfCurrentCharging").toFloat();
	soefler.update("EnAmnt", chargingEnergy);

	soefler.unlock();

	qDebug() << "state is: " << lockDetection << evStatus;
	qDebug() << "time is: " << chargingTime;
}


void ClsProfile::updateHeat(GenIo *source)
{
	uint64_t timeval;
	QVariant value, OpModHeat;
	float ThrmCapPct;
	qint16 TmpRtg;
	quint16 TmpSet, MaxTmp, MinTmp;
	quint32 tOperation;
	qint16 pwrNom, Tmp;
	qint32 pwrHome;
	quint16 nStep, stepW = 500;

	timeval = Hal_getTimeInMs();
	source->readBlock("measures1");
	source->readBlock("measures2");
	source->readBlock("measures3");

	bool ok = soefler.lock("HEAT");
	if( !ok ){
		return;
	}

	// todo: Error Code  5376-5414
	updateDevice(source, io_Unknwn);

	soefler.select("HEAT_DCTS1");
	tOperation = source->read("TotalOperatingSeconds").toInt();
	soefler.update("OpTmh", tOperation);

	Tmp = source->read("ActualTemperatureBoiler").toInt();
	MinTmp = source->read("TemperatureMinValue").toInt();
	MaxTmp = source->read("TemperatureMaxValue").toInt();

	double coeff = std::max(1.0, 100.0 / (MaxTmp-10));
	ThrmCapPct = std::max(0.0, std::min(100.0, (MaxTmp-Tmp)*coeff));

	soefler.update("ThrmCapPct", ThrmCapPct);

	soefler.select("HEAT_MMXU1");
	nStep = source->read("RelaisStatus").toInt();
	value = nStep * stepW; /* Actual Active Power */
	soefler.update("TotW", value);
	if( source->metersPCC() ){
		soefler.update("TotW", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}

	soefler.select("HEAT_STMP1");
	soefler.update("MaxTmp", MaxTmp);

	soefler.update("MinTmp", MinTmp);

	soefler.update("Tmp", Tmp);

	soefler.select("HEAT_ZCTS1");
	TmpRtg = source->read("UserTemperatureNominalValue").toInt();
	soefler.update("TmpRtg", TmpRtg);

	TmpSet = source->read("TemperatureNominalValue").toInt();
	soefler.update("TmpSet", TmpSet);

	pwrNom = source->read("PowerNominalValue").toInt();
	soefler.update("OutWSet", pwrNom);

	pwrHome = source->read("HomeTotalPower").toInt();
	soefler.update("InWSet", pwrHome);
	// To do: HomeTotalPower should be written by smart meter
	soefler.update("LocAgent", pthAgent, CURRENT_TIME, QUALITY_VALIDITY_GOOD, "ZELL1");

	if (pwrNom == -1){
		OpModHeat = 2;
	}else{
		OpModHeat = 1;
	}
	soefler.update("OpMod", OpModHeat);
	soefler.unlock();
}

void ClsProfile::updateJanitzaMeter(GenIo *source)
{
	bool metersPcc;

	uint64_t timeval = Hal_getTimeInMs();
	source->readBlock("measures1");
	metersPcc = source->metersPCC();

	bool ok = soefler.lock("MEAS");
	if( !ok ){
		return;
	}
	updateDevice(source, io_Unknwn);
	soefler.select("MEAS_MMXU1");
	soefler.update("Hz", source->read("Hz"), timeval);
	soefler.update("PhV.phsA", source->read("PhV_phsA"), timeval);
	soefler.update("PhV.phsB", source->read("PhV_phsB"), timeval);
	soefler.update("PhV.phsC", source->read("PhV_phsC"), timeval);

	soefler.update("TotVAr", source->read("TotVAr"), timeval, QUALITY_VALIDITY_QUESTIONABLE);
	soefler.update("TotW", source->read("TotW"), timeval, QUALITY_VALIDITY_QUESTIONABLE);


	if( metersPcc ){
		soefler.select("NVP_MMXU1");
		soefler.update("Hz", source->read("Hz"), timeval);
		soefler.update("PhV.phsA", source->read("PhV_phsA"), timeval);
		soefler.update("PhV.phsB", source->read("PhV_phsB"), timeval);
		soefler.update("PhV.phsC", source->read("PhV_phsC"), timeval);

		soefler.update("TotVAr", source->read("TotVAr"), timeval, QUALITY_VALIDITY_QUESTIONABLE);
		soefler.update("TotW", source->read("TotW"), timeval, QUALITY_VALIDITY_QUESTIONABLE);
	}
	soefler.unlock();
}

/* TODO: make a concept for second device */
void ClsProfile::updateJanitzaMeterAt2(GenIo *source)
{
	bool metersPcc;

	uint64_t timeval = Hal_getTimeInMs();
	source->readBlock("measures1");
	metersPcc = source->metersPCC();

	bool ok = soefler.lock("MEAS");
	if( !ok ){
		return;
	}
	updateDevice(source, io_Unknwn);
	soefler.select("MEAS2_MMXU1");
	soefler.update("Hz", source->read("Hz"), timeval);
	soefler.update("PhV.phsA", source->read("PhV_phsA"), timeval);
	soefler.update("PhV.phsB", source->read("PhV_phsB"), timeval);
	soefler.update("PhV.phsC", source->read("PhV_phsC"), timeval);

	soefler.update("TotVAr", source->read("TotVAr"), timeval, QUALITY_VALIDITY_QUESTIONABLE);
	soefler.update("TotW", source->read("TotW"), timeval, QUALITY_VALIDITY_QUESTIONABLE);

	if( metersPcc ){
		soefler.select("NVP_MMXU1");
		soefler.update("Hz", source->read("Hz"), timeval);
		soefler.update("PhV.phsA", source->read("PhV_phsA"), timeval);
		soefler.update("PhV.phsB", source->read("PhV_phsB"), timeval);
		soefler.update("PhV.phsC", source->read("PhV_phsC"), timeval);

		soefler.update("TotVAr", source->read("TotVAr"), timeval, QUALITY_VALIDITY_QUESTIONABLE);
		soefler.update("TotW", source->read("TotW"), timeval, QUALITY_VALIDITY_QUESTIONABLE);
	}
	soefler.unlock();
}

void ClsProfile::updateSchdVal(quint32 slot, QVariant value)
{
	soefler.lock("FSCH1");
	soefler.writeElement("ValASG", slot+1, 4, "setMag.f", value, "FSCH1");
	soefler.unlock();
}

void ClsProfile::updateBat(GenIo* source)
{
	uint64_t timeval;
	io_ConnState conn;
	bool metersPCC;
	qint64 ctime = 0;
	QVariant value;
	QVariant w, va, var;

	timeval = Hal_getTimeInMs();

	/*  REMOVED for legal reasons */

	source->readBlock("status");
	metersPCC = source->metersPCC();

	conn = source->getHealth(&ctime);

	bool ok = soefler.lock("BAT");
	if( !ok ){
		return;
	}
	updateDevice(source, conn);

	soefler.select("BAT_MMXU1");
	w = source->read("Pac_total_W", "status");
	va = source->read("Apparent_output", "status");
	if( w.isValid() && va.isValid() ){
		var = QVariant(sqrt(pow(va.toFloat(), 2) - pow(w.toFloat(), 2)));
	} else {
		var = QVariant(QVariant::Invalid);
	}

	soefler.update("TotW", w);
	if( metersPCC ){
		soefler.update("TotW", -w.toFloat(), timeval, QUALITY_VALIDITY_QUESTIONABLE, "NVP_MMXU1");
	}

	/*va*/
	soefler.update("TotVA", va);

	/*var*/
	soefler.update("TotVAr", var);
	if( metersPCC ){
		soefler.update("TotVAr", -var.toFloat(), timeval, QUALITY_VALIDITY_QUESTIONABLE, "NVP_MMXU1");
	}

	value = source->read("Fac", "status");
	soefler.update("Hz", value);
	if( metersPCC ){
		soefler.update("Hz", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}

	value = source->read("Uac", "status");
	soefler.update("PhV.phsA", value, timeval, QUALITY_VALIDITY_QUESTIONABLE);
	soefler.update("PhV.phsB", value, timeval, QUALITY_VALIDITY_QUESTIONABLE);
	soefler.update("PhV.phsC", value, timeval, QUALITY_VALIDITY_QUESTIONABLE);
	if( metersPCC ){
		soefler.update("PhV.phsA", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
		soefler.update("PhV.phsB", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
		soefler.update("PhV.phsC", value, timeval, QUALITY_VALIDITY_GOOD, "NVP_MMXU1");
	}

	soefler.select("BAT_DSTO1");
	value = source->read("RSOC", "status");
	soefler.update("SoCAhr", value);

	value = source->read("USOC", "status");
	soefler.update("OutWh", value);

	/*  REMOVED for legal reasons */

	value = source->read("OperatingMode", "status");
	soefler.update("OpMod", value);

	/*  REMOVED for legal reasons */

	soefler.select("BAT_DBAT1");
	value = source->read("Ubat", "status");
	soefler.update("Vol", value);

	soefler.select("BAT_ZINV1");

	value = source->read("GridFeedIn_W", "status");
	soefler.update("OutWSet", value);

	//IEDMODEL_CLS_BAT_ZINV1_OutVarSet_setMag_f
	soefler.unlock();
}

void ClsProfile::setAgent(qint32 agent)
{
	pthAgent = agent;
}

void ClsProfile::updateHealth()
{
	static quint32 failures_old = 0;

	quint32 failures = TargetLayer::getFailures();
	if( failures != failures_old ){
		failures_old = failures;

		uint64_t timeval = Hal_getTimeInMs();
		QString description = "failures: " + QString::number(failures);

		soefler.update("Health.stVal", 0, timeval, QUALITY_VALIDITY_GOOD, "LLN0");
		soefler.write("Health.d", description, "LLN0");
	}
}

