/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "scheler.h"

#include "iec61850_server.h"
#include "static_model.h"
#include "soefler.h"
#include <QDebug>
#include <QDateTime>
#include <QTimer>
#include <QVariant>
#define ds QDateTime::currentDateTime().toString("hh:mm:ss,zzz ").toLatin1().data()
#define MIN_DATE 946684800000 /* 1.1.2000 */
#define NO_TYPE_CHECK ((MmsType)0xFF)
#define NO_MIN Soefler::noCheck
#define NO_MAX Soefler::noCheck

//TODO: avoid static variables
static QMap<QString, Scheler*> schelerInstances;

Scheler::Scheler(Soefler* parent, QString ln): QObject(parent),
	enaReq(parent, "EnaReq", ln),
	dsaReq(parent, "DsaReq", ln),
	togtimer(this)
{
	soefler = parent;
	currentState = SchdNotReady;
	lnode = ln;
	intv = 1;
	entries = 0;
	//todo: internal id "ln" not intended for external use
	schelerInstances.insert(ln, this);

	connect(&togtimer, SIGNAL(timeout()), this, SLOT(slotToggleSchedule()));

	connect(this, SIGNAL(scheduleEnabled()), this, SLOT(slotStartSchedule()));
	connect(this, SIGNAL(stateUpdated()), this, SLOT(updateState()));
}

void Scheler::init()
{
	uint64_t timeval = Hal_getTimeInMs();

	// ****   transfer initial values for FSCH1  *****//
	bool ok = soefler->lock(lnode);
	if( !ok ){
		return;
	}
	soefler->update("SchdSt", getState(), timeval);

	soefler->write("NumEntr.setVal", 5);

	soefler->write("SchdIntv.setVal", 900); /* 15 min */
	soefler->write("SchdIntv.units.SIUnit", 4); /* second */

	/*	MmsValue* valASG_val = IedServer_getAttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_ValASG1_setMag);
		for( int i=0; i<schedule->numberEntries; i++ ){
			qDebug() << "add value" << (int)valASG[i];
			MmsValue* entry = MmsValue_getElement(valASG_val, i);
			if( entry!= NULL ){
				MmsValue_setFloat(entry, valASG[i]);
			} else {
				entry = MmsValue_newFloat(valASG[i]);
				MmsValue_setElement(valASG_val, i, entry);
			}
		}*/

//	timeString = schedule->startTime;
//	quint32 hr, mn, day;
//	QString month, weekday;
//	hr = timeString.mid(0,2).toInt(); // QString to quint8, works fine
//	mn = timeString.mid(3,2).toInt();
//	day = timeString.mid(7,2).toInt();
//	month = timeString.mid(10,2);
//	year = timeString.mid(13,4);
//	weekday = timeString.right(19);

//	IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_StrTm_setCal_hr, hr); // quint8 -> updateInt32, doesn't seem to work
//	IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_StrTm_setCal_mn, mn);
//	IedServer_updateInt32AttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_StrTm_setCal_day, day);
//	IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_StrTm_setCal_month, month.toLatin1().data());
//	IedServer_updateVisibleStringAttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_StrTm_setCal_weekDay, weekday.toLatin1().data());

	soefler->write("IntvPer.setVal", 1440);
	soefler->write("IntvTyp.setVal", 2);
	//	IedServer_updateQuality(iedServer, IEDMODEL_LD1_Normal_FSCH1_IntvTyp_q, value.isValid()? QUALITY_VALIDITY_GOOD: QUALITY_VALIDITY_INVALID);
	//	IedServer_updateUTCTimeAttributeValue(iedServer, IEDMODEL_LD1_Normal_FSCH1_IntvTyp_t, timeval);

	soefler->unlock();

	enaReq.init();
	dsaReq.init();

	/* theese attributes are independend of currentState */
	soefler->addReader("EnaReq.ctlModel", 0, 5, lnode);
	soefler->addReader("DsaReq.ctlModel", 0, 5, lnode);

	/* the following attributes are NOT available during state Ready or Running */
	soefler->addReader("NumEntr.setVal", new SchelReader(this, MMS_INTEGER, 1, 100), lnode);
	soefler->addReader("SchdIntv.setVal", new SchelReader(this, MMS_INTEGER, 0, 3600), lnode);
	soefler->addReader("SchdIntv.units.SIUnit", new SchelReader(this, MMS_INTEGER, 4, 4), lnode);
	soefler->addReader("SchdReuse.setVal", new SchelReader(this, MMS_BOOLEAN), lnode);
	SchelReader* valAsgChecker = new SchelReader(this, MMS_FLOAT, -32000, 32000);
	soefler->addReaders("ValASG", 4, "setMag.f", valAsgChecker, lnode);

	/* the following attributes are not used and therefore state check only */
	SchelReader* stateChecker = new SchelReader(this, NO_TYPE_CHECK);

	soefler->addReader("StrTm.setCal.occ", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.occType", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.occPer", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.weekDay", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.month", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.day", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.hr", stateChecker, lnode);
	soefler->addReader("StrTm.setCal.mn", stateChecker, lnode);

	/* ignore IntvPer and IntvTyp as they where removed in 61850-90-10 edition 1.0 */
	soefler->addReader("IntvPer.setVal", stateChecker, lnode);
	//soefler->addReader("IntvPer.units.SIUnit", new SchelReader(this, MMS_INTEGER, 4, 4), lnode);
	soefler->addReader("IntvTyp.setVal", stateChecker, lnode);

	SoefReader* reader = soefler->addReader("ZReq.setVal", NO_MIN, NO_MAX, lnode);
	connect(reader, SIGNAL(updated(QVariant)), this, SLOT(updateZRequest(QVariant)));
}

Scheler *Scheler::getScheler(QString ln)
{
	if( ! schelerInstances.contains(ln) ){
		qDebug() << "no schedule found at" << ln;
		return NULL;
	}
	return schelerInstances[ln];
}

//todo: rework it according to writeHandler
CheckHandlerResult Scheler::checkHandler(void* parameter, MmsValue* ctlVal, bool test, bool interlockCheck)
{
	Q_UNUSED(test);

	if( ctlVal == NULL || MmsValue_getType(ctlVal) != MMS_BOOLEAN ){
		return CONTROL_OBJECT_ACCESS_DENIED;
	}
	if( MmsValue_getBoolean(ctlVal) == false ){
		return CONTROL_OBJECT_ACCESS_DENIED;
	}
	qDebug() << "scheler.checkHandler called with value == true";

	if( parameter == IEDMODEL_CLS_SCHD_FSCH1_EnaReq ){
		if( enableSchedule(true) ){
			return CONTROL_ACCEPTED;
		}
		return CONTROL_OBJECT_ACCESS_DENIED;
	}
	if( parameter == IEDMODEL_CLS_SCHD_FSCH1_DsaReq ){
		if( disableSchedule(true) ){
			return CONTROL_ACCEPTED;
		}
		return CONTROL_OBJECT_ACCESS_DENIED;
	}
	return CONTROL_OBJECT_UNDEFINED;
}

ControlHandlerResult Scheler::controlHandler(void* parameter, MmsValue* value, bool test)
{
	if( value == NULL || MmsValue_getType(value) != MMS_BOOLEAN ){
		return CONTROL_RESULT_FAILED;
	}
	if( MmsValue_getBoolean(value) == false ){
		return CONTROL_RESULT_FAILED;
	}

	if( parameter == IEDMODEL_CLS_SCHD_FSCH1_EnaReq ){
		if( enableSchedule(test) ){
			enaReq.trigger();
			return CONTROL_RESULT_OK;
		}
		return CONTROL_RESULT_FAILED;
	}
	if( parameter == IEDMODEL_CLS_SCHD_FSCH1_DsaReq ){
		if( disableSchedule(test) ){
			dsaReq.trigger();
			return CONTROL_RESULT_OK;
		}
		return CONTROL_RESULT_FAILED;
	}

	qDebug() << "scheler:controlHandler attribute not found";
	return CONTROL_RESULT_FAILED;
}

void Scheler::setState(SchdState state, bool test)
{
	if( test ){
		return;
	}

	currentState = state;
	qInfo() << "switch schedule state to" << state;
	emit stateUpdated();
}

bool Scheler::enableSchedule(bool test)
{
	QVariant v;
	QList<QVariant> l;

	switch( getState() ){
	case SchdNotReady:
		v = soefler->read("SchdIntv.units.SIUnit", lnode);
		if( !v.isValid() || v.toInt() !=  4 /* seconds */ ){
			setState(SchdNotReady, test);
			return false;
		}

		v = soefler->read("SchdIntv.setVal", lnode);
		if( !v.isValid() || v.toInt() <= 0 || v.toInt() > 86400 /*1 day*/ ){
			setState(SchdNotReady, test);
			return false;
		}
		intv = v.toInt() * 1000ll;

		v = soefler->read("NumEntr.setVal", lnode);
		if( !v.isValid() || v.toInt() < 1 || 100 < v.toInt() ){
			setState(SchdNotReady, test);
			return false;
		}
		entries = v.toUInt();
		values.clear();
		v = soefler->readList("ValASG", 4, "setMag.f", entries, lnode);
		if( v.type() != QVariant::List ){
			return false;
		}

		l = v.toList();
		for( quint32 i=0; i<entries; i++ ){
			v = l[i];
			if( !v.isValid() || v.toFloat() < -32000 || 32000 < v.toFloat() ){
				setState(SchdNotReady, test);
				return false;
			}
			values.append(v.toFloat());
		}

		/* todo: some device specific tests*/

		//unit = soefler->read("ValASG.units.SIUnit", lnode);

		/* TODO: check that either start time (StrTm) or external trigger (EvTrg) is set */
		setState(SchdReady, test);
		if( test == false ){
			setState(SchdRunning, false);
			emit scheduleEnabled();
		}
		return true;
	default:
		/* result is false and state remains unchanged by default */
		return false;
	}
}

bool Scheler::disableSchedule(bool test)
{
	switch( getState() ){
	case SchdReady:
	case SchdRunning:
	case SchdNotRunning:
	case SchdNotReady:
		setState(SchdNotReady, test);
		if( test == false ){
			togtimer.stop();
		}
		return true;
	default:
		return false;
	}
}

void Scheler::slotStartSchedule()
{
	if( currentState != SchdRunning || intv == 0 ){
		qDebug() << "starting schedule aborted due to wrong state";
		return;
	}

	qint64 now = QDateTime::currentMSecsSinceEpoch();
	if( now < MIN_DATE ){
		QTimer::singleShot(15000, this, SLOT(slotStartSchedule()));
	}

	qint64 today = QDateTime(QDate::currentDate()).toMSecsSinceEpoch() * 1000ll;

	index = (now - today + intv) / intv;
	index %= entries;
	qint64 remaining = intv - ((now - today) % intv);
	/* todo: avoid switching within 1 s */

	togtimer.setInterval(intv);
	QTimer::singleShot(remaining, this, SLOT(slotToggleSchedule()));
	slotToggleSchedule();
	/* stop to synchronise with time period */
	togtimer.stop();
}

void Scheler::slotToggleSchedule()
{
	if( !togtimer.isActive() ){
		togtimer.start();
	}
	QVariant newValue = values[index];

	qDebug() << "toggle schedule values[" << index << "] = " << newValue;

	soefler->update("ValMV", newValue, CURRENT_TIME, QUALITY_VALIDITY_GOOD,lnode);

	emit toggle(MMS_FLOAT, newValue);

	index++;
	if( index >= entries && false ){
		togtimer.stop();
	}
	index %= entries;
}

void Scheler::updateState()
{
	uint64_t timeval = Hal_getTimeInMs();
	soefler->update("SchdSt", currentState, timeval, QUALITY_VALIDITY_GOOD, lnode);
	soefler->update("Beh", Beh_on, timeval, QUALITY_VALIDITY_GOOD, lnode);
	soefler->write("ZReq.setVal", currentState == SchdRunning, lnode);
}

void Scheler::updateZRequest(QVariant value)
{
	if( value.type() != QVariant::Bool ){
		qDebug() << "invalid type for ZReq";
		return;
	}
	if( value.toBool() ){
		enableSchedule(false);
	} else {
		disableSchedule(false);
	}
}

ControlObject::ControlObject(Soefler* parent, const char* object, QString ln) : QObject(parent)
{
	soefler = parent;
	obName = object;
	lnode = ln;
}

void ControlObject::init()
{
	soefler->writeNode(soefler->getNode(obName, "ctlModel", lnode), 1);
	soefler->update("Beh", Beh_on, CURRENT_TIME, QUALITY_VALIDITY_GOOD, lnode);
}

void ControlObject::trigger()
{
	/* everything done by libiec ? */

	//soefler->lock((LogicalNode*)mnode);
	//TODO: incorrect use of object and suffix
	//qDebug() << ds << "trigger start";
	//soefler->set((DataObject*)mnode, "stVal", QVariant::Bool, true);
	//soefler->set((DataObject*)mnode, "origin.orCat", QVariant::Int, soefler->get(mnode, QVariant::Int, "Oper.origin.orCat"));
	//soefler->set((DataObject*)mnode, "origin.orIdent", QVariant::String, soefler->get(mnode, QVariant::String, "Oper.origin.orIdent"));
	//soefler->set((DataObject*)mnode, "ctlNum", QVariant::Int, soefler->get(mnode, QVariant::Int, "Oper.ctlNum"));
	//qDebug() << ds << "trigger end";
	//soefler->unlock();
}

SchelReader::SchelReader(Scheler *parent, MmsType type, QVariant min, QVariant max) : SoefReader((QObject*)parent, type, min, max)
{
	scheler = parent;
}

MmsDataAccessError SchelReader::handleWrite(MmsType type, QVariant value)
{
	if( scheler->getState() == SchdReady || scheler->getState() == SchdRunning ){
		return DATA_ACCESS_ERROR_TEMPORARILY_UNAVAILABLE;
	}
	if( type == NO_TYPE_CHECK ){
		return DATA_ACCESS_ERROR_SUCCESS;
	}
	return SoefReader::handleWrite(type, value);
}

SchedController::SchedController(Soefler* parent, QString ln) : QObject(parent)
{
	soefler = parent;
	lnode = ln;
	schedule = NULL;
	entity = NULL;
}

void SchedController::init(QString cSchedule, QString controlledEntity)
{
	soefler->lock(lnode);

	SoefReader* schdReader = new SchedElementReader(soefler);
	soefler->addReader("Schd01.setSrcRef", schdReader);
	connect(schdReader, SIGNAL(updated(QVariant)), this, SLOT(updateSched(QVariant)));
	if( schdReader->handleWrite(MMS_VISIBLE_STRING, cSchedule) == DATA_ACCESS_ERROR_SUCCESS ){
		soefler->write("Schd01.setSrcRef", cSchedule);
		soefler->write("ActSchdRef.srcRef", cSchedule);
	}

	SoefReader* entReader = new ElementReader(soefler);
	soefler->addReader("CtlEnt.setSrcRef", entReader);
	connect(entReader, SIGNAL(updated(QVariant)), this, SLOT(updateEntity(QVariant)));
	if( entReader->handleWrite(MMS_VISIBLE_STRING, controlledEntity) == DATA_ACCESS_ERROR_SUCCESS ){
		soefler->write("CtlEnt.setSrcRef", controlledEntity);
	}

	soefler->update("Beh", Beh_on);
	soefler->unlock();
}

void SchedController::updateSched(QVariant schd)
{
	if( schedule!= NULL && entity!= NULL ){
		disconnect(schedule, SIGNAL(toggle(MmsType, QVariant)), this, SLOT(toggleValue(MmsType, QVariant)));
		soefler->write("ActSchdRef.srcRef", "", lnode);
	}

	if( schd.type() != QVariant::String ){
		return;
	}
	schedule = Scheler::getScheler(schd.toString());

	if( schedule!= NULL && entity!= NULL ){
		connect(schedule, SIGNAL(toggle(MmsType, QVariant)), this, SLOT(toggleValue(MmsType, QVariant)));
		soefler->write("ActSchdRef.srcRef", schd, lnode);
	}
}

void SchedController::updateEntity(QVariant ent)
{
	if( schedule!= NULL && entity!= NULL ){
		disconnect(schedule, SIGNAL(toggle(MmsType, QVariant)), this, SLOT(toggleValue(MmsType, QVariant)));
	}

	if( ent.type() != QVariant::String ){
		return;
	}
	entity = soefler->getReader(ent.toString());

	if( schedule!= NULL && entity!= NULL ){
		connect(schedule, SIGNAL(toggle(MmsType, QVariant)), this, SLOT(toggleValue(MmsType, QVariant)));
	}
}

void SchedController::toggleValue(MmsType type, QVariant value)
{
	emit entity->handleWrite(type, value);

	qDebug() << "toggle Value" << value;
	soefler->lock(lnode);
	soefler->update("ValMV", value);
	ModelNode* node = entity->getNode();
	if( node != NULL ){
		soefler->writeNode(node, value);
	}
	soefler->unlock();
}

SchedElementReader::SchedElementReader(Soefler *parent) : SoefReader(parent, MMS_VISIBLE_STRING, NO_MIN, NO_MAX)
{}

MmsDataAccessError SchedElementReader::handleWrite(MmsType type, QVariant value)
{
	if( value.type() != QVariant::String ){
		return DATA_ACCESS_ERROR_TYPE_UNSUPPORTED;
	}
	QString str = value.toString();
	if( str.length() != 0 && Scheler::getScheler(str) == NULL ){
		return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
	}

	return SoefReader::handleWrite(type, value);
}

ElementReader::ElementReader(Soefler *parent) : SoefReader(parent, MMS_VISIBLE_STRING, NO_MIN, NO_MAX)
{
	soefler = parent;
}

MmsDataAccessError ElementReader::handleWrite(MmsType type, QVariant value)
{
	if( value.type() != QVariant::String ){
		return DATA_ACCESS_ERROR_TYPE_UNSUPPORTED;
	}
	QString str = value.toString();
	if( str.length() != 0 && soefler->getReader(str) == NULL ){
		qDebug() << "list of schedulable entries:" << soefler->listReaders();
		return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
	}

	return SoefReader::handleWrite(type, value);
}


