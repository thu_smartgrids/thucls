/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef CLSPROFILE_H
#define CLSPROFILE_H

#include <QObject>
#include <QVariant>
#include "iec61850_common.h"
#include "soefler.h"
#include "../genio.h"
#include "../modsun/modler.h"
#include "scheler.h"

/**
 * IEC 61850 Layer (Sixty One Eight Fifty LayER).
 * Based on libiec61850 and utilizes a static model. Realised as singleton.
 */
class ClsProfile : public QObject
{
	Q_OBJECT
protected:
	explicit ClsProfile(QObject *parent = 0);
public:
	static ClsProfile* getInstance(QObject *parent = 0);
	~ClsProfile();

	Soefler* getSoefler() { return &soefler; }

	void start();
	void init(QString name, QString info);
	void initDevice(QVariant vendor, QVariant swRev, QVariant d, qint32 ldMode, bool metersPCC = false);
	void initMeter(QVariant vendor, QVariant swRev, QVariant d);
	void initPV(Modler* source);
	bool initSLog(GenIo* source);
	bool initCharger(Modler* source);
	bool initBat(GenIo* source);
	bool initHeat(GenIo* source);
	bool initJanitzaMeter(GenIo* source);
	bool initScheler(Scheler *source);
	void updateDevice(GenIo *device, io_ConnState health=io_Unknwn, QString healthmsg=NULL);
	void updateMeter(float power, qint32 quality);
	void updatePV(Modler* source);
	void updateSLog(GenIo* source);
	void updateCharger(Modler* source);
	void updateBat(GenIo* source);
	void updateHeat(GenIo* source);
	void updateJanitzaMeter(GenIo* source);
	void updateScheler(GenIo* source);
	void updateSchdVal(quint32 slot, QVariant value);

	void setAgent(qint32 agent);

	void initMeterAt2(QVariant vendor, QVariant swRev, QVariant d);
	void updateJanitzaMeterAt2(GenIo* source);
signals:
	void signalSetAgent(qint32 agent);
	void signalSetMinW(QVariant value);
	void signalSetMaxW(QVariant value);
	void signalSetPercent(QVariant value);
	void signalSetBatPower(qint32 pow);
	void signalSetBatOpMod(QVariant value);
	void signalSetBatMaxSOC(QVariant value);
	void signalSetBatMinSOC(QVariant value);
	void signalSetChaMaxI(QVariant value);
	void signalSetHeatPower(QVariant value);
	void signalSetHomePower(QVariant value);
	void signalSetHeatOpMod(QVariant value);
	void signalSetHeatMaxTmp(QVariant value);
	void signalSetHeatMinTmp(QVariant value);
	void signalSetHeatSetTmp(QVariant value);

public slots:
	void updateHealth();

protected:
	Soefler soefler;
	Scheler scheler;
	qint32 pthAgent;
protected slots:
	void setBatPower(QVariant value);
	void setLocAgent(QVariant value);
	void slotSetChaPwrLim(QVariant value);
};

#endif // CLSPROFILE_H
