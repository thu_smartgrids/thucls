/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "soefler.h"
#include "soefler.h"

#include <math.h>
#include "iec61850_server.h"
#include "mms_common.h"
#include "static_model.h"
#include "targetlayer.h"
#include <QDebug>
#include <QMap>
#include <QThread>

static Soefler* todoRemove = NULL;
//static Scheler* staticScheler = NULL;

static char objectRef[255];

static QVariant Mms2Qv(MmsValue* value)
{
	MmsType type = MmsValue_getType(value);
	switch( type ){
	case MMS_BOOLEAN:
		return MmsValue_getBoolean(value);
	case MMS_INTEGER:
		return MmsValue_toInt32(value);
	case MMS_UNSIGNED:
		return MmsValue_toUint32(value);
	case MMS_FLOAT:
		return MmsValue_toFloat(value);
	case MMS_VISIBLE_STRING:
		return MmsValue_toString(value);
	case MMS_UTC_TIME:
		return MmsValue_toUnixTimestamp(value); //todo: make QVariant of type time
	default:
		qDebug() << "incompatible type" << type;
		return QVariant(QVariant::Invalid);

	}
}

static bool Qv2Mms(MmsValue* result, QVariant value)
{
	MmsType type = MmsValue_getType(result);
	switch( type ){
	case MMS_BOOLEAN:
		MmsValue_setBoolean(result, value.toBool());
		break;
	case MMS_INTEGER:
		MmsValue_setInt32(result, value.toInt());
		break;
	case MMS_UNSIGNED:
		MmsValue_setUint32(result, value.toUInt());
		break;
	case MMS_FLOAT:
		MmsValue_setFloat(result, value.toFloat());
		break;
	case MMS_VISIBLE_STRING:
		MmsValue_setVisibleString(result, value.toString().toLatin1().data());
		break;
	case MMS_UTC_TIME:
		MmsValue_setUtcTimeMs(result, value.toLongLong());
		break;
	default:
		qDebug() << "incompatible type" << type;
		return false;
	}
	return true;
}

QVector<SoefReader*> SoefReader::readers;

MmsDataAccessError SoefReader::handleWriteAccess (DataAttribute* dataAttribute, MmsValue* value, ClientConnection connection, void* parameter)
{
	Q_UNUSED(connection)

	if( dataAttribute == NULL || value == NULL || parameter == NULL ){
		return DATA_ACCESS_ERROR_OBJECT_UNDEFINED;
	}

	qDebug() << "write request for 61850 param" << ModelNode_getObjectReference((ModelNode*) dataAttribute, objectRef);

	SoefReader* reader = (SoefReader*)parameter;
	if( ! SoefReader::readers.contains(reader) ){
		qDebug() << "unknown reader for 61850 parameter";
		return DATA_ACCESS_ERROR_OBJECT_ACCESS_UNSUPPORTED;
	}

	return reader->handleWrite(MmsValue_getType(dataAttribute->mmsValue), Mms2Qv(value));
}

const QVariant Soefler::noCheck(QVariant::Invalid);

Soefler::Soefler(QObject* parent) : GenIo(parent, false)
{
	static int instances = 0;
	++instances;
	tcpPort = IEC_PORT;
	currentNode = NULL;
	lockedBy = NULL;
	qDebug() << "create ied server (libiec version is" << LibIEC61850_getVersionString() << ")";
	//qDebug() << "trying to create ied server" << instances;

	//IedServerConfig config = IedServerConfig_create();

	//static QString name = "iGrids module " + shortName.remove(0, shortName.lastIndexOf(':'));
	//static QString mac = TargetLayer::getMac();
	//static const char* test = "this is a test  ";
	//IedModel_setIedName(&iedModel, test);

	iedServer = IedServer_create(&iedModel);

	/* Don't allow access to SP variables by default */
	IedServer_setWriteAccessPolicy(iedServer, IEC61850_FC_SP, ACCESS_POLICY_DENY);

	//IedServer_setWriteAccessPolicy(iedServer, IEC61850_FC_CF, ACCESS_POLICY_ALLOW);
	//IedServer_setWriteAccessPolicy(iedServer, IEC61850_FC_CO, ACCESS_POLICY_ALLOW);

	/* Instruct the server that we will be informed if a clients writes to a
	 * certain variables we are interested in.
	 */

	todoRemove = this;
}

Soefler::~Soefler()
{
	if( iedServer == NULL )  {
		return;
	}
	qDebug() << "stopping iedServer";
	/* stop MMS server - close TCP server socket and all client sockets */
	IedServer_stop(iedServer);

	/* Cleanup - free all resources */
	IedServer_destroy(iedServer);
}

void Soefler::start()
{
	currentNode = NULL;

	if( iedServer == NULL ){
		return;
	}
	if( !IedServer_isRunning(iedServer) ){
		qDebug() << "starting ied server";
		if( tcpPort != 102 ){
			qDebug() << "using port " << tcpPort;
		}
		IedServer_start(iedServer, tcpPort);
	}

	if (!IedServer_isRunning(iedServer)) {
		qDebug() << "Starting ied server at port " << tcpPort << " failed!";
		IedServer_destroy(iedServer);
		iedServer = NULL;
	}
}

void Soefler::addBlock(QString block, LogicalNode* lnode)
{
	blocks[block] = lnode;
	currentNode = (ModelNode*)lnode;
}

SoefReader* Soefler::addReader(QString element, QVariant min, QVariant max, QString block)
{
	ModelNode* node = getNode(element.toLatin1().constData(), NULL, block);
	MmsType type = getType(node);
	SoefReader* reader = new SoefReader(this, type, min, max);

	bool okay = addReader(element, reader, block);

	if( ! okay ){
		return NULL;
	}

	return reader;
}

bool Soefler::addReader(QString element, SoefReader *reader, QString block)
{
	ModelNode* node = getNode(element.toLatin1().constData(), NULL, block);

	return addReaderForNode(node, reader);
}

SoefReader* Soefler::getReader(QString element)
{
	if( !readerMap.contains(element) ){
		return NULL;
	}

	return readerMap[element];
}

QString Soefler::listReaders()
{
	return readerMap.keys().join(' ');
}

bool Soefler::readBlock(QString block)
{
	return blocks.contains(block);
}

QVariant Soefler::read(QString element, QString block)
{
	ModelNode* node = getNode(element.toLatin1().constData(), NULL, block);
	return readNode(node);
}

bool Soefler::write(QString element, QVariant value, QString block)
{
	ModelNode* node = getNode(element.toLatin1().constData(), NULL, block);
	qint32 result = writeNode(node, value);
	return result == QUALITY_VALIDITY_GOOD;
}

bool Soefler::update(QString element, QVariant value, uint64_t time, qint32 qualityGood, QString block)
{
	ModelNode* node = getNode(element.toLatin1().constData(), NULL, block);
	return updateNode(node, NULL, value, time, qualityGood);
}

bool Soefler::updateBeh(qint32 ldMode, qint32 lnMode, uint64_t time, qint32 qualityGood, QString block)
{
	qint32 lnBeh = std::max(ldMode, lnMode);
	return update("Beh", lnBeh, time, qualityGood, block);
}

bool Soefler::addReaders(QString element, quint32 digits, QString suffix, SoefReader *reader, QString block)
{
	ModelNode* node;
//	digits = std::max(digits, 10);
	bool result = true;

	for( qint32 i = 0;; i++ ){
		QString eli = element + QString::number(i+1).rightJustified(digits, '0');
		node = getNode(eli.toLatin1().constData(), suffix.toLatin1().constData(), block, false);
		if( node == NULL ){
			break;
		}
		result &= addReaderForNode(node, reader, false);
	}
	return result;
}

QVariant Soefler::readList(QString element, quint32 digits, QString suffix, quint32 count, QString block)
{
	ModelNode* node;
//	digits = std::max(digits, 10);
	QVariantList result;

	for( quint32 i = 0; i<count; i++ ){
		QString eli = element + QString::number(i+1).rightJustified(digits, '0');
		node = getNode(eli.toLatin1().constData(), suffix.toLatin1().constData(), block);
		if( node != NULL ){
			result.append(readNode(node));
		}
	}
	return result;
}

bool Soefler::writeList(QString element, quint32 digits, QString suffix, QVariantList values, QString block)
{
	ModelNode* node;
//	digits = std::max(digits, 10);
	bool result = values.length() > 0;

	for( qint32 i = 0; i<values.length(); i++ ){
		QString eli = element + QString::number(i+1).rightJustified(digits, '0');
		node = getNode(eli.toLatin1().constData(), suffix.toLatin1().constData(), block);
		if( node != NULL ){
			result &= writeNode(node, values[i]);
		} else {
			result = false;
		}
	}
	return result;
}

bool Soefler::writeElement(QString element, quint32 idx, quint32 digits, QString suffix, QVariant value, QString block)
{
	ModelNode* node;
//	digits = std::max(digits, 10);
	QString eli = element + QString::number(idx+1).rightJustified(digits, '0');
	node = getNode(eli.toLatin1().constData(), suffix.toLatin1().constData(), block);
	if( node != NULL ){
		return writeNode(node, value);
	} else {
		return false;
	}
}

ModelNode* Soefler::getNode(const char* object, const char* suffix, QString block, bool dbg)
{
	ModelNode* node = NULL;
	if( block == NULL || block == "" ){
		node = currentNode;
	} else if( blocks.contains(block) ){
		node = (ModelNode*)blocks[block];
	}

	if( node == NULL ){
		if( dbg ){
			qDebug() << "61850 node not found for package" << block;
		}
		return NULL;
	}

	if( object == NULL || *object == '\0' ){
		if( dbg ){
			qDebug() << "invalid address to 61850 data object given";
		}
		return NULL;
	}

	node = ModelNode_getChild(node, object);

	if( node == NULL ){
		if( dbg ){
			qDebug() << "did not found 61850 data object" << object;
		}
		return NULL;
	}

	if( suffix != NULL && *suffix != '\0' ){
		node = ModelNode_getChild(node, suffix);

		if( node == NULL ){
			if( dbg ){
				qDebug() << "did not found 61850 data object" << object << "suffix" << suffix;
			}
			return NULL;
		}
	}

	return node;
}

ModelNode* Soefler::getNode(ModelNode* object, const char* suffix)
{
	if( suffix != NULL && *suffix != '\0' ){
		object = ModelNode_getChild(object, suffix);

		if( object == NULL ){
			qDebug() << "did not found 61850 data object" << object << "suffix" << suffix;
			return NULL;
		}
	}

	return object;
}

MmsType Soefler::getType(ModelNode* node)
{
	DataAttribute* attr = (DataAttribute*)node;
	if( attr == NULL || attr->mmsValue == NULL )
		return MMS_DATA_ACCESS_ERROR;
	else
		return MmsValue_getType(attr->mmsValue);
}

DataAttribute* Soefler::getFirstAttr(ModelNode* node)
{
	if( node == NULL ){
		return NULL;
	}

	while( node->firstChild != NULL ){
		node = node->firstChild;
	}
	if( ModelNode_getType(node) != DataAttributeModelType ){
		return NULL;
	}
	return (DataAttribute*) node;
}

bool Soefler::addReaderForNode(ModelNode *node, SoefReader *reader, bool schedulable)
{
	if( node == NULL ){
		return false;
	}

	MmsType type = getType(node);
	DataAttribute* attr = getFirstAttr(node);

	if( attr == NULL ){
		qDebug() << "element not found for reading:" << ModelNode_getObjectReference(node, objectRef);
		return false;
	}

	if( type == MMS_ARRAY ){
		qDebug() << "reading of arrays not supported yet";
	}

	IedServer_handleWriteAccess(iedServer, attr, &SoefReader::handleWriteAccess, reader);

	if( schedulable ){
		const char* id = strchr(ModelNode_getObjectReference(node, objectRef), '/');
		if( id != NULL && *id != '\0' ){
			reader->setNode(node);
			readerMap.insert(id + 1, reader);
			//qDebug() << "add reader for" << id+1;
		}
	}
	return true;
}

QVariant Soefler::readNode(ModelNode* node)
{
	if( node == NULL || ModelNode_getType(node) != DataAttributeModelType ){
		qDebug() << "61850 data attribute not found";
		return QVariant(QVariant::Invalid);
	}
	/*if( !SoefReader::isEnabled() ){
		return QVariant(QVariant::Invalid);
	}*/
	DataAttribute* attr = (DataAttribute*)node;
	MmsType type = getType(node);

	switch( type ){
	case MMS_BOOLEAN:
		return IedServer_getBooleanAttributeValue(iedServer, attr);
	case MMS_INTEGER:
	case MMS_UNSIGNED:
		return IedServer_getInt32AttributeValue(iedServer, attr);
	case MMS_FLOAT:
		return IedServer_getFloatAttributeValue(iedServer, attr);
	case MMS_VISIBLE_STRING:
		return IedServer_getStringAttributeValue(iedServer, attr);
	case MMS_UTC_TIME:
		return (qint64) IedServer_getUTCTimeAttributeValue(iedServer, attr);
	case MMS_ARRAY:
	{
		QList<QVariant> out;
		MmsValue* currentValue;
		for(int i = 0; i<attr->elementCount; i++ ){
			currentValue = MmsValue_getElement(attr->mmsValue, i);
			while( MmsValue_getType(currentValue) == MMS_STRUCTURE ){
				currentValue =  MmsValue_getElement(currentValue, 0);
			}
			out.append(Mms2Qv(currentValue));
		}
		return out;
	}
	default:
		qDebug() << "incompatible type for" << ModelNode_getObjectReference(node, NULL);
		return QVariant(QVariant::Invalid);
	}
}

qint32 Soefler::writeNode(ModelNode* node, QVariant value)
{
	if( node != NULL && ModelNode_getType(node) == DataAttributeModelType ){
		DataAttribute* attr = (DataAttribute*)node;
		MmsType type = getType(node);

		switch( type ){
		case MMS_BOOLEAN:
			IedServer_updateBooleanAttributeValue(iedServer, attr, value.toBool());
			break;
		case MMS_INTEGER:
		case MMS_UNSIGNED:
			IedServer_updateInt32AttributeValue(iedServer, attr, value.toInt());
			break;
		case MMS_FLOAT:
			IedServer_updateFloatAttributeValue(iedServer, attr, value.toFloat());
			break;
		case MMS_VISIBLE_STRING:
			IedServer_updateVisibleStringAttributeValue(iedServer, attr, value.toString().toLatin1().data());
			break;
		case MMS_UTC_TIME:
			IedServer_updateUTCTimeAttributeValue(iedServer, attr, value.toLongLong());
			break;
		case MMS_ARRAY:
		{
			QList<QVariant> in = value.toList();
			MmsValue* currentValue;
			qint32 len = std::min(attr->elementCount, in.length());
			for(int i = 0; i<len; i++ ){
				currentValue = MmsValue_getElement(attr->mmsValue, i);
				while( MmsValue_getType(currentValue) == MMS_STRUCTURE ){
					currentValue =  MmsValue_getElement(currentValue, 0);
				}
				Qv2Mms(currentValue, in[i]);
			}
			break;
		}
		default:
			qDebug() << "incompatible type for" << ModelNode_getObjectReference(node, objectRef);
			return QUALITY_VALIDITY_INVALID + QUALITY_DETAIL_INCONSISTENT;
		}
	} else {
		qDebug() << "61850 data attribute not found";
		return QUALITY_VALIDITY_INVALID + QUALITY_DETAIL_BAD_REFERENCE;
	}
	return 	QUALITY_VALIDITY_GOOD;
}

bool Soefler::updateNode(ModelNode* node, const char* suffix, QVariant value, uint64_t time, qint32 qualityGood)
{
/*
 f	apc
 f	asg	setMag_f
 int	inc
 int	ing	setVal
 bool	spc
 bool	spg
 enum	enc	stVal
 enum	eng
 enum	ens	stVal
*/
	if( node == NULL ){
		qDebug() << "invalid address to 61850 data object given";
		return false;
	}

	if( time == CURRENT_TIME ){
		time = Hal_getTimeInMs();
	}

	if( value.isValid() ){
		ModelNode* s;

		if( suffix != NULL && *suffix != '\0' ){
			s = ModelNode_getChild(node, suffix);
		} else {
			s = (ModelNode*)node;
			while( s->firstChild != NULL ){
				s = s->firstChild;
			}
		}

		qint32 qSet = writeNode(s, value);
		qualityGood = std::max(qualityGood, qSet);

		ModelNode* q = ModelNode_getChild(node, "q");
		if( q != NULL && ModelNode_getType(q) == DataAttributeModelType ){
			IedServer_updateQuality(iedServer, (DataAttribute*)q, qualityGood);
		}

		ModelNode* t = ModelNode_getChild(node, "t");
		if( t != NULL && ModelNode_getType(t) == DataAttributeModelType ){
			IedServer_updateUTCTimeAttributeValue(iedServer, (DataAttribute*)t, time);
		}
		return qSet == QUALITY_VALIDITY_GOOD;
	} else {
		bool qualityChanged = false;

		ModelNode* q = ModelNode_getChild(node, "q");
		if( q != NULL && ModelNode_getType(q) == DataAttributeModelType ){
			qint32 qVal = IedServer_getInt32AttributeValue(iedServer, (DataAttribute*)q);
			if( qVal != QUALITY_VALIDITY_INVALID ){
				IedServer_updateQuality(iedServer, (DataAttribute*)q, QUALITY_VALIDITY_INVALID);
				qualityChanged = true;
			}
		}

		if( qualityChanged ){
			ModelNode* t = ModelNode_getChild(node, "t");
			if( t != NULL && ModelNode_getType(t) == DataAttributeModelType ){
				IedServer_updateUTCTimeAttributeValue(iedServer, (DataAttribute*)t, time);
			}
		}
		return false;
	}
}

bool Soefler::lock(QString block)
{
	if( lockedBy != NULL ){
		qDebug() << "Soefler already locked";
		return false;
	}
	lockedBy = QThread::currentThread();
	IedServer_lockDataModel(iedServer);

	return select(block);
}

bool Soefler::select(QString block)
{
	/* for debug purposes use: unlock(); lock(block); */
	if( ! blocks.contains(block) ){
		qDebug() << "no logical node assigned to" << block;
		return false;
	}
	if( lockedBy != QThread::currentThread() ){
		qDebug() << "Soefler not locked by this thread";
		return false;
	}

	return selectNode(blocks[block]);
}

bool Soefler::selectNode(LogicalNode* node)
{
	currentNode = (ModelNode*)node;
	return true;
}

void Soefler::unlock()
{
	/* in doubt forces unlocking (to prevent stall) */
	if( lockedBy != NULL ){
		IedServer_unlockDataModel(iedServer);
	}
	lockedBy = NULL;
	currentNode = NULL;
}

SoefReader::SoefReader(QObject* parent, MmsType type, QVariant min, QVariant max) : QObject(parent)
{
	valType = type;
	minVal = min;
	maxVal = max;
	readers.append(this);
}

SoefReader::~SoefReader()
{
	readers.removeAll(this);
}

void SoefReader::setNode(ModelNode* node)
{
	this->node = node;
}

ModelNode* SoefReader::getNode()
{
	return this->node;
}

/*bool SoefReader::isEnabled()
{
	* checks that the Beh of the logical node equals 1 (enabled) *
	return true;
}*/

MmsDataAccessError SoefReader::handleWrite(MmsType type, QVariant value)
{
	bool okay = true;

	if( type!= valType ){
		return DATA_ACCESS_ERROR_TYPE_UNSUPPORTED;
	}

	if( minVal.isValid() ){
		switch( valType ){
		case MMS_BOOLEAN:
			if( value.toBool() < minVal.toBool() ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_INTEGER:
			if( value.toInt(&okay) < minVal.toInt() ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_UNSIGNED:
			if( value.toUInt(&okay) < minVal.toUInt() ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_FLOAT:
			if( value.toDouble(&okay) < minVal.toDouble() ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_UTC_TIME:
			if( value.toLongLong(&okay) < minVal.toLongLong() ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		default:
			qDebug() << "no range check foreseen for type" << valType;
			break;
		}

		if( !okay ){
			return DATA_ACCESS_ERROR_TYPE_UNSUPPORTED;
		}
	}
	if( maxVal.isValid() ){
		switch( valType ){
		case MMS_BOOLEAN:
			if( maxVal.toBool() < value.toBool() ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_INTEGER:
			if( maxVal.toInt() < value.toInt(&okay) ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_UNSIGNED:
			if( maxVal.toUInt() < value.toUInt(&okay) ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_FLOAT:
			if( maxVal.toDouble() < value.toDouble(&okay) ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		case MMS_UTC_TIME:
			if( maxVal.toLongLong() < value.toLongLong(&okay) ){
				return DATA_ACCESS_ERROR_OBJECT_VALUE_INVALID;
			}
			break;
		default:
			qDebug() << "no range check foreseen for type" << valType;
			break;
		}

		if( !okay ){
			return DATA_ACCESS_ERROR_TYPE_UNSUPPORTED;
		}
	}
	// if( isEnabled() )
	emit updated(value);
	return DATA_ACCESS_ERROR_SUCCESS;
}
