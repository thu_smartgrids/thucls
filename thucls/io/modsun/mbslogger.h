#ifndef MBSLOGGER_H
#define MBSLOGGER_H

#include <QObject>
#include "modler.h"

/**
 * Interface to the SolarLog PV data logger
 */

class MbSLogger : public Modler
{
public:
	MbSLogger(QObject* parent);

protected:
	bool initDict();
};

#endif // MBSLOGGER_H
