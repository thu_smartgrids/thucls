/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef MBEL_H
#define MBEL_H
/**
 * \file mbel.h
 * contains elements needed to build a dictionary for Modler class
 */

#include <QString>
#include <QVariant>

typedef enum {
	mb_int16,
	mb_uint16,
	mb_acc16,
	mb_int32,
	mb_uint32,
	mb_acc32,
	mb_int64,
	mb_uint64,
	mb_acc64,

	mb_string,

	mb_float32,
} mb_Type;

typedef enum {
	mb_R,
	mb_W,
	mb_RW
} mb_IoType;

typedef enum {
	mb_Hold,  //holding register
	mb_In,    //input register
	mb_Coil,  //coil
	mb_Disc,  //discrete
} mb_RegType;

class mbConv {
public:
	virtual QVariant in(QString element, QVariant inval) = 0;
	virtual QVariant out(QString element, QVariant outval) = 0;
};

/**
 * Dictionary entry for one Modbus parameter
 */
typedef struct {
	quint16     address;
	quint16     size;
	QString     name;
	mb_Type     type;
	mb_IoType   ioType;
	mb_RegType  rType;
	bool        isMonitored;
	mbConv*     conversion;
} DataPoint;

#endif // MBEL_H
