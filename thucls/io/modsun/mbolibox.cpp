/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "mbolibox.h"
#include <QDateTime>
#include <QDebug>
#include "plugIns/appling.h"

#define MAX_COUNT 180 /* assuming update runs each 5 seconds (12/min * 15min) */

extern "C" {
#include "modbus/modbus-tcp.h"
#include "modbus/modbus.h"
}

MbOliBox::MbOliBox(AppLing* parent) : Modler(parent, false)
{
	slaveId = 1;
	tcpPort = 5020;

	appling = parent;
	access = noCtrl;
	lastDsoLimit = NO_VALUE;
	lastEmLimit = NO_VALUE;

	maxPow = 0;
	sum_powerProduction = 0;
	sum_powerConsumption = 0;
	count = 1;
	firstRun = true;
	startTime = QDateTime::currentMSecsSinceEpoch();
}

MbOliBox* MbOliBox::getOneInstance(AppLing* parent)
{
	static MbOliBox instance(parent);
	static bool first = true;

	if( first ){
		first = false;
		return &instance;
	}
	return NULL;
}

bool MbOliBox::init(QString ip, OliAccessConfig accessConfig)
{
	access = accessConfig;
	return Modler::init(ip);
}

bool is15(qint32 latencyMsecs = 20000){
	return QDateTime::currentMSecsSinceEpoch() % 900000 < latencyMsecs;
}

void MbOliBox::initValues(QVariant maxPower)
{
	maxPow = std::max(maxPower.toInt(), 0);
	write_univ("MaxPower", maxPow, "static");
	write_univ("Access", quint32(access), "static");
	writeBlock("static");
}

void MbOliBox::update(QVariant power, QVariant limit)
{
	//connectMB();
	if( limit.isValid() ){
		write_univ("ConfirmLimit", limit, "power");
	}
	if( power.isValid() ){
		qint32 w = qint32(power.toFloat() +.5);
		QVariant zero(0);
		if( w < 0 ){
			sum_powerProduction -= w;
			write_univ("PowerProduction", -w, "power");
			write_univ("PowerConsumption", 50, "power");
		} else {
			sum_powerConsumption += w;
			write_univ("PowerProduction", zero, "power");
			write_univ("PowerConsumption", 50 + w, "power");
		}
		writeBlock("power");
		readBlock("power");
	}
	if( count >= (180+10) || (count >= 10 && is15()) || (firstRun && is15()) ){
		quint32 production = sum_powerProduction / count / 4;
		quint32 consumption = sum_powerConsumption / count / 4 + 12;
		if( firstRun ){
			qint64 duration = QDateTime::currentMSecsSinceEpoch() - startTime;
			double factor = duration / 900000;
			production *= factor;
			consumption *= factor;
		}
		write_univ("EnergyProduction", QVariant(production), "energy");
		write_univ("EnergyConsumption", QVariant(consumption), "energy");
		writeBlock("energy");
		qDebug() << "send energy values +" << production << "kWh -" << consumption << "kWh";
		sum_powerProduction = 0;
		sum_powerConsumption = 0;
		count = 0;
		firstRun = false;

		write_univ("MaxPower", maxPow, "static");
		write_univ("Access", quint32(access), "static");
		writeBlock("static");
	}
	//disconnectMB();
	count++;
	checkDsoLimit(count % 6 == 3); /* refresh once during 30 seconds */
	checkEmLimit(count % 6 == 4); /* refresh once during 30 seconds */
}

bool MbOliBox::initDict()
{
	static DataPoint oliPoints[] = {
		{10, 1, "PowerConsumption", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{11, 1, "PowerProduction", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{12, 1, "ConfirmLimit", mb_uint16, mb_W, mb_Hold, true, NULL},
		{20, 1, "EnergyConsumption", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{21, 1, "EnergyProduction", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{30, 1, "MaxPower", mb_uint16, mb_W, mb_Hold, true, NULL},
		{31, 1, "Access", mb_uint16, mb_W, mb_Hold, true, NULL},
		{40, 1, "SetDsoLimit", mb_uint16, mb_R, mb_Hold, true, NULL},
		{41, 1, "SetEmLimit", mb_uint16, mb_R, mb_Hold, true, NULL},
		{50, 1, "ApiVersion", mb_uint16, mb_R, mb_Hold, true, NULL},
		{51, 1, "MagicNumber", mb_uint16, mb_R, mb_Hold, true, NULL}//"oliClsApi1.00"
	};
	static quint32 nOliPoints = sizeof oliPoints / sizeof(DataPoint);

	bool okay = initDictionary(oliPoints, nOliPoints);

	addBlock("power", 0, "PowerConsumption", "ConfirmLimit");
	addBlock("energy", 0, "EnergyConsumption", "EnergyProduction");
	addBlock("static", 0, "MaxPower", "Access");

	if( okay ){
		qint32 apiVersion = read("ApiVersion").toInt();
		qint32 magicNumber = read("MagicNumber").toInt();

		if( apiVersion != 100 || magicNumber != 0x2EF5){
			qDebug() << "could not verify API version of Oli Box";
			return false;
		}
	}
	return okay;
}

void MbOliBox::connectMB()
{
	if( mbConn ){
		connectDevice();
	}
}

void MbOliBox::disconnectMB()
{
	if( mbConn ){
		mbEx.lock();
		modbus_close(mbConn);
		mbEx.unlock();
	}
}

void MbOliBox::checkDsoLimit(bool forceWrite)
{
	if( (access & dsoCtrl) != dsoCtrl ){
		/* not active */
		return;
	}

	QVariant limit = read("SetDsoLimit");
	if( !forceWrite && lastDsoLimit == limit ){
		/* nothing to do */
		return;
	}
	lastDsoLimit = limit;

	if( !limit.isValid() || limit.toInt() < 0 || limit.toInt() > 100 ){
		/* nothing to do */
		return;
	}
	quint32 percent = limit.toInt();

	emit appling->slotSetDinoPercent(std::max(1u, percent));
	qInfo() << "Oli sets DSO limit to" << limit << "%";
}

void MbOliBox::checkEmLimit(bool forceWrite)
{
	if( (access & emCtrl) != emCtrl ){
		/* not active */
		return;
	}

	QVariant limit = read("SetEmLimit");
	if( !forceWrite && lastEmLimit == limit ){
		/* nothing to do */
		return;
	}
	lastEmLimit = limit;

	if( !limit.isValid() || limit.toInt() < 0 || limit.toInt() > 100 ){
		/* nothing to do */
		return;
	}
	quint32 percent = limit.toInt();

	emit appling->setEmPercent("oliBox", std::max(1u, percent));
	qInfo() << "Oli sets EM limit to" << percent << "%";
}
