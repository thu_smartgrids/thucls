/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef SUNSEL_H
#define SUNSEL_H

#include <QString>

/** definitions for SunS model Elements */

//TODO: replace "slyr_" by "sunslayer::"
//TODO: rename slyr_Element by SunSel
typedef enum {
/* 16 bit integers */
	slyr_int16,
	slyr_sunssf,
/*u*/	slyr_uint16,
	slyr_acc16,
	slyr_enum16,
	slyr_bitfield16,
	slyr_pad,
/* 32 bit integers */
	slyr_int32,
/*u*/	slyr_uint32,
	slyr_acc32,
	slyr_enum32,
	slyr_bitfield32,
	slyr_ipaddr,
/* 64 bit integers */
	slyr_int64,
	slyr_acc64,
/* 128 bit integers */
	/* ipv6addr behaves like string */
/* String */
	slyr_ipv6addr,
	slyr_string,
/* floating points */
	slyr_float32,
/* scale factors */
	/* sunssf behaves like 16 bit integer */
} slyr_Type;

typedef enum {
	slyr_R,
	slyr_W,
	slyr_RW
} slyr_IOType;

/**
  * Describes one SunSpec model element and is used to form a dictionary.
  */
typedef struct slyr_ElementStruct {
	quint16     model;
	quint16     offset; // corresponds to Block Offset (as in xml)
	quint16     size;
	QString     name;
	slyr_Type   type;
	slyr_IOType ioType;
	bool        isMandatory;
	qint16      sOffset; //-1 corresponds to n/a
	/* these fields are not needed for SunSpec <-> SunSpec conversion
	 * QString  label;
	 * QString  unit;
	 * QString  description;
	 * QString  notes;
	 */
} slyr_Element;



#endif // SUNSEL_H
