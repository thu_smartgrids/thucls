/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "mbsmartheater.h"

MbSmartHeater::MbSmartHeater(QObject *parent) : Modler(parent)
{
	busorder = BigEndian;
	slaveId = 247;
}

bool MbSmartHeater::initDict()
{
    /**
      * used dataPoints of the E.G.O Smart Heater
	  */
	class StaticConv : public mbConv {
	public:
		StaticConv(float factor) { this->factor = factor; }
		QVariant in(QString element, QVariant inval) { return QVariant(inval.toFloat() * factor); }
		QVariant out(QString element, QVariant outval) { return QVariant(outval.toFloat() / factor); }
	private:
		float factor;
	};

	static StaticConv conv0_01(0.01);
	static DataPoint smartHeaterPoints[] = {
		{8192, 1, "ManufacturerID", mb_uint16, mb_R, mb_Hold, true, NULL},
		{8193, 1, "ProductID", mb_uint16, mb_R, mb_Hold, true, NULL},
		{8194, 1, "ProductVersion", mb_uint16, mb_R, mb_Hold, true, NULL},
		{8195, 1, "FirmwareVersion", mb_uint16, mb_R, mb_Hold, true, NULL},
		{8196, 16, "VendorName", mb_string, mb_R, mb_Hold, true, NULL},
		{8212, 16, "ProductName", mb_string, mb_R, mb_Hold, true, NULL},
		{8228, 16, "SerialNumber", mb_string, mb_R, mb_Hold, true, NULL},
		{8244, 2, "ProductionDate", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4096, 1, "Relais[1].ActualPower", mb_uint16, mb_R, mb_Hold, true, NULL},
		{4097, 2, "Relais[1].OperatingSeconds", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4099, 2, "Relais[1].SwitchingCycles", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4101, 1, "Relais[1].MinOnTime", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4102, 1, "Relais[1].MinOffTime", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4128, 1, "Relais[2].ActualPower", mb_uint16, mb_R, mb_Hold, true, NULL},
		{4129, 2, "Relais[2].OperatingSeconds", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4131, 2, "Relais[2].SwitchingCycles", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4133, 1, "Relais[2].MinOnTime", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4134, 1, "Relais[2].MinOffTime", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4160, 1, "Relais[3].ActualPower", mb_uint16, mb_R, mb_Hold, true, NULL},
		{4161, 2, "Relais[3].OperatingSeconds", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4163, 2, "Relais[3].SwitchingCycles", mb_uint32, mb_R, mb_Hold, true, NULL},
		{4165, 1, "Relais[3].MinOnTime", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4166, 1, "Relais[3].MinOffTime", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4612, 1, "RelaisCount", mb_uint16, mb_R, mb_Hold, true, NULL},
		{4613, 1, "ActualTemperaturePCB", mb_int16, mb_R, mb_Hold, true, NULL},
		{4617, 1, "TemperatureMinValue", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4618, 1, "TemperatureMaxValue", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4619, 1, "TemperatureNominalValue", mb_uint16, mb_RW, mb_Hold, true, NULL},
		{4864, 1, "PowerNominalValue", mb_int16, mb_RW, mb_Hold, true, NULL},
		{4865, 2, "HomeTotalPower", mb_int32, mb_RW, mb_Hold, true, NULL},
		{5120, 2, "TotalOperatingSeconds", mb_uint32, mb_R, mb_Hold, true, NULL},
		{5122, 2, "ErrorCounter", mb_uint32, mb_R, mb_Hold, true, NULL},
		{5124, 1, "ActualTemperatureBoiler", mb_int16, mb_R, mb_Hold, true, NULL},
		{5125, 1, "ActualTemperatureExternalSensor1", mb_int16, mb_R, mb_Hold, true, NULL},
		{5126, 1, "ActualTemperatureExternalSensor2", mb_int16, mb_R, mb_Hold, true, NULL},
		{5127, 1, "UserTemperatureNominalValue", mb_int16, mb_R, mb_Hold, true, NULL},
		{5128, 1, "RelaisStatus", mb_uint16, mb_R, mb_Hold, true, NULL},
//		{5800, 4, "IpAddress", mb_u8, mb_RW, mb_Hold, true, NULL},
//		{5804, 4, "GatewayAddress", mb_u8, mb_RW, mb_Hold, true, NULL},
//		{5808, 4, "DNSAddress", mb_u8, mb_RW, mb_Hold, true, NULL},
		{5376, 2, "Error[1].OperatingHour", mb_uint32, mb_R, mb_Hold, true, NULL},
		{5378, 1, "Error[1].OperatingSecond", mb_uint16, mb_R, mb_Hold, true, NULL},
		{5379, 1, "Error[1].ErrorCode", mb_uint16, mb_R, mb_Hold, true, NULL}

		// To do: ErrorCodes, 5376-5414
	};
	static quint32 nSmartHeaterPoints = sizeof smartHeaterPoints / sizeof(DataPoint);


	return initDictionary(smartHeaterPoints, nSmartHeaterPoints);
}

QVariant MbSmartHeater::read_buffer(mb_Type type, Ubuf& inbuf)
{
	qint32 i=0, j=-2;

	if( type == mb_string ){
		do {
			inbuf.w[i] = io_htons(inbuf.w[i], HalfEndian);

			i++;
			j+=2;
		} while(inbuf.s[j] != 0 && inbuf.s[j+1] != 0 && i <= UBUF_MAX_WORDS);
	}
	return Modler::read_buffer(type, inbuf);
}
