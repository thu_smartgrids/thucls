/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef MODLER_H
#define MODLER_H

#include "../genio.h"
#include <QMap>
#include <QMutex>
#include "mbel.h"

#define UBUF_MAX_WORDS 100

/**
  * Helper type to serve as I/O buffer.
  */
typedef union {
//	qint16  i16;
	quint16 u16;
//	qint32  i32;
	quint32 u32;
//	qint64  i64;
	quint32 h64[2];
	float	f32;
	char    s[UBUF_MAX_WORDS * 2];
	quint16 w[UBUF_MAX_WORDS];
} Ubuf;

extern "C" {
typedef struct _modbus modbus_t;
}

/**
 * Helper class to describe blocks of parameters which are transfered at once.
 */
class MBlock
{
public:
	MBlock(QString name, qint64 validity, quint16 start, quint16 len, mb_RegType rType);
	bool read(quint16* inbuf, quint16 addr, quint16 len);
	bool write(quint16 addr, quint16 len, quint16* outbuf);

	QString    blockName;
	qint64     validityPeriod;
	quint16    startAddr;
	quint16    blockLen;
	mb_RegType rType;

	qint64     lastReadTime;
	bool       valid;
	bool       isMonitored;
	Ubuf       data;
};

/**
 * Modbus Handler to access Modbus via TCPv4. Based upon libmodbus.
 * Reads multiple parameters as block. Uses a dictionary to handle the single parameters.
 */
class Modler : public GenIo
{
	Q_OBJECT
public:
	explicit Modler(QObject *parent = 0, bool metering = true);
	~Modler();

	QString getName() { return ip; }

	bool init(QString ip, qint32 deviceId = -1);
	void enterCyclingMode();

	bool addBlock(QString block, qint64 validity, QString start, QString end);
	bool readBlock(QString block);
	QVariant read(QString element, QString block = NULL);
	bool writeBlock(QString block);
	bool write(QString element, QVariant value) { return write_univ(element, value); }
	bool write_univ(QString element, QVariant value, QString block = NULL);

	io_ConnState checkHealth();
	bool reconnect();
	bool recover(quint16 failrate);
signals:

public slots:

protected:
	modbus_t* mbConn = NULL;
	QMutex mbEx; /* Mutex for accesses on mbConn (only) */
	QMap<QString, DataPoint*> dict;
	ByteOrder busorder;
	quint8 slaveId;
	quint16 tcpPort;

	QString ip;
	bool cycling;
	quint32 cmToMillis; /* cycling mode: timeout in ms */

	QMap<QString, MBlock*> blocks;
	QMap<QString, MBlock*> cache; //for each cached element refer its block

	bool initDictionary(DataPoint* dataPoints, quint32 nPoints);

	virtual bool initCon(); /* set modbus parameters, like device ID and timeout */
	virtual bool initDict() = 0; /* initialise the dictionary with device specific data points */

	bool read_registers(quint16* inbuf, qint32 addr, mb_RegType regtype, quint16 size);
	virtual QVariant read_buffer(mb_Type type, Ubuf& inbuf);

	void write_buffer(Ubuf& outbuf, mb_Type type, QVariant value);
	bool write_registers(qint32 addr, mb_RegType regtype, quint16 size, quint16* outbuf);

	bool connectDevice();
};


#endif // MODLER_H
