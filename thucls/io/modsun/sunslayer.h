/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef SUNSLAYER_H
#define SUNSLAYER_H

#include "modler.h"

typedef struct slyr_ElementStruct slyr_Element;

/**
 * SunSpec I/O Layer for communication over TCPv4.
 * Mainly extends the Modler to build up a dictionary during initialisation phase.
 */
class SunSlayer : public Modler
{
	Q_OBJECT
public:
	explicit SunSlayer(QObject *parent = 0);
	bool isModelAvail(quint16 model);

protected:
	QMap<quint16, qint32> addressBook;

	bool initDict();

	bool initAddressBook();

	QVariant read_buffer(mb_Type type, Ubuf& inbuf);
};

#endif // SUNSLAYER_H
