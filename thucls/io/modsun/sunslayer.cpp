/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "sunslayer.h"

#include <QDebug>
#include <cmath>
#include "sunsel.h"
#include "sunsmodls.h"
#include "../genio.h"

extern "C" {
#include "modbus/modbus.h"
#include "errno.h"
}

SunSlayer::SunSlayer(QObject *parent) : Modler(parent)
{
}

bool SunSlayer::initAddressBook()
{
	qint32 base_i = 0;
	qint32 base[] = {40000, 0, 50000};

	quint16 inbuf[4] = {0};

	qint32 addr;
	quint16 id = 0xFFFF;
	quint16 len = 0;

	qint32 ret;

	addressBook.clear();

	mbEx.lock();
	/* search base address */
	do {
		errno = 0;
		ret = modbus_read_registers(mbConn, base[base_i], 4, inbuf);
		if( ret == -1 && errno != EMBXILFUN && errno != EMBXILADD && errno != EMBXILVAL ){
			qDebug() << "MB read error" << modbus_strerror(errno) << "(" << errno << ")";
		}
		errno = 0;

		if (ret == 4 && inbuf[0] == 21365 && inbuf[1] == 28243) {
			addr = base[base_i] + 2;
			id = inbuf[2];
			len = inbuf[3];
			break;
		}

		base_i++;
	} while (base_i < 3);

	/* identify the supported models */
	while( id != 0xFFFF ) {
		addressBook.insert(id, addr);

		if( len & 0x1 ) {
			qDebug() << "block" << id << "has odd length";
		//	len++;
		};
		addr+=len + 2;

		ret = modbus_read_registers(mbConn, addr, 2, inbuf);
		if( ret < 0 ) {
			qDebug() << "modbus error during init of address book";
			break;
		}

		id = inbuf[0];
		len = inbuf[1];
	}
	mbEx.unlock();

	qDebug() << "address book has" << addressBook.size() << "entries:";
	qDebug() << addressBook;

	return true;
}

QVariant SunSlayer::read_buffer(mb_Type type, Ubuf &inbuf)
{
	QVariant result = Modler::read_buffer(type, inbuf);
	switch(type){
	case mb_int16:
		if( result.toInt() == qint16(0x8000u) ){
			return QVariant(QVariant::Invalid);
		}
		break;
	case mb_uint16:
		if( result.toUInt() == 0xFFFFu ){
			return QVariant(QVariant::Invalid);
		}
		break;
	case mb_int32:
		if( result.toInt() == qint32(0x80000000u) ){
			return QVariant(QVariant::Invalid);
		}
		break;
	case mb_uint32:
		if( result.toUInt() == 0xFFFFFFFFu ){
			return QVariant(QVariant::Invalid);
		}
		break;
	case mb_int64:
		if( result.toLongLong() == qint64(0x8000000000000000u) ){
			return QVariant(QVariant::Invalid);
		}
		break;
	case mb_uint64:
		if( result.toULongLong() == 0xFFFFFFFFFFFFFFFFu ){
			return QVariant(QVariant::Invalid);
		}
		break;
	case mb_acc16:
	case mb_acc32:
	case mb_acc64:
		if( result.toULongLong() == 0 ){
			return QVariant(QVariant::Invalid);
		}
		break;
	default:
		break;
	}

	return result;
}

mb_Type castSlyrType(slyr_Type type)
{
	switch (type)
	{
	case slyr_int16:
	case slyr_sunssf:
		return mb_int16;
	case slyr_uint16:
	case slyr_enum16:
	case slyr_bitfield16:
	case slyr_pad:
		return mb_uint16;
	case slyr_acc16:
		return mb_acc16;
	case slyr_int32:
		return mb_int32;
	case slyr_uint32:
	case slyr_enum32:
	case slyr_bitfield32:
	case slyr_ipaddr:
		return mb_uint32;
	case slyr_acc32:
		return mb_acc32;
	case slyr_int64:
		return mb_int64;
	case slyr_acc64:
		return mb_acc64;
	case slyr_ipv6addr: // TODO: is this really a good idea?
	case slyr_string:
		return mb_string;
	case slyr_float32:
		return mb_float32;
	}
	/* should not reach this line */
	return mb_int16;
}

bool SunSlayer::initDict()
{
	class SuCon: public mbConv
	{
	private:
		float factor;
		quint16 address;
		SunSlayer* parent;

		void readFactor(QString element) {
			quint16 buf;
			qint16 ssf;
			bool okay = false;

			if( parent == NULL ){
				return;
			}

			if( parent->cache.contains(element) ){
				okay = parent->cache[element]->read(&buf, address, 1);
			}

			if( ! okay ){
				okay = parent->read_registers(&buf, address, mb_Hold, 1);
			}

			factor = 0;
			if( okay ){
				ssf = (qint16)io_ntohs(buf, BigEndian);
				if( -10 <= ssf && ssf <= 10 ){
					factor = pow(10, ssf);
				}
			}
		}

	public:
		SuCon(quint16 address, SunSlayer* parent) {
			factor = 0;
			this->address = address;
			this->parent = parent;
		}

		QVariant in(QString element, QVariant inval) {
			Q_UNUSED(element)

			readFactor(element);
			if( factor == 0 || !inval.isValid() ){
				return QVariant(QVariant::Invalid);
			}

			return QVariant(inval.toInt() * factor);
		}

		QVariant out(QString element, QVariant outval) {
			Q_UNUSED(element)

			readFactor(element);
			if( factor == 0 || !outval.isValid() ){
				return QVariant(QVariant::Invalid);
			}

			return QVariant(outval.toFloat() / factor);
		}
	};

	class Confactory{
	private:
		QMap<quint16, mbConv*> sucons;
		SunSlayer* parent;
	public:
		Confactory(SunSlayer* parent) {
			this->parent = parent;
		}

		mbConv* get(quint16 address) {
			if( ! sucons.contains(address) ){
				sucons[address] = new SuCon(address, parent); //TODO: should be free'd
			}
			return sucons[address];
		}
	};

	QList<quint16> models;
	Confactory confactory(this);
	bool okay;

	dict.clear();
	foreach( MBlock* blk, blocks.values() ){
		delete blk;
	}
	blocks.clear();
	cache.clear();

	okay = initAddressBook();
	if( !okay ) {
		qDebug() << "initialisation of address book failed";
		return false;
	}

	models = addressBook.keys();

	for( quint32 i = 0; i < sizeof sunSmodls / sizeof(slyr_Element); i++ ) {
		if( models.contains(sunSmodls[i].model) )	{
			DataPoint* tmp = new DataPoint;
			slyr_Element* elSpec = &sunSmodls[i];

			tmp->address = addressBook[elSpec->model] + 2 + elSpec->offset;
			tmp->name = elSpec->name;
			tmp->size = elSpec->size;
			tmp->type = castSlyrType(elSpec->type);
			//TODO: clean up (requires update of xslt)
			tmp->ioType = (mb_IoType)(elSpec->ioType);
			tmp->rType = mb_Hold;
			tmp->isMonitored = elSpec->isMandatory;
			if( elSpec->sOffset != -1 ){
				tmp->conversion = confactory.get(addressBook[elSpec->model] + 2 + elSpec->sOffset);
			} else {
				tmp->conversion = NULL;
			}

			dict.insert(elSpec->name, tmp);
		}
	}

	qDebug() << "dictionary has" << dict.size() << "entries:";
	qDebug() << dict.keys();

	return true;
}

bool SunSlayer::isModelAvail(quint16 model)
{
	if( addressBook.keys().contains(model) ) {
		return true;
	}
	return false;
}
