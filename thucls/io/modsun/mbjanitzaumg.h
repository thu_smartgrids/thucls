/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef MBJANITZAUMG_H
#define MBJANITZAUMG_H

#include <QObject>
#include "modler.h"

/**
 * Interface for Janitza UMG 96RM series metering device
 */
class MbJanitzaUmg : public Modler
{
	Q_OBJECT
public:
	explicit MbJanitzaUmg(QObject* parent = 0);
protected:
	bool initDict();
};

#endif // MBJANITZAUMG_H
