/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "mbjanitzaumg.h"

MbJanitzaUmg::MbJanitzaUmg(QObject *parent) : Modler(parent)
{
	slaveId = 1;
}

bool MbJanitzaUmg::initDict()
{
	static DataPoint janitzaPoints[] = {
		{19000, 2, "PhV_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19002, 2, "PhV_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19004, 2, "PhV_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19012, 2, "A_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19014, 2, "A_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19016, 2, "A_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19020, 2, "W_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19022, 2, "W_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19024, 2, "W_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19026, 2, "TotW", mb_float32, mb_R, mb_Hold, true, NULL},
		{19028, 2, "VA_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19030, 2, "VA_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19032, 2, "VA_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19034, 2, "TotVA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19036, 2, "VAr_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19038, 2, "VAr_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19040, 2, "VAr_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19042, 2, "TotVAr", mb_float32, mb_R, mb_Hold, true, NULL},
		{19044, 2, "PF_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19046, 2, "PF_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19048, 2, "PF_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19050, 2, "Hz", mb_float32, mb_R, mb_Hold, true, NULL},
		{19110, 2, "ThdPhV_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19112, 2, "ThdPhV_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19114, 2, "ThdPhV_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{19116, 2, "ThdA_phsA", mb_float32, mb_R, mb_Hold, true, NULL},
		{19118, 2, "ThdA_phsB", mb_float32, mb_R, mb_Hold, true, NULL},
		{19120, 2, "ThdA_phsC", mb_float32, mb_R, mb_Hold, true, NULL},
		{25436, 32, "DeviceName", mb_string, mb_RW, mb_Hold, true, NULL},
		{26000, 2, "SerialNumber", mb_uint32, mb_R, mb_Hold, true, NULL},
		{26002, 2, "ItemNumber", mb_uint32, mb_R, mb_Hold, true, NULL},
		{26004, 1, "ReleaseBase", mb_uint16, mb_R, mb_Hold, true, NULL},
		{26005, 1, "ModbusAddress", mb_uint16, mb_R, mb_Hold, true, NULL},
		{26006, 2, "ModbusBaudrate", mb_uint32, mb_R, mb_Hold, true, NULL},
		{26008, 1, "HardwareIndex", mb_uint16, mb_R, mb_Hold, true, NULL}
	};
	static quint32 nJanitzaPoints = sizeof janitzaPoints / sizeof(DataPoint);


	return initDictionary(janitzaPoints, nJanitzaPoints);
}
