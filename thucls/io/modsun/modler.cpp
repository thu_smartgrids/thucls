/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "modler.h"

#include <QDebug>
#include <QDateTime>
#include <QTcpSocket>

extern "C" {
#include "modbus/modbus-tcp.h"
#include "modbus/modbus.h"
}

Modler::Modler(QObject *parent, bool metering) : GenIo(parent, metering)
{
	mbConn = NULL;
	busorder = BigEndian;
	slaveId = 1;
	tcpPort = 502;

	cycling = false;
	cmToMillis = 320;

	qDebug() << "libmodbus has verion" << LIBMODBUS_VERSION_STRING;
}

Modler::~Modler()
{
	/* by intention no mbEx.lock() */
	if( mbConn ) {
		modbus_close(mbConn);

		modbus_free(mbConn);
		mbConn = NULL;
	}

	foreach( MBlock* blk, blocks.values() ){
		delete blk;
	}
	blocks.clear();
	cache.clear();
}

bool Modler::connectDevice()
{
	qint32 ret = -1;
	QTcpSocket socket;

	socket.connectToHost(ip, tcpPort, QIODevice::ReadOnly);

	if( ! socket.waitForConnected(1000) ){
		qDebug() << "could not connect to port" << tcpPort;
		return false;
	}
	socket.close();

	mbEx.lock();
	ret = modbus_connect(mbConn);
	mbEx.unlock();

	if ( ret ){
		qDebug() << "could not connect to" << ip << " port " << tcpPort << " return code: " << ret;
	}
	return ret == 0;
}

bool Modler::init(QString ip, qint32 deviceId)
{
	bool okay;

	dict.clear();

	mbEx.lock();
	if( mbConn ) {
		qDebug() << "re-init of ModBus layer";
		modbus_close(mbConn);
		modbus_free(mbConn);
	}

	this->ip = ip;

	if( 1 <= deviceId && deviceId <= 255 ){
		slaveId = (quint8) deviceId;
	}

	mbConn = modbus_new_tcp(ip.toLatin1().data(), tcpPort);
	mbEx.unlock();

	if( mbConn == NULL ) {
		qDebug() << "something went terribly wrong!!";
		return false;
	}

	okay = initCon();
	if(! okay ) {
		qDebug() << "configuring the connection failed";
		return false;
	}

	okay = connectDevice();
	if( ! okay ) {
		qInfo() << "connection to" << ip << "failed";
		return false;
	}

	okay = initDict();
	if(! okay ) {
		qDebug() << "initialising the dictionary failed";
		return false;
	}

	connState = io_IsOkay;
	connStaTime = QDateTime::currentMSecsSinceEpoch();
	return true;
}

bool Modler::initCon()
{
	timeval timeout = {1,0}; /* 1s (initial value, shortened in cycling mode */

	mbEx.lock();
	modbus_set_slave(mbConn, slaveId);
	modbus_set_response_timeout(mbConn, &timeout);
	mbEx.unlock();

	return true;
}

void Modler::enterCyclingMode()
{
	timeval timeout;

	if( cycling ){
		/* already in cycling mode */
		return;
	}

	cycling = true;
	timeout.tv_sec = cmToMillis / 1000;
	timeout.tv_usec = (cmToMillis % 1000) * 1000;
	mbEx.lock();
	modbus_set_response_timeout(mbConn, &timeout);
	mbEx.unlock();
}

bool Modler::initDictionary(DataPoint* dataPoints, quint32 nPoints)
{
	for( quint32 i = 0; i < nPoints; i++ ) {
		dict.insert(dataPoints[i].name, &dataPoints[i]);
	}

	qDebug() << "dictionary has" << dict.size() << "entries:";
	qDebug() << dict.keys();

	return true;
}

bool Modler::addBlock(QString block, qint64 validity, QString start, QString end)
{
	if( blocks.contains(block) ){
		qDebug() << "Modler::block named" << block << "already exists";
		return false;
	}

	if( !dict.contains(start) || !dict.contains(end) ){
		qDebug() << "Modler::could not find start/end in block" << block;
		return false;
	}

	quint16 startAddr = dict[start]->address;
	quint16 endAddr = dict[end]->address;
	quint16 blockLen = endAddr + dict[end]->size - startAddr;
	if( endAddr < startAddr || blockLen > UBUF_MAX_WORDS ){
		qDebug() << "Modler::block" << block << "has invalid size";
		return false;
	}

	mb_RegType rType = dict[start]->rType;

	MBlock* newBlock = new MBlock(block, validity, startAddr, blockLen, rType);
	blocks[block] = newBlock;

	bool differingRTypes = false;
	foreach( DataPoint* el, dict.values() ){
		if( startAddr <= el->address && el->address <= endAddr ){
			cache[el->name] = newBlock;

			if( el->isMonitored ){
				newBlock->isMonitored = true;
			}

			if( el->rType != rType ){
				differingRTypes = true;
			}
		}
	}
	if( differingRTypes ){
		qDebug() << "Modler::differing register types in block" << block;
	}
	return true;
}

bool Modler::readBlock(QString block)
{
	if( ! blocks.contains(block) ){
		qDebug() << "Modler: block named" << block << "is unknown";
		return false;
	}
	MBlock* blk = blocks[block];
	qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
	bool okay = read_registers(&blk->data.u16, blk->startAddr, blk->rType, blk->blockLen);
	if( ! okay ){
		if( blk-> isMonitored ){
			qDebug() << "Modler: reading block" << block << "failed";
		}
		blk->valid = false;
		return false;
	}
	blk->valid = true;
	blk->lastReadTime = currentTime;
	return true;
}

QVariant Modler::read(QString element, QString block)
{
	Q_UNUSED(block)

	bool okay;
	DataPoint* elSpec;
	Ubuf inbuf = {0};
	QVariant result;

	elSpec = dict[element];
	if( elSpec == NULL ) {
		qDebug() << "element" << element << "not found";
		return QVariant(QVariant::Invalid);
	}

	if( (elSpec->ioType != mb_R) && (elSpec->ioType != mb_RW) ) {
		qDebug() << element << "not readable";
		return QVariant(QVariant::Invalid);
	}

	if( elSpec->size > sizeof inbuf / 2 ) {
		qDebug() << element << "too big";
		return QVariant(QVariant::Invalid);
	}

	if( !cache.contains(element) ){
		okay = read_registers(&inbuf.u16, elSpec->address, elSpec->rType, elSpec->size);
		if( !okay && elSpec->isMonitored ){
			qDebug() << "reading" << element << "failed";
		}
	} else {
		okay = cache[element]->read(&inbuf.u16, elSpec->address, elSpec->size);
	}

	if( !okay ) {
		failcount += elSpec->isMonitored;
		return QVariant(QVariant::Invalid);
	}

	kaycount += elSpec->isMonitored;
	result = read_buffer(elSpec->type, inbuf);

	if( elSpec->conversion != NULL ){
		result = elSpec->conversion->in(element, result);
	}

	return result;
}

bool Modler::read_registers(quint16* inbuf, qint32 addr, mb_RegType regtype, quint16 size)
{
	qint32 ret;

	/* TODO: check whether reconnect is needed */

	//qDebug() << "read at " << addr << " size= " << size;
	mbEx.lock();
	switch( regtype ){
	case mb_Hold:
		ret = modbus_read_registers(mbConn, addr, size, inbuf);
		break;
	case mb_In:
		ret = modbus_read_input_registers(mbConn, addr, size, inbuf);
		break;
	case mb_Coil:
		ret = modbus_read_bits(mbConn, addr, size, (quint8*)inbuf);
		break;
	case mb_Disc:
		ret = modbus_read_input_bits(mbConn, addr, size, (quint8*)inbuf);
		break;
	default:
		qDebug() << "read_registers: regtype not supported";
		ret = -1;
		break;
	}
	mbEx.unlock();

	if( ret != size ) {
		qDebug() << "read registers " << ret << "!=" << size;
		return false;
	}

	/* revert the network2host conversion of the modbus layer */
	for( quint32 i = 0; i < size; i++ ) {
		inbuf[i] = io_htons(inbuf[i], BigEndian);
	}

	return true;
}

QVariant Modler::read_buffer(mb_Type type, Ubuf& inbuf)
{
	switch (type) {
	case mb_int16:
		return QVariant((qint16) io_ntohs(inbuf.u16, busorder));
	case mb_uint16:
	case mb_acc16:
		return QVariant(io_ntohs(inbuf.u16, busorder));
	case mb_int32:
		return QVariant((qint32) io_ntohl(inbuf.u32, busorder));
	case mb_uint32:
	case mb_acc32:
		return QVariant((quint32) io_ntohl(inbuf.u32, busorder));
	case mb_int64:
	case mb_uint64:
	case mb_acc64:
		return QVariant(( (quint64)(io_ntohl(inbuf.h64[0], busorder)) << 32)
				+ io_ntohl(inbuf.h64[1], busorder));
	case mb_string:
		return QVariant(QString(inbuf.s));
	case mb_float32:
		inbuf.u32 = io_ntohl(inbuf.u32, busorder);
		return QVariant(inbuf.f32);
	default:
		return QVariant(QVariant::Invalid);
	}
}

bool Modler::writeBlock(QString block)
{
	if( ! blocks.contains(block) ){
		qDebug() << "Modler: block named" << block << "is unknown";
		return false;
	}
	MBlock* blk = blocks[block];
	Q_UNUSED(blk->valid);
	Q_UNUSED(blk->lastReadTime);

	bool okay = write_registers(blk->startAddr, blk->rType, blk->blockLen, &(blk->data.u16));
	if( ! okay ){
		if( blk->isMonitored ){
			qDebug() << "Modler: writing block" << block << "failed";
			failcount += 1;
		}
		return false;
	}
	kaycount += blk->isMonitored;
	return true;
}

bool Modler::write_univ(QString element, QVariant value, QString block)
{
	DataPoint* elSpec;
	Ubuf outbuf = {0};
	bool result;

	elSpec = dict[element];
	if( elSpec == NULL ) {
		qDebug() << "element" << element << "not found";
		failcount += 1;
		return false;
	}

	if( (elSpec->ioType != mb_W) && (elSpec->ioType != mb_RW) ) {
		qDebug() << element << "not writeable";
		return false;
	}

	if( elSpec->size > sizeof outbuf / 2 ) {
		qDebug() << element << "too big";
		return false;
	}

	if( elSpec->conversion != NULL ){
		value = elSpec->conversion->out(element, value);
	}

	if( ! value.isValid() ){
		qDebug() << "given value for" << element << "is invalid";
		return false;
	}

	write_buffer(outbuf, elSpec->type, value);

	if( !blocks.contains(block) ){
		result = write_registers(elSpec->address, elSpec->rType, elSpec->size, &outbuf.u16);
	} else {
		result = blocks[block]->write(elSpec->address, elSpec->size, &outbuf.u16);
	}

	if( !result ){
		qDebug() << "writing" << element << "failed";
		failcount += elSpec->isMonitored;
	} else {
		kaycount += elSpec->isMonitored;
	}

	return result;
}

void Modler::write_buffer(Ubuf& outbuf, mb_Type type, QVariant value)
{
	switch (type) {
	case mb_int16:
		outbuf.u16 = io_htons(value.toInt(), busorder);
		break;
	case mb_uint16:
	case mb_acc16:
		outbuf.u16 = io_htons(value.toUInt(), busorder);
		break;
	case mb_int32:
		outbuf.u32 = io_htonl(value.toInt(), busorder);
		break;
	case mb_uint32:
	case mb_acc32:
		outbuf.u32 = io_htonl(value.toUInt(), busorder);
		break;
	case mb_int64:
	case mb_uint64:
	case mb_acc64:
		outbuf.h64[0] = io_htonl(value.toLongLong() >> 32, busorder);
		outbuf.h64[1] = io_htonl(value.toLongLong() & 0xFFFFFFFFu, busorder);
		break;
	case mb_string:
		strncpy(outbuf.s, value.toString().toLatin1().data(), sizeof outbuf);
		break;
	case mb_float32:
		outbuf.f32 = value.toFloat();
		outbuf.u32 = io_htonl(outbuf.u32, busorder);
		break;
	}
}

bool Modler::write_registers(qint32 addr, mb_RegType regtype, quint16 size, quint16* outbuf)
{
	qint32 ret;

	/*ret = modbus_connect(mbConn);
	if( ret != 0 ) {
		qDebug() << "connection failed";
		return false;
	}*/

	/* revert the host2network conversion of the modbus layer */
	for( quint32 i = 0; i < size; i++ ) {
		outbuf[i] = io_ntohs(outbuf[i], BigEndian);
	}

	mbEx.lock();
	switch( regtype ){
	case mb_Hold:
		if( size == 1 ){
			ret = modbus_write_register(mbConn, addr, outbuf[0]);
		} else {
			ret = modbus_write_registers(mbConn, addr, size, outbuf);
		}
		break;
	default:
		qDebug() << "write_registers: regtype not supported";
		ret = -1;
		break;
	}
	mbEx.unlock();
	qDebug() << "wrote at " << addr << " size= " << size;

	if( ret != size ) {
		return false;
	}

	/*modbus_close(mbConn);*/

	return true;
}

io_ConnState Modler::checkHealth()
{
	io_ConnState result = GenIo::checkHealth();

	qint64 currentTime = QDateTime::currentMSecsSinceEpoch();
	foreach( MBlock* blk, blocks.values() ){
		if( blk->valid && (blk->lastReadTime + blk->validityPeriod < currentTime) ){
			blk->valid = false;
		}
	}

	return result;
}

bool Modler::recover(quint16 failrate)
{
	Q_UNUSED(failrate)

	return reconnect();
}

bool Modler::reconnect()
{
	bool okay;

	mbEx.lock();
	modbus_close(mbConn);
	mbEx.unlock();

	if( dict.size() != 0 ){
		okay = connectDevice();
	} else {
		okay = init(ip, slaveId);
	}

	if( ! okay ) {
		qDebug() << "connection failed";
		return false;
	}

	return true;
}

MBlock::MBlock(QString name, qint64 validity, quint16 start, quint16 len, mb_RegType rType)
{
	blockName = name;
	validityPeriod = validity;
	startAddr = start;
	blockLen = len;
	this->rType = rType;

	valid = false;
	isMonitored = false;
	lastReadTime = 0;
	memset(&data.u16, 0, sizeof data);
	/*data is marked as invalid no initialisation needed therefore*/
}

bool MBlock::read(quint16 *inbuf, quint16 addr, quint16 len)
{
	if( !valid ){
		return false;
	}
	if( addr < startAddr || (startAddr+blockLen < addr+len) ){
		qDebug("Modler: trying to read from wrong block");
		return false;
	}
	memcpy(inbuf, &data.w[addr-startAddr], len*2);
	return true;
}

bool MBlock::write(quint16 addr, quint16 len, quint16 *outbuf)
{
	if( addr < startAddr || (startAddr+blockLen < addr+len) ){
		qDebug("Modler: trying to write at wrong block");
		return false;
	}
	memcpy(&data.w[addr-startAddr], outbuf, len*2);
	return true;
}
