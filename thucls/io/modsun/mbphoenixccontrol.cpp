/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "mbphoenixccontrol.h"

MbPhoenixCControl::MbPhoenixCControl(QObject *parent) : Modler(parent)
{
	busorder = HalfEndian;
	slaveId = 180;
}

/**
 * Initializes the Modler dictionary with the used dataPoints of the Phoenix EV Charge Control.
 */
bool MbPhoenixCControl::initDict()
{
	class StaticConv : public mbConv {
	public:
		StaticConv(float factor) { this->factor = factor; }
		QVariant in(QString element, QVariant inval) { Q_UNUSED(element); return QVariant(inval.toFloat() * factor); }
		QVariant out(QString element, QVariant outval) { Q_UNUSED(element); return QVariant(outval.toFloat() / factor); }
	private:
		float factor;
	};

	static StaticConv conv10(10);
	static StaticConv conv0_01(0.01);
	static StaticConv conv0_001(0.001);
	static DataPoint chargeControlPoints[] = {
		{100, 1, "EvStatus", mb_int16, mb_R, mb_In, true, NULL},
		{101, 1, "ProximityChargingCurrent", mb_int16, mb_R, mb_In, false, NULL},
		{102, 2, "ChargingTime", mb_int32, mb_R, mb_In, false, NULL},
		{105, 2, "FirmwareVersion", mb_int32, mb_R, mb_In, true, NULL},
		{107, 1, "ErrorCodes", mb_int16, mb_R, mb_In, true, NULL},
		{108, 2, "DispV1", mb_int32, mb_R, mb_In, true, &conv0_01},
		{110, 2, "DispV2", mb_int32, mb_R, mb_In, true, &conv0_01},
		{112, 2, "DispV3", mb_int32, mb_R, mb_In, true, &conv0_01},
		{114, 2, "DispI1", mb_int32, mb_R, mb_In, false, &conv0_001},
		{116, 2, "DispI2", mb_int32, mb_R, mb_In, false, &conv0_001},
		{118, 2, "DispI3", mb_int32, mb_R, mb_In, false, &conv0_001},
		{120, 2, "DispRealPower", mb_int32, mb_R, mb_In, false, &conv10},
		{122, 2, "DispReactivePower", mb_int32, mb_R, mb_In, false, NULL},
		{124, 2, "DispApparentPower", mb_int32, mb_R, mb_In, false, &conv10},
		{126, 2, "DispPowerFactor", mb_int32, mb_R, mb_In, false, &conv0_01},
		{128, 2, "DispEnergy", mb_int32, mb_R, mb_In, false, &conv0_01},
		{130, 2, "DispMaxPower", mb_int32, mb_R, mb_In, false, NULL},
		{132, 2, "DispEnergyOfCurrentCharging", mb_int32, mb_R, mb_In, false, &conv0_001},
		{134, 2, "DispFrequency", mb_int32, mb_R, mb_In, false, &conv0_01},
		{200, 1, "Enable", mb_uint16, mb_R, mb_Disc, true, NULL},
		{202, 1, "LockDetection", mb_uint16, mb_R, mb_Disc, true, NULL},/*single bit*/
		{300, 1, "PwmSignal", mb_int16, mb_RW, mb_Hold, true, NULL},
		{304, 6, "SerialNo", mb_string, mb_R, mb_Hold, true, NULL},
		{310, 5, "DeviceName", mb_string, mb_RW, mb_Hold, true, NULL},
		{315, 1, "IPAddressPart1", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{316, 1, "IPAddressPart2", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{317, 1, "IPAddressPart3", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{318, 1, "IPAddressPart4", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{352, 2, "ConvV1", mb_float32, mb_RW, mb_Hold, false, NULL},
		{354, 2, "ConvV2", mb_float32, mb_RW, mb_Hold, false, NULL},
		{356, 2, "ConvV3", mb_float32, mb_RW, mb_Hold, false, NULL},
		{358, 2, "ConvI1", mb_float32, mb_RW, mb_Hold, false, NULL},
		{360, 2, "ConvI2", mb_float32, mb_RW, mb_Hold, false, NULL},
		{362, 2, "ConvI3", mb_float32, mb_RW, mb_Hold, false, NULL},
		{364, 2, "ConvRealPower", mb_float32, mb_RW, mb_Hold, false, NULL},
		{366, 2, "ConvReactivePower", mb_float32, mb_RW, mb_Hold, false, NULL},
		{368, 2, "ConvApparentPower", mb_float32, mb_RW, mb_Hold, false, NULL},
		{376, 2, "ConvEnergyOfCurrentCharging", mb_float32, mb_RW, mb_Hold, false, NULL},
		{378, 2, "ConvFrequency", mb_float32, mb_RW, mb_Hold, false, NULL},
		{386, 2, "BaudRate", mb_uint32, mb_R, mb_Hold, false, NULL},/*for testing purpose*/
		{402, 1, "StationAvail", mb_uint16, mb_RW, mb_Coil, false, NULL},/*single bit*/
		{410, 1, "ByteSequence", mb_uint16, mb_RW, mb_Coil, false, NULL},/*0 little endian, 1 big endian * check at initialisation*/
		{500, 1, "WordsV1", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{501, 1, "WordsV2", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{502, 1, "WordsV3", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{503, 1, "WordsI1", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{504, 1, "WordsI2", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{505, 1, "WordsI3", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{506, 1, "WordsRealPower", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{507, 1, "WordsReactivePower", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{508, 1, "WordsApparentPower", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{512, 1, "WordsEnergyOfCurrentCharging", mb_uint16, mb_RW, mb_Hold, false, NULL},
		{513, 1, "WordsFrequency", mb_uint16, mb_RW, mb_Hold, false, NULL},
	};
	static quint32 nChargeControlPoints = sizeof chargeControlPoints / sizeof(DataPoint);


	return initDictionary(chargeControlPoints, nChargeControlPoints);
}
