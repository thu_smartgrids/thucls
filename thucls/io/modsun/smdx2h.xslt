<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:csv="csv:csv">

<!-- generate the C header file for either a single smdx file or a selection of smdx files
     run with "msxsl smdx_00001.xml smdx2h.xslt" 
      or with "msxsl sel.xml smdx2h.xslt -o sunsmodls.h" -->

    <xsl:output method="text" encoding="utf-8" />
    <xsl:strip-space elements="*" />
	
	<xsl:key name="sf_by_name" match="point" use="@id"/>

	<xsl:template match="/smdxSelection">
        <!-- header -->
        <xsl:text>/** SunSpec model definitions.&#xa;  * This file was generated from the respective smdx's. */&#xa;#ifndef SUNSMODLS_H&#xa;#define SUNSMODLS_H&#xa;&#xa;#include "sunsel.h"&#xa;&#xa;static slyr_Element sunSmodls [] = {</xsl:text>
		<xsl:if test="@path">
			<xsl:apply-templates select="smdx" >
				<xsl:with-param name="path" select="@path" />
			</xsl:apply-templates>
		</xsl:if>
		<!-- footer -->
		<xsl:text>&#xa;};&#xa;&#xa;#endif // SUNSMODLS_H&#xa;</xsl:text>
	</xsl:template>
	
	<xsl:template match="smdx">
		<xsl:param name="path" />
		
		<xsl:if test="@model">
			<xsl:variable name="model" select="format-number(@model, '00000')" />			
			<xsl:apply-templates select="document(concat($path, '/smdx_', $model, '.xml') )/sunSpecModels/model" />
			<xsl:if test="position() != last()">
				<xsl:text>,</xsl:text>
			</xsl:if>
		</xsl:if>
	</xsl:template>
	
    <xsl:template match="/sunSpecModels">
        <xsl:text>static slyr_Element sunSmodls [] = {</xsl:text>

        <xsl:apply-templates select="model" />

		<xsl:text>&#xa;};&#xa;</xsl:text>
    </xsl:template>

    <xsl:template match="model">
		<xsl:apply-templates select="block/point">
			<xsl:with-param name="modelID" select="@id" />
		</xsl:apply-templates>
	</xsl:template>
	
	<xsl:template match="point">
		<xsl:param name="modelID"/>
        <xsl:variable name="point" select="." />	
		<xsl:value-of select="normalize-space($point/*[name() = 'offset'])" />
		
		<xsl:text>&#xa;&#9;{</xsl:text>

		<xsl:value-of select="$modelID" />
		<xsl:text>, </xsl:text>
		<xsl:value-of select="@offset" />
		<xsl:text>, </xsl:text>
		<xsl:choose>
			<xsl:when test="@len" >
				<xsl:value-of select="@len" />
			</xsl:when>
			<xsl:when test="@type = 'int16'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'sunssf'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'uint16'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'acc16'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'enum16'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'bitfield16'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'pad'">
				<xsl:text>1</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'uint32'">
				<xsl:text>2</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'int32'">
				<xsl:text>2</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'acc32'">
				<xsl:text>2</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'enum32'">
				<xsl:text>2</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'bitfield32'">
				<xsl:text>2</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'ipaddr'">
				<xsl:text>2</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'int64'">
				<xsl:text>4</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'uint64'">
				<xsl:text>4</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'acc64'">
				<xsl:text>4</xsl:text>
			</xsl:when>
			<xsl:when test="@type = 'ipv6addr'">
				<xsl:text>8</xsl:text>
			</xsl:when>
			<!-- when type is string then use @len -->
			<xsl:when test="@type = 'float32'">
				<xsl:text>2</xsl:text>
			</xsl:when>
		</xsl:choose>
		<xsl:text>, &quot;</xsl:text>
		<xsl:value-of select="@id" />
		<xsl:text>&quot;, slyr_</xsl:text>
		<xsl:value-of select="@type" />
		<xsl:text>, slyr_</xsl:text>
		<xsl:choose>
			<xsl:when test="@access = 'w'">
				<xsl:text>W</xsl:text>
			</xsl:when>
			<xsl:when test="@access = 'rw'">
				<xsl:text>RW</xsl:text>
			</xsl:when>
			<xsl:otherwise> <!-- default -->
				<xsl:text>R</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<!--hier ist neu -->
		<xsl:choose>
			<xsl:when test="@mandatory = 'true'">
				<xsl:text>, true</xsl:text>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>, false</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>, </xsl:text>
		<xsl:choose>
			<xsl:when test="@sf">
				<xsl:for-each select="key('sf_by_name',@sf)">
					<xsl:value-of select="@offset" />
<!-- 					<xsl:text> /*</xsl:text>
					<xsl:value-of select="@id" />					
					<xsl:text>*/</xsl:text>
 -->				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>-1</xsl:text>
			</xsl:otherwise>
		</xsl:choose>
		<xsl:text>}</xsl:text>
		<xsl:if test="position() != last()">
			<xsl:text>,</xsl:text>
		</xsl:if>
    </xsl:template>

</xsl:stylesheet>