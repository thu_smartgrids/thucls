#include "mbslogger.h"

MbSLogger::MbSLogger(QObject *parent) : Modler(parent)
{
	/* slaveId = 1, tcpPort = 502, BigEndian */
	busorder = HalfEndian;
}

/**
 * Initializes the Modler dictionary with the used dataPoints of the Solar Log data logger series.
 */
bool MbSLogger::initDict()
{
	static DataPoint loggerPoints[] = {
		{3500, 2, "lastUpdateTime", mb_uint32, mb_R, mb_Hold, false, NULL},
		{3502, 2, "Pac", mb_uint32, mb_R, mb_In, true, NULL},
		{3504, 2, "Pdc", mb_uint32, mb_R, mb_In, true, NULL},
		{3506, 1, "Uac", mb_uint16, mb_R, mb_In, true, NULL},
		{3507, 1, "Udc", mb_uint16, mb_R, mb_In, true, NULL},
		{3508, 2, "DailyYield", mb_uint32, mb_R, mb_In, true, NULL},
		{3510, 2, "YesterdayYield", mb_uint32, mb_R, mb_In, true, NULL},
		{3530, 2, "TotalPower", mb_uint32, mb_R, mb_In, true, NULL}, /* to be confirmed by using another SolarLog */

		{10400, 1, "PLimitType", mb_uint16, mb_W, mb_Hold, true, NULL},
		{10401, 1, "PLimitPerc", mb_uint16, mb_W, mb_Hold, true, NULL},
		{10404, 2, "WatchDog_Tag", mb_uint32, mb_W, mb_Hold, true, NULL},

		{10900, 1, "Status", mb_uint16, mb_R, mb_In, true, NULL},
		{10901, 1, "PLimitPercN", mb_uint16, mb_R, mb_In, true, NULL},
		{10902, 2, "PLimitN", mb_float32, mb_R, mb_In, true, NULL},
		{10904, 2, "ProdW", mb_uint32, mb_R, mb_In, true, NULL},
		{10906, 2, "PossibleProdW", mb_uint32, mb_R, mb_In, false, NULL},
		{10908, 2, "ConsW", mb_uint32, mb_R, mb_In, true, NULL},
		{10910, 2, "GridfeedW", mb_uint32, mb_R, mb_In, true, NULL}
	};
	static quint32 nLoggerPoints = sizeof loggerPoints / sizeof(DataPoint);

	return initDictionary(loggerPoints, nLoggerPoints);
}
