/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef MBOLIBOX_H
#define MBOLIBOX_H

#include <QObject>
#include <QVariant>
#include "modler.h"

class AppLing;

static const QVariant NO_VALUE(QVariant::Invalid);
typedef enum {
	noCtrl = 0,
	emCtrl = 1,
	dsoCtrl = 2,
	genCtrl = 3  /* both EM + DSO */
} OliAccessConfig;

class MbOliBox : public Modler
{
	Q_OBJECT
public:
	static MbOliBox* getOneInstance(AppLing *parent = 0);
	bool init(QString ip, OliAccessConfig accessConfig = noCtrl);

	void initValues(QVariant maxPower);
	void update(QVariant power, QVariant limit=NO_VALUE);
	void connectMB();
	void disconnectMB();
public slots:
	void checkDsoLimit(bool forceWrite);
	void checkEmLimit(bool forceWrite);
protected:
	explicit MbOliBox(AppLing* parent = 0);
	bool initDict();

	AppLing* appling;
	OliAccessConfig access;
	QVariant lastDsoLimit, lastEmLimit;

	quint32 maxPow;
	quint64 sum_powerProduction, sum_powerConsumption;
	quint32 count;
	bool firstRun;
	qint64 startTime;
};

#endif // MBOLIBOX_H
