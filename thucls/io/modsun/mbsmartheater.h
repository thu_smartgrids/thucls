/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef MBSMARTHEATER_H
#define MBSMARTHEATER_H

#include <QObject>
#include "modler.h"

/**
 * Interface to the E.G.O Smart Heater 29.65335.100
 */

class MbSmartHeater : public Modler
{
	Q_OBJECT
public:
	MbSmartHeater(QObject* parent);

protected:
	bool initDict();

	QVariant read_buffer(mb_Type type, Ubuf& inbuf);
};

#endif // MBSMARTHEATER_H
