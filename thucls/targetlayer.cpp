/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "targetlayer.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QDebug>
#include <QDir>
#include <QEventLoop>
#include <QFile>
#include <QHash>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkInterface>
#include <QProcessEnvironment>
#include <QTextStream>
#include <QTimer>
#include <QUdpSocket>
#include <string>

//for test only:
//#define PLUGIN 1

static quint32 failures;
extern QTextStream cout;

QNetworkInterface getNetInterface()
{
#if defined(linux)
	return QNetworkInterface::interfaceFromName("eth0");
#else
	foreach( QNetworkInterface ni, QNetworkInterface::allInterfaces() ){
		if( (ni.flags() & (QNetworkInterface::IsLoopBack | QNetworkInterface::IsUp))
				== QNetworkInterface::IsUp ){
			if( ni.humanReadableName() == "Ethernet" ){
				return ni;
			}
		}
	}
	return QNetworkInterface();
#endif
}

/* this object serves as a container for the Socket object, which is expected to be part of the
 * QT parent/child hierarchy
 */
class Logger : QObject {
public:
	Logger(QObject* parent);
	~Logger();

	QUdpSocket usock;
	QHostAddress outAddr;
	QList<quint32> notified;

	QFile outFile;
	qint32 verbosity;
};

static Logger* logger = NULL;

#if defined(PLUGIN) && defined(linux)
#include <pthread.h>
#include <syslog.h>

/** Message handler to redirect qDebug to syslog */
void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
	Q_UNUSED(context)

	QString emsg = '#' + QString::number(pthread_self()) + "# " + context.file + '\n' + msg;
	QString omsg = "      " + msg + '\n';

	//QString msg2 = logger->outAddr.toString();
	//QAbstractSocket::SocketState tmp = logger->usock.state();


	switch (type) {
	case QtDebugMsg:
		::syslog(LOG_DEBUG, emsg.toLocal8Bit().constData());
		break;
	case QtWarningMsg:
		::syslog(LOG_WARNING, emsg.toLocal8Bit().constData());
		break;
	default:
		::syslog(LOG_ALERT, emsg.toLocal8Bit().constData());
		break;
	}

	if( ! logger ){
		return;
	}
	qint64 len = logger->usock.writeDatagram(omsg.toLatin1(), logger->outAddr, 10035);
	logger->usock.flush();

	Q_UNUSED(len)
}
#else

/** Message handler to redirect qDebug to log file */
void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
/*	Q_UNUSED(context)
	Q_UNUSED(type)*/

	if( ! logger ){
		return;
	}

#ifndef INTERACTIVE
	quint32 id;
	switch( type ){
	case QtDebugMsg:
		id = qHash(QString(context.file)) + context.line;
		if( logger->notified.contains(id) ){
			return;
		}
		logger->notified.append(id);
		break;
	default:
		break;
	}
#endif

	QString emsg = QDateTime::currentDateTime().toString("MMM/dd hh:mm:ss,zzz: ").toLatin1().data() + msg;

	QTextStream out(&logger->outFile);
	out << emsg << endl;
	out.flush();

	if( logger->verbosity < 0 ){
		return;
	}

	cout << emsg << endl;
}
#endif

Logger::Logger(QObject* parent) : QObject(parent), usock(this), outFile("out.log") {
	verbosity = 0;

	QString ip = TargetLayer::getIP();
	usock.bind(QHostAddress(ip), 10333, QAbstractSocket::ShareAddress | QAbstractSocket::ReuseAddressHint);
	outAddr = QHostAddress(ip.left(ip.lastIndexOf(".")) + ".255");

#ifndef PLUGIN
	outFile.open(QIODevice::WriteOnly | QIODevice::Append);
	QTextStream out(&outFile);
	out << "\n----\n\n\n";
	out.flush();
#endif

	if( ! logger ){
		logger = this;
	}

	qInstallMessageHandler(myMessageOutput);
}

Logger::~Logger() {
	if( logger == this ){
		qInstallMessageHandler(0);
		logger = NULL;
#ifndef PLUGIN
		outFile.close();
#endif
	}
}



static QNetworkInterface qni = getNetInterface();


void TargetLayer::initLogger(QObject* parent)
{
	if( logger )
		return;
	/* constructor will set logger */
	new Logger(parent);
}


QVariantMap TargetLayer::readConfigFile()
{
	QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
	if( env.contains("CLS_CONFIG") ){
		QString var = env.value("CLS_CONFIG");
		if( var.startsWith("{") ){
			QVariantMap result = QJsonDocument::fromJson(var.toLatin1()).object().toVariantMap();
			if( !result.isEmpty() ){
				qDebug() << "using CLS_CONFIG";
				return result;
			}
		}
		qDebug() << "invalid CLS_CONFIG:" << var;
	}

	QDir::setCurrent(QCoreApplication::applicationDirPath());
	QFile cfgFile("config.json");

	bool okay = cfgFile.open(QIODevice::ReadOnly);
	if( !okay ){
		qDebug() << "could not read config file";
		return QVariantMap();
	}
	QVariantMap result = QJsonDocument::fromJson(cfgFile.readAll()).object().toVariantMap();
	cfgFile.close();

	if( result.isEmpty() ){
		qDebug() << "config file invalid";
	}
	return result;
}

///** Handle events that are unhandled yet */
void TargetLayer::waitForOthers(int ms)
{
	QEventLoop eventLoop;
	QTimer::singleShot(ms, &eventLoop, SLOT(quit()));
	QTimer timer;
	QObject::connect(&timer, &QTimer::timeout, &eventLoop, &QEventLoop::quit);
	timer.start(1000); //1 sec

	eventLoop.exec();
}

QString TargetLayer::getSwRevision()
{
	static QString swRevision = QString("1.4 (") + QString(__DATE__) + QString(" ") + QString(__TIME__) + QString(")");
	qDebug() << "SW Revision:" << swRevision;
	return swRevision;
}

QString TargetLayer::getIP()
{
	QString result = "";

	foreach( QNetworkAddressEntry addr, qni.addressEntries() ){
		QHostAddress ip = addr.ip();
		if( ip.protocol() == QAbstractSocket::IPv4Protocol && !ip.isLoopback() ){
			result = ip.toString();
			return result;
		}
	}

	return result;
}

QString TargetLayer::getMac()
{
	QString result = qni.hardwareAddress();

	if( (result.size() < 17) || !result.contains(':') ){
		return "00:00:00:00:00:00";
	}

	return result;
}

quint32 TargetLayer::getTimeSlice15min()
{
	return QTime::currentTime().msecsSinceStartOfDay() / 900000;
}

void TargetLayer::reportFailure(quint32 failure)
{
	failures |= failure;
}

quint32 TargetLayer::getFailures()
{
	return failures;
}
