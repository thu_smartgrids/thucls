/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef TARGETLAYER_H
#define TARGETLAYER_H

#include <QObject>
#include <QString>
#include <QVariantMap>

#if !defined(PLUGIN) && defined(linux)
	#define IEC_PORT 10003
#else
	#define IEC_PORT 102
#endif


/**
 * Bundles functions that contain complex or potentially plattform depending access
 * onto QT library and/or Operation System.
 */
namespace TargetLayer
{
	void initLogger(QObject* parent);
	QVariantMap readConfigFile();

	void waitForOthers(int ms);
	QString getSwRevision();
	QString getIP();
	QString getMac();

	quint32 getTimeSlice15min();

	void reportFailure(quint32 failure);
	quint32 getFailures();

	static const quint64 failDoubleClientInitedTwice = 1;
}

#endif // TARGETLAYER_H
