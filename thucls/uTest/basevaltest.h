/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#ifndef BASEVALTEST_H
#define BASEVALTEST_H

#include "../plugIns/sling.h"

class BasevalTest : public Sling
{
	Q_OBJECT
public:
	explicit BasevalTest(QObject *parent = 0);

	bool connectDevice();

protected:
	bool checkBaseVals();
};

#endif // BASEVALTEST_H
