/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "iotest.h"

#include <QStringList>
#include <QTextStream>

IoTest::IoTest()
{
}

void IoTest::interactiveTest(QString name, GenIo* device)
{
#ifndef PLUGIN
	extern QTextStream cin, cout;

	QString inStr;
	QStringList sl;

	if( device == NULL ){
		cout << "can't perform interactive test for " << name << endl;
		return;
	}

	cout << "perform interactive test for " << name << endl;

	while (1) {
		inStr = cin.readLine();
		sl = inStr.split(" ", QString::SkipEmptyParts);
		if (sl.size() == 0 || sl[0].size() != 1) {
			sl=QStringList("h");
		}

		switch (sl[0].at(0).toLatin1()) {
		case 'b':
			if (sl.size() == 2) {
				cout << device->readBlock(sl[1]) << endl;
			} else {
				cout << "enter H for help!" << endl;
			}
			break;
		case 'r':
		case 'R':
			if (sl.size() == 2) {
				cout << device->read(sl[1]).toString() << endl;
			} else if (sl.size() == 3) {
				cout << device->read(sl[1], sl[2]).toString() << endl;
			} else {
				cout << "enter H for help!" << endl;
			}
			break;
		case 'w':
		case 'W':
			if (sl.size() == 3) {
				//qDebug() << sl[1] << "," << sl[2];
				cout << device->write(sl[1], sl[2]) << endl;
			} else {
				cout << "enter H for help!" << endl;
			}
			break;
		case 'q':
		case 'Q':
			return;
		default:
			cout << "Choose one of these commands:" << '\n';
			cout << "Read Block: \'B\' + block" << '\n';
			cout << "Read:       \'R\' + name" << '\n';
			cout << "            \'R\' + name + block" << '\n';
			cout << "Write:      \'W\' + name + value" << '\n';
			cout << "Quit:       \'Q\'" << endl;
		}
	}
#else
	Q_UNUSED(name)
	Q_UNUSED(device)
#endif
}
