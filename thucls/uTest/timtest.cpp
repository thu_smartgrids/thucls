/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "timtest.h"
#include <QDateTime>
#include <QVariant>
#include <QVariantMap>
#include <QVector>
#include <QDebug>

typedef enum {
	Anton, Berta, Claus, Dieta, Emma, Friedolin, Gerda, Helmut, Ida,
	Jacqueline, Karl_Heinz, Linda, Mannfred, Norbert, NoName
} Names;

static QVariantMap sWeight;
static QVector<QVariant> eWeight(NoName);
static QVariant aWeight[] = {75, 90, 60, 72, 67, 50, 65, 101, 55, 59, 82, 63, 80, 70};

TimTest::TimTest()
{
	sWeight["Anton"] = 75;
	sWeight["Berta"] = 90;
	sWeight["Claus"] = 60;
	sWeight["Dieta"] = 72;
	sWeight["Emma"] = 67;
	sWeight["Friedolin"] = 50;
	sWeight["Gerda"] = 65;
	sWeight["Helmut"] = 101;
	sWeight["Ida"] = 55;
	sWeight["Jacqueline"] = 59;
	sWeight["Karl-Heinz"] = 82;
	sWeight["Linda"] = 63;
	sWeight["Mannfred"] = 80;
	sWeight["Norbert"] = 70;

	eWeight[Anton] = 75;
	eWeight[Berta] = 90;
	eWeight[Claus] = 60;
	eWeight[Dieta] = 72;
	eWeight[Emma] = 67;
	eWeight[Friedolin] = 50;
	eWeight[Gerda] = 65;
	eWeight[Helmut] = 101;
	eWeight[Ida] = 55;
	eWeight[Jacqueline] = 59;
	eWeight[Karl_Heinz] = 82;
	eWeight[Linda] = 63;
	eWeight[Mannfred] = 80;
	eWeight[Norbert] = 70;
}

void TimTest::interactiveTest()
{
	quint32 i, j = 0;
	qint64 t;

	qint64 tStart, tEnd;

	qDebug() << "start timing benchmark";

	tStart = QDateTime::currentMSecsSinceEpoch();
	for( i=0; i < 1000; i++ ){
		t = QDateTime::currentMSecsSinceEpoch();
		/* avoid removed function call by optimising compiler */
		j ^= t & 0xFFFFFF;
	}
	tEnd = QDateTime::currentMSecsSinceEpoch();
	qDebug() << "some number: " << j;
	qDebug() << "reading millis:" << (tEnd - tStart) << "ms";


//	tStart = QDateTime::currentMSecsSinceEpoch();
//	for( i=0; i < 100000; i++ ){
//		t = QDateTime::currentSecsSinceEpoch();
//		/* avoid removed function call by optimising compiler */
//		j ^= t & 0xFFFFFF;
//	}
//	tEnd = QDateTime::currentMSecsSinceEpoch();
//	qDebug() << "some number: " << j;
//	qDebug() << "reading seconds:" << (tEnd - tStart) << "ms";


	tStart = QDateTime::currentMSecsSinceEpoch();
	j = 1001;
	for( i=0; i < 1000; i++ ){
		j += sWeight["Friedolin"].toInt();
		j += sWeight["Ida"].toInt();
		j += sWeight["Jacqueline"].toInt();
		j += sWeight["Claus"].toInt();
		j += sWeight["Linda"].toInt();
		j += sWeight["Gerda"].toInt();
		j += sWeight["Emma"].toInt();
		j += sWeight["Norbert"].toInt();
		j += sWeight["Dieta"].toInt();
		j += sWeight["Anton"].toInt();
		j += sWeight["Mannfred"].toInt();
		j += sWeight["Karl-Heinz"].toInt();
		j += sWeight["Berta"].toInt();
		j += sWeight["Helmut"].toInt();
		j ^= i;
	}
	tEnd = QDateTime::currentMSecsSinceEpoch();
	qDebug() << "some number: " << j;
	qDebug() << "string as index:" << (tEnd - tStart) << "ms";

	tStart = QDateTime::currentMSecsSinceEpoch();
	j = 1001;
	for( i=0; i < 1000; i++ ){
		j += eWeight[Friedolin].toInt();
		j += eWeight[Ida].toInt();
		j += eWeight[Jacqueline].toInt();
		j += eWeight[Claus].toInt();
		j += eWeight[Linda].toInt();
		j += eWeight[Gerda].toInt();
		j += eWeight[Emma].toInt();
		j += eWeight[Norbert].toInt();
		j += eWeight[Dieta].toInt();
		j += eWeight[Anton].toInt();
		j += eWeight[Mannfred].toInt();
		j += eWeight[Karl_Heinz].toInt();
		j += eWeight[Berta].toInt();
		j += eWeight[Helmut].toInt();
		j ^= i;
	}
	tEnd = QDateTime::currentMSecsSinceEpoch();
	qDebug() << "some number: " << j;
	qDebug() << "enum as index:" << (tEnd - tStart) << "ms";

	tStart = QDateTime::currentMSecsSinceEpoch();
	j = 1001;
	for( i=0; i < 1000; i++ ){
		j += aWeight[Friedolin].toInt();
		j += aWeight[Ida].toInt();
		j += aWeight[Jacqueline].toInt();
		j += aWeight[Claus].toInt();
		j += aWeight[Linda].toInt();
		j += aWeight[Gerda].toInt();
		j += aWeight[Emma].toInt();
		j += aWeight[Norbert].toInt();
		j += aWeight[Dieta].toInt();
		j += aWeight[Anton].toInt();
		j += aWeight[Mannfred].toInt();
		j += aWeight[Karl_Heinz].toInt();
		j += aWeight[Berta].toInt();
		j += aWeight[Helmut].toInt();
		j ^= i;
	}
	tEnd = QDateTime::currentMSecsSinceEpoch();
	qDebug() << "some number: " << j;
	qDebug() << "plain array:" << (tEnd - tStart) << "ms";

}
