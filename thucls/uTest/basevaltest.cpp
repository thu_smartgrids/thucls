/*******************************************************************
 **
 ** Copyright (C) 2015-2021 Smart Grids Research Group at the
 **               University of Applied Sciences Ulm, Germany
 **
 ** Contact: heiko.lorenz@thu.de
 **
 ** This program is hereby distributed under the terms of the GNU 
 ** Lesser General Public License version 3. A copy of the license 
 ** should be part of this distribution package. 
 ** If not, see <http://www.gnu.org/licenses/>
 **
 ** This program is distributed WITHOUT ANY WARRANTY; See the
 ** GNU General Public License for more details. 
 **
 *******************************************************************/

#include "basevaltest.h"

/**
 * This component is intended to be used for unit-tests on the local host. The
 * Sunspec Test Tool by J.D. Blair (see https://github.com/jdblair/sunspec) serves
 * as counterpart.
 * The model description file basevaltest.model utilises SunSpec model type 63001,
 * which is intended for testing purposes. Before starting the test start the suns
 * tool in server mode (admin rights are needed) using this shell call:
 *
 * sudo suns -vvv -s -m "basevaltest.model"
 */


BasevalTest::BasevalTest(QObject *parent) :
	Sling(parent)
{
}

bool BasevalTest::connectDevice()
{
	bool result = Sling::connectDevice();

	if( result ) {
		result = checkBaseVals();
	}
	return result;

}

bool BasevalTest::checkBaseVals()
{
	typedef struct {
		QString key;
		QVariant value;
	} Value;

	static Value testValues[] = {
		{"Mn", "Lorenz Industries Ltd."},
		{"Md", "Basic value test"},
		{"SN", "none"},
		{"int16_1", 29696},
		{"uint16_1", 32768},
		{"int32_1", 321},
		{"uint32_1", 2000000000},
		{"float32", 234.5},
		{"int64", 0x123456789ABCDEF0ull},
		{"string", "It's a string with 31 chars!!"}
	};

	static Value outValues[] = {
		{"int16_4", 42},
		{"uint16_4", 0xFFFF},
		{"int32_3", 123},
		{"uint32_3", 4000000000ull},
		{"int64", 0x23456789ABCDEF01ull},
		{"float32", 12345.875},
		{"string", "Test!"}
	};

	static const quint32 nTestValues = sizeof testValues / sizeof(Value);
	static const quint32 nOutValues = sizeof outValues / sizeof(Value);

	quint32 n, count;
	QVariant value;

	for (n=0; n<nTestValues; n++) {
		value = slayer.read(testValues[n].key);
		if (value != testValues[n].value) {
			qDebug() << "!!! " << testValues[n].key << ": " << value.toString()
				 << " != " << testValues[n].value.toString();
			return false;
		}
	}
	qDebug() << nTestValues << " test values passed";

	for (n=0; n<nOutValues; n++) {
		value = outValues[n].value;
		if( ! slayer.write(outValues[n].key, outValues[n].value) )
		{
			qDebug() << "!!! writing to " << outValues[n].key << "failed";
			return false;
		}
	}

	for (n=0; n<nOutValues; n++) {
		value = slayer.read(outValues[n].key);
		if (value != outValues[n].value) {
			qDebug() << "!!! " << outValues[n].key << ": " << value.toString()
				 << " != " << outValues[n].value.toString();
			return false;
		}
	}

	qDebug() << nOutValues << " out values passed";

	count=0;
	for (n=0; n<nTestValues; n++) {
		if( slayer.write(testValues[n].key, testValues[n].value) )
			count++;
	}

	qDebug() << count << " test values reset";
	return true;
}
